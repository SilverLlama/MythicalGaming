#if defined STANDALONE_BUILD
#include <sourcemod>
#include <sdktools>

#include <store>
#include <zephstocks>

new bool:GAME_TF2 = false;
#endif

#if defined STANDALONE_BUILD
public OnPluginStart()
#else
public letters_OnPluginStart()
#endif
{
#if defined STANDALONE_BUILD
	// TF2 is unsupported
	new String:m_szGameDir[32];
	GetGameFolderName(m_szGameDir, sizeof(m_szGameDir));
	if(strcmp(m_szGameDir, "tf")==0)
		GAME_TF2 = true;
#endif
	Store_RegisterHandler("letters", "", letters_OnMapStart, letters_Reset, letters_Config, letters_Equip, letters_Remove, true);
}

public letters_OnMapStart()
{
}

public letters_Reset()
{
}

public letters_Config(&Handle:kv, itemid)
{
	Store_SetDataIndex(itemid, 0);
	return true;
}

public letters_Equip(client, id)
{
	return -1;
}

public letters_Remove(client, id)
{
}
