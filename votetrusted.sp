#include <cstrike>
#include <sourcemod>
#include <core>
#include <float>
new bool:gag[66][66];
new Float:percentage = 60;
public Plugin:myinfo =
{
	name = "Gag vote",
	description = "Gags a person on DM vote",
	author = "Meitis",
	version = "1.0",
	url = "http://www.last-resistance.co.uk/"
};


bool:DM_IsClientDM(client)
{
	new bool:value;
	new flags = GetUserFlagBits(client);
	if (GetUserFlagBits(client) & (ADMFLAG_CUSTOM6 | ADMFLAG_GENERIC) == (ADMFLAG_CUSTOM6 | ADMFLAG_GENERIC))
	{
    value = true;
	}
	return value;
}
new ayaya = 1;
public void OnPluginStart()
{
	RegConsoleCmd("sm_votegag", SwapteamVote, "Start a vote to gag a player.", 0);
	RegConsoleCmd("sm_gagvote", SwapteamVote, "Start a vote to gag a player.", 0);
	LoadTranslations("common.phrases");
	return 0;
}

public void OnClientDisconnect(client)
{
	if (GetAdminFlag(client,ADMFLAG_GENERIC)) 
	{
		ayaya--;
	}
	new i = 1;
	while (i <= MaxClients)
	{
		gag[i][client] = false;
		gag[client][i] = false;
		i++;
	}
	return 0;
}

public OnClientPostAdminCheck(client) 
{ 
	if (GetAdminFlag(client,ADMFLAG_GENERIC)) 
	{ 
		ayaya++;
	} 
}


public Action:SwapteamVote(client, args)
{
 if ( ayaya == 1 )
 {
	if (args != 1)
	{
		ReplyToCommand(client, " \x07[SM]\x01 Usage: sm_votegag\x03 <player/#userid>");
		return Action:3;
	}
	if (!DM_IsClientDM(client))
	{
		ReplyToCommand(client, " \x07[SM]\x01 Only \x03dedicated members\x01 can use this command. Apply for it on the forums!");
		return Action:3;
	}
	new String:arg1[32];
	GetCmdArg(1, arg1, 32);
	new String:target_name[64];
	new bool:tn_is_ml;
	new target_list[1];
	if (0 >= ProcessTargetString(arg1, client, target_list, 1, 8, target_name, 64, tn_is_ml))
	{
		ReplyToCommand(client, " \x07[SM]\x01 No matching client was found.");
		return Action:3;
	}
	gag[target_list[0]][client] = true;
	PrintToChat(client,"%N wants to gag %N using !votegag %N. (%d/%d).", client, target_list, target_list, GetGagCount(target_list[0]), GetRequired());
	return Action:3;
}
else 
{
PrintToChat(client, "Sorry there is a staff on the server who can mute.");
return Plugin_Handled;
}
}


GetDMCount()
{
	new count;
	new i = 1;
	while (i <= MaxClients)
	{
		if (IsClientInGame(i) && DM_IsClientDM(i))
		{
			count++;
		}
		i++;
	}
	return count;
}



ExecuteGag(client)
{
	if (IsClientInGame(client))
	{
		new userid = GetClientUserId(client);
		ServerCommand("sm_gag #%d 15 Gagged by DM vote", userid);
		ResetVotes(client);
	}
	return 0;
}

public void OnMapStart()
{
	new i = 1;
	while (i <= MaxClients)
	{
		ResetVotes(i);
		i++;
	}
	return 0;
}

GetGagCount(client)
{
	new count;
	new i = 1;
	while (i <= MaxClients)
	{
		if (gag[client][i])
		{
			count++;
		}
		i++;
	}
	if (IsClientInGame(client) && count >= GetRequired())
	{
		PrintToChatAll("Gag vote successful, player %N gagged.", client);
		ExecuteGag(client);
	}
	return count;
}

public void ResetVotes(client)
{
	new i = 1;
	while (i <= MaxClients)
	{
		gag[client][i] = false;
		i++;
	}
	return 0;
}

GetRequired()
{
	return RoundToCeil(percentage * GetDMCount());
}