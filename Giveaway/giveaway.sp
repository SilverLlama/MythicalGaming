#pragma semicolon 1

#define DEBUG

#define PLUGIN_AUTHOR "SilverLlama"
#define PLUGIN_VERSION "1.00"
#include <ttt>
#include <sourcemod>
#include <sdktools>
#include <store>
#include <multicolors>

EngineVersion g_Game;

Handle g_pPool = null;
Handle g_hStartTimer = null;

bool bHasHappened;
bool bTimeLimitOff = false;

int iCount;

public Plugin myinfo = 
{
	name = "Name Giveaway",
	author = PLUGIN_AUTHOR,
	description = "15 minutes into every map does a giveaway for all the people who have Mythical in their name.",
	version = PLUGIN_VERSION,
	url = "mythicalgaming.net"
};

public void OnPluginStart()
{
	g_Game = GetEngineVersion();
	if(g_Game != Engine_CSGO && g_Game != Engine_CSS)
	{
		SetFailState("This plugin is for CSGO/CSS only.");	
	}
	g_pPool = CreateArray();
	
	HookEvent("round_start", OnRoundStart);
}

public void OnMapEnd(){
	if(g_hStartTimer != null)
		g_hStartTimer = null;
		
	bHasHappened = false;
}

public void OnMapStart(){

	Handle hTmp;	
	hTmp = FindConVar("mp_timelimit");
	float iTimeLimit = GetConVarFloat(hTmp);			
	if (hTmp != INVALID_HANDLE)
		CloseHandle(hTmp);
	
	bHasHappened = false;
	iCount = 0;
	
	iTimeLimit = iTimeLimit * 30;
	if (iTimeLimit > 0)
		g_hStartTimer = CreateTimer(iTimeLimit, LoadStuff);
	else bTimeLimitOff = true;
	
}

public Action OnRoundStart(Handle event, const char[] name, bool dontBroadcast)
{
	if (!bTimeLimitOff)
		return Plugin_Continue;
	
	if(bHasHappened)
		return Plugin_Continue;
	
	iCount++;
	
	if(iCount < 7)
		return Plugin_Continue;
	
	
	char cName[MAX_NAME_LENGTH];
	int test;
	ClearArray(g_pPool);
	LoopValidClients(i){
		if(IsFakeClient(i))
			continue;
		GetClientName(i, cName, sizeof(cName));
		test = StrContains(cName, "mythical", false);
		
		if ( test != -1)
		{
			PushArrayCell(g_pPool, i);
		}
		
	}
	
	int iArraySize = GetArraySize(g_pPool);
	
	if (iArraySize <= 0)
	{
		PrintToChatAll(" \x04[GiveAway] \x01Nobody won the map giveaway! To enter next map add \x02'Mythical' \x01in your name.");
		bHasHappened = true;
		return Plugin_Handled;
	}
	
	float size = float(iArraySize);
	float randFlt = GetRandomFloat(0.0, size);
	int randInt = RoundToNearest(randFlt);
	
	if (randInt == size)
		randInt = randInt - 1;
	
	int winner_account = GetArrayCell(g_pPool, randInt);
	
	
	Store_SetClientCredits(winner_account, Store_GetClientCredits(winner_account) + 150);
	
	GetClientName(winner_account, cName, sizeof(cName));
	//PrintToChatAll("Total: %i, Selected number: %i, winner: %s", iArraySize, randInt, name);
	PrintToChatAll(" \x04[GiveAway] \x02%s \x01just won the map giveaway (150 credits)! To enter next map add \x02'Mythical' \x01in your name.", cName);
	bHasHappened = true;
	
	return Plugin_Continue;
}

int GetClientOfAccountId(int accountID)
{
	char buffer1[32];
	Format(buffer1, 32, "%d", accountID);
	LoopClients(i)
	{
		char buffer2[32];
		Format(buffer2, 32, "%d", g_iAccountID[i]);
		if(StrEqual(buffer1, buffer2))
		{
			return i;
		}
	}
	
	return -1;
}

public Action LoadStuff(Handle timer)
{
	if(bHasHappened)
		return Plugin_Handled;
	
	char name[MAX_NAME_LENGTH];
	int test;
	ClearArray(g_pPool);
	LoopValidClients(i){
		if(IsFakeClient(i))
			continue;
		GetClientName(i, name, sizeof(name));
		test = StrContains(name, "mythical", false);
		
		if ( test != -1)
		{
			PushArrayCell(g_pPool, i);
		}
		
	}
	
	int iArraySize = GetArraySize(g_pPool);
	
	if (iArraySize <= 0)
	{
		PrintToChatAll(" \x04[GiveAway] \x01Nobody won the map giveaway! To enter next map add \x02'Mythical' \x01in your name.");
		bHasHappened = true;
		return Plugin_Handled;
	}
	
	float size = float(iArraySize);
	float randFlt = GetRandomFloat(0.0, size);
	int randInt = RoundToNearest(randFlt);
	
	if (randInt == size)
		randInt = randInt - 1;
	
	int winner_account = GetArrayCell(g_pPool, randInt);
	
	
	Store_SetClientCredits(winner_account, Store_GetClientCredits(winner_account) + 150);
	
	GetClientName(winner_account, name, sizeof(name));
	//PrintToChatAll("Total: %i, Selected number: %i, winner: %s", iArraySize, randInt, name);
	PrintToChatAll(" \x04[GiveAway] \x02%s \x01just won the map giveaway (150 credits)! To enter next map add \x02'Mythical' \x01in your name.", name);
	bHasHappened = true;
	g_hStartTimer = null;
	return Plugin_Handled;
}