/*
 * SourceMod Radio
 *
 * Adds a radio player to SourceMod that supports direct station links or streaming audio
 *
 * Coded by dubbeh - www.dubbeh.net
 *
 * Licensed under the GPLv3
 *
 */


void PrintPrimaryGenreList(int iClient)
{
	int iIndex = 0, iCount = 0;
	char szGenreName[64] = "";
	char szBuffer[512] = "";
	RadioGenre genre;
	
	for (iIndex = 0, iCount = 0; iIndex < g_Genres.Count; iIndex++)
	{
		genre = g_Genres.Items(iIndex);
		
		if (genre.IsValid && genre.m_iParentID == 0)
		{
			genre.GetName(szGenreName, sizeof(szGenreName));
			
			if ((iIndex + 2) == g_Genres.Count)
			{
				StrCat(szBuffer, sizeof(szBuffer), szGenreName);
			} else {
				StrCat(szBuffer, sizeof(szBuffer), szGenreName);
				StrCat(szBuffer, sizeof(szBuffer), ", ");
			}
			
			iCount++;
			
			if (iCount == 10)
			{
				CReplyToCommand(iClient, szBuffer);
				clear_string(szBuffer, strlen(szBuffer));
				iCount = 0;
			}
		}
	}
	
	if (iCount < 10)
	{
		CReplyToCommand(iClient, szBuffer);
	}
}

void clear_string (char []szBuffer, int iLen)
{
	int iIndex = 0;
	for (iIndex = 0; iIndex <= iLen; iIndex++)
	{
		szBuffer[iIndex] = 0;
	}
}

int FindPrimaryGenreIndexByName(char[] szGenreName)
{
	char szCompareGenreName[64] = "";
	int iIndex = 0;
	RadioGenre genre;
	
	for (iIndex = 0; iIndex < g_Genres.Count; iIndex++)
	{
		genre = g_Genres.Items(iIndex);
		if (genre.IsValid && genre.m_iParentID == 0)
		{
			genre.GetName(szCompareGenreName, sizeof(szCompareGenreName));
			if (!strcmp(szGenreName, szCompareGenreName, false))
				return iIndex;
		}
	}
	
	return 0;
}

void BrowseURL(int iClient, char[] szURL)
{
	if (g_EngineVersion == Engine_CSGO)
		ShowMOTDPanel(iClient, "SourceMod Browse", CSGOBrowseHack(iClient, szURL), MOTDPANEL_TYPE_URL);
	else
		ShowMOTDPanel(iClient, "SourceMod Browse", szURL, MOTDPANEL_TYPE_URL);
}

void PluginCleanup()
{
	if (g_mRadioBaseMenu) { delete g_mRadioBaseMenu; g_mRadioBaseMenu = null; }
	if (g_mRadioBaseGenresMenu) { delete g_mRadioBaseGenresMenu; g_mRadioBaseGenresMenu = null; }
	if (g_mRadioStationsMenu) { delete g_mRadioStationsMenu; g_mRadioStationsMenu = null; }
	if (g_mRadioVolumeMenu) { delete g_mRadioVolumeMenu; g_mRadioVolumeMenu = null; }
	g_Genres.Dispose(true);
	g_Stations.Dispose(true);
}

void LoadStation(int iClient, char[] szStationID)
{
	int iStationID = 0;
	char szBuffer[128] = "";
	char szStreamURL[128] = "";
	char szStationVolume[12] = "";
	char szClientName[MAX_NAME_LENGTH + 1] = "";
	char szStationName[64] = "";
	RadioStation station;
	
	
	if ((iStationID = StringToInt(szStationID)) > 0 && IsClientConnected(iClient) && IsClientInGame(iClient))
	{
		station = g_Stations.Items(GetStationIndexFromID(iStationID));
		
		if (!station.IsValid)
			LogMessage("[SM-RADIO] Error: Tried to load a station with an invalid index. Please check your stations list file.");
		
		station.GetStreamURL(szStreamURL, sizeof(szStreamURL));
		
		if (szStreamURL[0] || g_cVarRadioPlayType.IntValue == RADIO_PLAY_TYPE_SHOUTCAST)
		{
			GetClientCookie(iClient, g_hCookieSavedVolume, szStationVolume, sizeof(szStationVolume));
			if (!szStationVolume[0])
			{
				FloatToString(g_cVarInitialConnectVolume.FloatValue, szStationVolume, sizeof(szStationVolume));
				SetClientCookie(iClient, g_hCookieSavedVolume, szStationVolume);
			}
			
			if (g_cVarRadioPlayType.IntValue == RADIO_PLAY_TYPE_DIRECT)
			{
				ShowMOTDFromURL(iClient, szStreamURL);
			}
			else if (g_cVarRadioPlayType.IntValue == RADIO_PLAY_TYPE_VOLUME_CONTROL)
			{
				// Removed for now, creates problems with certain IceCast 2 streams - Need to add custom detection
				/*if (StrContains(szStreamURL, ".mp3", false) == -1)
				{
					StrCat(szStreamURL, sizeof(szStreamURL), "/;");
				}*/
				
				ShowMOTDFromURL(iClient, RadioVolumeWrapper(iClient, szStreamURL, szStationVolume));
			}
			else if (g_cVarRadioPlayType.IntValue == RADIO_PLAY_TYPE_SHOUTCAST)
			{
				Format(szBuffer, sizeof(szBuffer), "%s?sid=%d&mod=%d&vol=%s", g_szShoutcastURL, station.m_iID, g_EngineVersion, szStationVolume);
				ShowMOTDFromURL(iClient, szBuffer);
			}
			
			if (g_cVarStationAdvert.IntValue)
			{
				station.GetName(szStationName, sizeof(szStationName));
				GetClientName(iClient, szClientName, sizeof(szClientName));
				CPrintToChatAll("%T", "Started Listening", LANG_SERVER, szClientName, szStationName);
			}
			
			SetClientCookie(iClient, g_hCookieSavedStation, szStationID);
		}
		else
		{
			LogMessage("[SM-RADIO] Error: Station with ID \"%d\" has no Stream URL set. Please correct this in the config file.", station.m_iID);
		}
	}
}

void ShowMOTDFromURL(int iClient, char[] szURL)
{
	if (!IsClientConnected(iClient) || !IsClientInGame(iClient))
		return;
	
	if (g_EngineVersion == Engine_CSGO)
		LoadSilentMOTDPanel(iClient, PLUGIN_NAME, szURL);
	else
		ShowMOTDPanel(iClient, PLUGIN_NAME, szURL, MOTDPANEL_TYPE_URL);
}

char CSGOBrowseHack(int iClient, char[] szURL)
{
	char szBuffer[256] = "";
	
	Format(szBuffer, sizeof(szBuffer), "%s?url=%s", g_szBrowseFixURL, szURL);
	IsURLTooLong(iClient, szBuffer);
	return szBuffer;
}

char RadioVolumeWrapper(int iClient, char[] szStationURL, char[] szVolume)
{
	char szURL[256] = "";
	
	Format(szURL, sizeof(szURL), "%s?url=%s&volume=%s", g_szVolumeWrapperURL, szStationURL, szVolume);
	IsURLTooLong(iClient, szURL);
	return szURL;
}

void IsURLTooLong(int iClient, char[] szURL)
{
	if (IsClientConnected(iClient) && IsClientInGame(iClient))
	{
		if (strlen(szURL) > MAX_MOTD_URL_SIZE)
		{
			LogMessage("[SM-RADIO] Warning: URL \"%s\" exceeds the 192 bytes limit and will end up truncated.", szURL);
			CPrintToChat(iClient, "%T", "Radio URL Too Long", LANG_SERVER);
		}
	}
}

void LoadOffPage(int iClient)
{
	char szClientName[MAX_NAME_LENGTH + 1] = "";
	
	if (IsClientConnected(iClient) && IsClientInGame(iClient))
	{
		LoadSilentMOTDPanel(iClient, "SourceMod Radio", g_szRadioOffPage);
		SetClientCookie(iClient, g_hCookieSavedStation, "");
		
		if (g_cVarStationAdvert.IntValue)
		{
			GetClientName(iClient, szClientName, sizeof(szClientName));
			CPrintToChatAll("%T", "Stopped Listening", LANG_SERVER, szClientName);
		}
	}
}

void LoadSilentMOTDPanel(int iClient, char[] szTitle, char[] szPage)
{
	if (!IsClientConnected(iClient) || !IsClientInGame(iClient))
		return;
	
	Handle kv = INVALID_HANDLE;
	
	kv = CreateKeyValues("data");
	KvSetString(kv, "title", szTitle);
	KvSetNum(kv, "type", MOTDPANEL_TYPE_URL);
	KvSetString(kv, "msg", szPage);
	ShowVGUIPanel(iClient, "info", kv, false);
	CloseHandle(kv);
}

int GetGenreIndexFromID(int iGenreID)
{
	int iIndex = 0;
	RadioGenre genre;
	
	for (iIndex = 0; iIndex < g_Genres.Count; iIndex++)
	{
		genre = g_Genres.Items(iIndex);
		if (!genre.IsValid)
			continue;
		else if (genre.m_iID == iGenreID)
			return iIndex;
	}
	
	return 0;
}

int GetStationIndexFromID(int iStationID)
{
	int iIndex = 0;
	RadioStation station;
	
	for (iIndex = 0; iIndex < g_Stations.Count; iIndex++)
	{
		station = g_Stations.Items(iIndex);
		
		if (!station.IsValid)
			continue;
		else if (iStationID == station.m_iID)
			return iIndex;
	}
	
	return 0;
}
