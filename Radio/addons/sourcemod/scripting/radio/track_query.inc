/*
 * SourceMod Radio
 *
 * Adds a radio player to SourceMod that supports direct station links or streaming audio
 *
 * Coded by dubbeh - www.dubbeh.net
 *
 * Licensed under the GPLv3
 *
 */

void GetCurretTrack(int iClient)
{
	char szClientStation[32] = "";
	int iStationID = 0;
	RadioStation station;
	Handle hHTTPRequest = INVALID_HANDLE;
	char szQueryURL[128] = "";
	char szCurrentTrack[128] = "";
	
	GetClientCookie(iClient, g_hCookieSavedStation, szClientStation, sizeof(szClientStation));
	iStationID = StringToInt(szClientStation);
	
	if (iStationID > 0 && iStationID != -1)
	{
		station = g_Stations.Items(GetStationIndexFromID(iStationID));
		
		if (station.IsValid)
		{
			if (g_cVarRadioPlayType.IntValue == RADIO_PLAY_TYPE_SHOUTCAST)
			{
				if (station.m_iLastUpdateTime < (GetTime() + TRACK_UPDATE_TIME))
				{
					// Prepare the query URL and execute it
					Format(szQueryURL, sizeof(szQueryURL), "%s?sid=%s", g_szShoutcastQueryURL, szClientStation);
					if (STEAMWORKS_AVAILABLE())
					{
						if ((hHTTPRequest = SteamWorks_CreateHTTPRequest(k_EHTTPMethodGET, szQueryURL)) != INVALID_HANDLE)
						{
							if (!SteamWorks_SetHTTPRequestContextValue(hHTTPRequest, GetClientSerial(iClient)) || 
								!SteamWorks_SetHTTPCallbacks(hHTTPRequest, SteamWorks_OnTrackQueryComplete) || 
								!SteamWorks_SendHTTPRequest(hHTTPRequest))
							{
								LogMessage("[SM-RADIO] Error: Problem querying the current track with SteamWorks.");
								CReplyToCommand(iClient, "Error: Problem querying the current track with SteamWorks.");
								CloseHandle(hHTTPRequest);
							}
						}
					} else {
						LogMessage("[SM-RADIO] Please install SteamWorks to use the track query feature.");
						CReplyToCommand(iClient, "Please install SteamWorks to use the track query feature.");
					}
				} else {
					station.GetCurrentTrack(szCurrentTrack, sizeof(szCurrentTrack));
					CReplyToCommand(iClient, szCurrentTrack);
				}
			} else {
				CReplyToCommand(iClient, "Track query only Supported in ShoutCast mode for now.");
			}
		} else {
			LogMessage("[SM-RADIO] Error: Tried to load a station with an invalid index. Please check your stations list file.");
		}
	} else {
		CReplyToCommand(iClient, "Pick a station first to use the track query feature.");
	}
}

public SteamWorks_OnTrackQueryComplete(Handle hRequest, bool bFailure, bool bRequestSuccessful, EHTTPStatusCode eStatusCode, any iClientSerial)
{
	if (!bFailure && bRequestSuccessful && eStatusCode == k_EHTTPStatusCode200OK && iClientSerial > 0)
	{
		SteamWorks_GetHTTPResponseBodyCallback(hRequest, SteamWorks_TrackQueryWebResponse, iClientSerial);
	}
	
	CloseHandle(hRequest);
}

public SteamWorks_TrackQueryWebResponse(const char[] szResponseBuffer, any iClientSerial)
{
	int iClient = 0;
	char szStationID[12] = "";
	int iStationID = 0;
	RadioStation station;
	
	iClient = GetClientFromSerial(iClientSerial);
	
	if (iClient && IsClientConnected(iClient) && IsClientInGame(iClient))
	{
		GetClientCookie(iClient, g_hCookieSavedStation, szStationID, sizeof(szStationID));
		iStationID = StringToInt(szStationID);
		
		station = g_Stations.Items(GetStationIndexFromID(iStationID));
		station.SetCurrentTrack(szResponseBuffer);
		station.m_iLastUpdateTime = GetTime();
		CPrintToChat(iClient, szResponseBuffer);
	} else {
		LogMessage("[SM-RADIO] SteamWorks_TrackQueryWebResponse() GetClientSerial returned 0. Maybe the client disconnected.");
	}
}
