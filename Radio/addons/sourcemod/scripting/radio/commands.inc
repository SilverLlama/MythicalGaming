/*
 * SourceMod Radio
 *
 * Adds a radio player to SourceMod that supports direct station links or streaming audio
 *
 * Coded by dubbeh - www.dubbeh.net
 *
 * Licensed under the GPLv3
 *
 */


public Action Command_Radio(int iClient, int iArgs)
{
	char szBaseSubCommand[32] = "";
	
	if (g_cVarEnable.IntValue)
	{
		if (iArgs > 0)
		{
			GetCmdArg(1, szBaseSubCommand, sizeof(szBaseSubCommand));
			
			if (!strcmp(szBaseSubCommand, "version", false))
			{
				CReplyToCommand(iClient, "SourceMod Radio v%s by dubbeh - www.dubbeh.net", PLUGIN_VERSION);
				CReplyToCommand(iClient, "Powered by www.ShoutCast.com");
			}
		}
		else if (g_mRadioBaseMenu != null)
		{
			g_mRadioBaseMenu.Display(iClient, MENU_TIME_FOREVER);
		}
		else
		{
			SetFailState("[SM-RADIO] Base menu appears to be invalid. Please report this error to the author.");
		}
	}
	
	return Plugin_Handled;
}

public Action Command_Browse(int iClient, int iArgs)
{
	if (g_cVarEnable.IntValue) {
		if (iArgs == 1) {
			char szWebsite[MAX_MOTD_URL_SIZE + 1] = "";
			
			GetCmdArg(1, szWebsite, sizeof(szWebsite));
			BrowseURL(iClient, szWebsite);
		} else {
			CReplyToCommand(iClient, "Invalid browse format");
			CReplyToCommand(iClient, "Usage: sm_browse \"www.website.com\" or !browse www.website.com");
		}
	}
	
	return Plugin_Handled;
}

public Action Command_RadioOff(int iClient, int iArgs)
{
	if (g_cVarEnable.IntValue)
		LoadOffPage(iClient);
	
	return Plugin_Handled;
}

public Action Command_Volume(int iClient, int iArgs)
{
	char szStationVolume[32] = "";
	float fVolume = 0.1;
	char szStationID[32] = "";
	
	if (g_cVarEnable.IntValue) {
		if (iArgs > 0) {
			GetCmdArg(1, szStationVolume, sizeof(szStationVolume));
			fVolume = StringToFloat(szStationVolume);
			
			if (fVolume > 0.01 && fVolume < 1.0) {
				SetClientCookie(iClient, g_hCookieSavedVolume, szStationVolume);
				CReplyToCommand(iClient, "%T", "Volume Set To", LANG_SERVER, szStationVolume);
				GetClientCookie(iClient, g_hCookieSavedStation, szStationID, sizeof(szStationID));
				LoadStation(iClient, szStationID);
			} else {
				CPrintToChat(iClient, "Invalid volume setting. Minimum 0.01 (1%) - Maximum 1.0 (100%)");
			}
		} else if (g_mRadioVolumeMenu != INVALID_HANDLE) {
			g_mRadioVolumeMenu.Display(iClient, MENU_TIME_FOREVER);
		} else {
			LogMessage("[SM-RADIO] Volume menu handle appears to be invalid. Please report this error the author.");
		}
	}
	
	return Plugin_Handled;
}

public Action Command_Track(int iClient, int iArgs)
{
	if (g_cVarEnable.IntValue) {
		if (STEAMWORKS_AVAILABLE())
			GetCurretTrack(iClient);
		else
			CReplyToCommand(iClient, "SteamWorks extension not found to use the track query feature.");
	}
	
	return Plugin_Handled;
}

public Action Command_Genres(int iClient, int iArgs)
{
	char szBaseSubCommand[64] = "";
	int iGenreFound = 0;
	RadioGenre genre;
	char szGenreName[64] = "";
	
	if (g_cVarEnable.IntValue && g_bUseGenres) {
		if (iArgs == 0) {
			if (g_mRadioBaseGenresMenu != INVALID_HANDLE) {
				g_mRadioBaseGenresMenu.Display(iClient, MENU_TIME_FOREVER);
			} else {
				SetFailState("[SM-RADIO] Genre menu appears to be invalid. Please report this error to the author.");
			}
		} else if (iArgs > 0) {
			GetCmdArg(1, szBaseSubCommand, sizeof(szBaseSubCommand));
			iGenreFound = FindPrimaryGenreIndexByName(szBaseSubCommand);
			
			if (iGenreFound > 0) {
				genre = g_Genres.Items(iGenreFound);
				if (genre.IsValid && genre.m_hMenu != INVALID_HANDLE) {
					DisplayMenu(genre.m_hMenu, iClient, MENU_TIME_FOREVER);
				} else {
					genre.GetName(szGenreName, sizeof(szGenreName));
					SetFailState("[SM-RADIO] Genre \"%s\" using index \"%d\" appears to be invalid. Please report this error to the author.", szGenreName, genre.m_iID);
				}
			} else {
				CReplyToCommand(iClient, "Listing available primary genres:");
				PrintPrimaryGenreList(iClient);
			}
		}
	}
	
	return Plugin_Handled;
}
