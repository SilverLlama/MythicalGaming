#if defined _dynamic_methodmap_radiostation
  #endinput
#endif
#define _dynamic_methodmap_radiostation

methodmap RadioStation < Dynamic
{
	public RadioStation()
	{
		Dynamic radiostation = Dynamic();
		return view_as<RadioStation>(radiostation);
	}
	
	property int m_iID
	{
		public get()
		{
			return this.GetInt("m_iID", 0);
		}
		public set (int iID)
		{
			this.SetInt("m_iID", iID);
		}
	}
	
	property int m_iGenreID
	{
		public get()
		{
			return this.GetInt("m_iGenreID", 0);
		}
		public set (int iGenreID)
		{
			this.SetInt("m_iGenreID", iGenreID);
		}
	}

	public bool GetName(char[] szBuffer, int iLength)
	{
		return this.GetString("m_szName", szBuffer, iLength);
	}
	
	public void SetName(const char[] szBuffer)
	{
		this.SetString("m_szName", szBuffer, 128);
	}
	
	public bool GetStreamURL(char[] szBuffer, int iLength)
	{
		return this.GetString("m_szStreamURL", szBuffer, iLength);
	}
	
	public void SetStreamURL(const char[] szBuffer)
	{
		this.SetString("m_szStreamURL", szBuffer, 128);
	}
	
	public bool GetCurrentTrack(char[] szBuffer, int iLength)
	{
		return this.GetString("m_szCurrentTrack", szBuffer, iLength);
	}
	
	public void SetCurrentTrack(const char[] szBuffer)
	{
		this.SetString("m_szCurrentTrack", szBuffer, 256);
	}
	
	property int m_iLastUpdateTime
	{
		public get()
		{
			return this.GetInt("m_iLastUpdateTime", 0);
		}
		public set (int iLastUpdateTime)
		{
			this.SetInt("m_iLastUpdateTime", iLastUpdateTime);
		}
	}
}
