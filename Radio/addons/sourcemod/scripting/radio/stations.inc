#if defined _dynamic_collection_radiostations_
  #endinput
#endif
#define _dynamic_collection_radiostations_


methodmap RadioStations < Collection
{
	public RadioStations()
	{
		return view_as<RadioStations>(new Collection());
	}
	
	public RadioStation Items(int index)
	{
		return view_as<RadioStation>(this.Items(index));
	}
}
