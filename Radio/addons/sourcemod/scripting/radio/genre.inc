#if defined _dynamic_methodmap_radiogenre_
  #endinput
#endif
#define _dynamic_methodmap_radiogenre_


methodmap RadioGenre < Dynamic
{
	public RadioGenre()
	{
		Dynamic radiogenre = Dynamic();
		return view_as<RadioGenre>(radiogenre);
	}
	
	property int m_iID
	{
		public get()
		{
			return this.GetInt("m_iID", 0);
		}
		public set (int iID)
		{
			this.SetInt("m_iID", iID);
		}
	}
	
	property bool m_bHasChildren
	{
		public get()
		{
			return this.GetBool("m_bHasChildren", false);
		}
		public set (bool bHasChildren)
		{
			this.SetBool("m_bHasChildren", bHasChildren);
		}
	}
	
	property int m_iParentID
	{
		public get()
		{
			return this.GetInt("m_iParentID", 0);
		}
		public set(int iParentID)
		{
			this.SetInt("m_iParentID", iParentID);
		}
	}
	
	property Handle m_hMenu
	{
		public get()
		{
			return this.GetHandle ("m_hMenu");
		}
		public set(Handle hMenu)
		{
			this.SetHandle("m_hMenu", hMenu);
		}
	}

	public bool GetName(char[] szBuffer, int iLength)
	{
		return this.GetString("Name", szBuffer, iLength);
	}
	
	public void SetName(const char[] szBuffer)
	{
		this.SetString("Name", szBuffer, 256);
	}
}
