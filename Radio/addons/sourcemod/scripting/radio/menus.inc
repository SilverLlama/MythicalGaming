/*
 * SourceMod Radio
 *
 * Adds a radio player to SourceMod that supports direct station links or streaming audio
 *
 * Coded by dubbeh - www.dubbeh.net
 *
 * Licensed under the GPLv3
 *
 */


void CreateRadioMenus()
{
	GetRadioStationsFromFile();
	CreateRadioBaseMenu();
	
	if (g_bUseGenres)
		CreateGenreMenus();
	
	if (g_cVarRadioPlayType.IntValue == RADIO_PLAY_TYPE_VOLUME_CONTROL || g_cVarRadioPlayType.IntValue == RADIO_PLAY_TYPE_SHOUTCAST)
		CreateRadioVolumeMenu();
	
	CreateStationMenus();
}

void CreateRadioBaseMenu()
{
	char szTranslation[64] = "";
	
	g_mRadioBaseMenu = new Menu(Handler_BaseRadioMenu);
	g_mRadioBaseMenu.SetTitle("SourceMod Radio:");
	
	if (g_bUseGenres)
	{
		Format(szTranslation, sizeof(szTranslation), "%T", "Select Genre", LANG_SERVER);
		
		g_mRadioBaseMenu.AddItem("1", szTranslation);
		
		// Check if we need to add volume control to the base menu
		if (g_cVarRadioPlayType.IntValue == RADIO_PLAY_TYPE_VOLUME_CONTROL || g_cVarRadioPlayType.IntValue == RADIO_PLAY_TYPE_SHOUTCAST)
		{
			Format(szTranslation, sizeof(szTranslation), "%T", "Volume", LANG_SERVER);
			g_mRadioBaseMenu.AddItem("2", szTranslation);
		}
		
		Format(szTranslation, sizeof(szTranslation), "%T", "Turn Off", LANG_SERVER);
		g_mRadioBaseMenu.AddItem("3", szTranslation);
		
	}
	else
	{
		Format(szTranslation, sizeof(szTranslation), "%T", "Select Station", LANG_SERVER);
		g_mRadioBaseMenu.AddItem("1", szTranslation);
		if (g_cVarRadioPlayType.IntValue == RADIO_PLAY_TYPE_VOLUME_CONTROL)
		{
			Format(szTranslation, sizeof(szTranslation), "%T", "Volume", LANG_SERVER);
			g_mRadioBaseMenu.AddItem("2", szTranslation);
		}
		Format(szTranslation, sizeof(szTranslation), "%T", "Turn Off", LANG_SERVER);
		g_mRadioBaseMenu.AddItem("3", szTranslation);
	}
}

public Handler_BaseRadioMenu(Menu mMenu, MenuAction maAction, int iClient, int iParam)
{
	if (maAction == MenuAction_Select)
	{
		char szSelection[32] = "";
		
		mMenu.GetItem(iParam, szSelection, sizeof(szSelection));
		
		if (g_bUseGenres)
		{
			if (szSelection[0] == '1')
			{
				if (g_mRadioBaseGenresMenu != INVALID_HANDLE)
					g_mRadioBaseGenresMenu.Display(iClient, MENU_TIME_FOREVER);
				else
					SetFailState("[SM-RADIO] Categories menu appears to be null. Please report this error to the author.");
			}
			else if (szSelection[0] == '2')
			{
				if (g_mRadioVolumeMenu != INVALID_HANDLE)
					g_mRadioVolumeMenu.Display(iClient, MENU_TIME_FOREVER);
				else
					SetFailState("[SM-RADIO] Volume menu handle appears to be invalid. Please report this error the author.");
			}
			else if (szSelection[0] == '3')
			{
				LoadOffPage(iClient);
			}
		}
		else
		{
			if (szSelection[0] == '1')
			{
				if (g_mRadioStationsMenu != INVALID_HANDLE)
					g_mRadioStationsMenu.Display(iClient, MENU_TIME_FOREVER);
				else
					SetFailState("[SM-RADIO] Station menu appears to be null. Please report this error to the author.");
			}
			// Volume control is only added if Radio Type > RADIO_PLAY_TYPE_DIRECT
			else if (szSelection[0] == '2')
			{
				if (g_mRadioVolumeMenu != INVALID_HANDLE)
					g_mRadioVolumeMenu.Display(iClient, MENU_TIME_FOREVER);
				else
					SetFailState("[SM-RADIO] Volume menu appears to be null. Please report this error to the author.");
			}
			else if (szSelection[0] == '3')
			{
				LoadOffPage(iClient);
			}
		}
	}
}

// Create the genre list menus
void CreateGenreMenus()
{
	char szTranslation[64];
	int iParentIndex = 0;
	char szParentGenreID[12] = "";
	char szParentGenreName[128] = "";
	int iChildIndex = 0;
	char szChildGenreID[12] = "";
	char szChildGenreName[128] = "";
	RadioGenre parentgenre;
	RadioGenre childgenre;
	
	g_mRadioBaseGenresMenu = new Menu(Handler_SelectGenre);
	
	Format(szTranslation, sizeof(szTranslation), "%T:", "Select Genre", LANG_SERVER);
	
	g_mRadioBaseGenresMenu.SetTitle(szTranslation);
	
	// Cycle through all the genres and check which are parents
	for (iParentIndex = 0; iParentIndex < g_Genres.Count; iParentIndex++)
	{
		parentgenre = g_Genres.Items(iParentIndex);
		
		// Is this is a parent genre with children?
		if (parentgenre.IsValid && parentgenre.m_bHasChildren)
		{
			// Add parent entry to base genres menu
			parentgenre.GetName(szParentGenreName, sizeof(szParentGenreName));
			IntToString(parentgenre.m_iID, szParentGenreID, sizeof(szParentGenreID));
			g_mRadioBaseGenresMenu.AddItem(szParentGenreID, szParentGenreName);
			
			// Initialize parent genre menu
			parentgenre.m_hMenu = new Menu(Handler_SelectGenre);
			SetMenuTitle(parentgenre.m_hMenu, szParentGenreName);
			
			// Append the child genres to the parent menus
			for (iChildIndex = 0; iChildIndex < g_Genres.Count; iChildIndex++)
			{
				childgenre = g_Genres.Items(iChildIndex);
				
				if (childgenre.IsValid && (parentgenre.m_iID == childgenre.m_iParentID))
				{
					IntToString(childgenre.m_iID, szChildGenreID, sizeof(szChildGenreID));
					childgenre.GetName(szChildGenreName, sizeof(szChildGenreName));
					AddMenuItem(parentgenre.m_hMenu, szChildGenreID, szChildGenreName);
				}
			}
			
			SetMenuExitBackButton(parentgenre.m_hMenu, true);
		}
	}
	
	g_mRadioBaseGenresMenu.ExitBackButton = true;
}

public Handler_SelectGenre(Menu mMenu, MenuAction maAction, int iClient, int iParam)
{
	char szGenreID[12] = "";
	char szGenreName[64] = "";
	RadioGenre genre;
	
	if (maAction == MenuAction_Cancel)
	{
		if (iParam == MenuCancel_ExitBack)
			g_mRadioBaseMenu.Display(iClient, MENU_TIME_FOREVER);
	}
	else if (maAction == MenuAction_Select)
	{
		mMenu.GetItem(iParam, szGenreID, sizeof(szGenreID));
		genre = g_Genres.Items(GetGenreIndexFromID(StringToInt(szGenreID)));
		
		if (genre.IsValid)
		{
			if (genre.m_hMenu != INVALID_HANDLE)
			{
				DisplayMenu(genre.m_hMenu, iClient, MENU_TIME_FOREVER);
			}
			else
			{
				genre.GetName(szGenreName, sizeof(szGenreName));
				SetFailState("[SM-RADIO] Error: Menu handle for genre \"%d\" appears to be invalid", szGenreName);
			}
		}
		else
		{
			SetFailState("[SM-RADIO] Error: Found an invalid Genre ID.");
		}
	}
}

/*
 * Append all the stations to each genre menu
 */
void CreateStationMenus()
{
	int iGenreIndex = 0;
	int iStationIndex = 0;
	RadioGenre genre;
	RadioStation station;
	char szStationID[16] = "";
	char szTranslation[64] = "";
	char szStationName[64] = "";
	
	if (g_bUseGenres)
	{
		for (iGenreIndex = 0; iGenreIndex < g_Genres.Count; iGenreIndex++)
		{
			genre = g_Genres.Items(iGenreIndex);
			if (genre.IsValid && !genre.m_bHasChildren)
			{
				genre.m_hMenu = new Menu(Handler_PlayRadioStation);
				Format(szTranslation, sizeof(szTranslation), "%T:", "Stations Menu Title", LANG_SERVER);
				SetMenuTitle(genre.m_hMenu, szTranslation);
			}
		}
		
		for (iStationIndex = 0; iStationIndex < g_Stations.Count; iStationIndex++)
		{
			station = g_Stations.Items(iStationIndex);
			
			// Make sure the station index is valid and also the station ID
			if ((station.IsValid) && (station.m_iID))
			{
				genre = g_Genres.Items(GetGenreIndexFromID(station.m_iGenreID));
				if (genre.IsValid && station.m_iGenreID == 0)
					LogMessage("[SM-RADIO] Error: Station ID \"%d\" has an invalid genre ID", station.m_iGenreID);
				
				station.GetName(szStationName, sizeof(szStationName));
				Format(szStationID, sizeof(szStationID), "%d", station.m_iID);
				AddMenuItem(genre.m_hMenu, szStationID, szStationName);
			}
			else
			{
				LogMessage("[SM-RADIO] Error: Found an invalid station in CreateStationMenus()");
			}
		}
		
		for (iGenreIndex = 0; iGenreIndex < g_Genres.Count; iGenreIndex++)
		{
			genre = g_Genres.Items(iGenreIndex);
			if (genre.IsValid && !genre.m_bHasChildren)
				SetMenuExitBackButton(genre.m_hMenu, true);
		}
	}
	else
	{
		g_mRadioStationsMenu = new Menu(Handler_PlayRadioStation);
		Format(szTranslation, sizeof(szTranslation), "%T:", "Stations Menu Title", LANG_SERVER);
		g_mRadioStationsMenu.SetTitle(szTranslation);
		
		for (iStationIndex = 0; iStationIndex < g_Stations.Count; iStationIndex++)
		{
			station = g_Stations.Items(iStationIndex);
			if (station.IsValid)
			{
				station.GetName(szStationName, sizeof(szStationName));
				Format(szStationID, sizeof(szStationID), "%d", station.m_iID);
				g_mRadioStationsMenu.AddItem(szStationID, szStationName);
			}
		}
		
		g_mRadioStationsMenu.ExitBackButton = true;
	}
}

public Handler_PlayRadioStation(Menu mMenu, MenuAction maAction, int iClient, int iParam)
{
	if (maAction == MenuAction_Cancel)
	{
		if (iParam == MenuCancel_ExitBack)
		{
			if (g_bUseGenres)
				g_mRadioBaseGenresMenu.Display(iClient, MENU_TIME_FOREVER);
			else
				g_mRadioBaseMenu.Display(iClient, MENU_TIME_FOREVER);
		}
	}
	else if (maAction == MenuAction_Select)
	{
		char szRadioStationID[16] = "";
		
		mMenu.GetItem(iParam, szRadioStationID, sizeof(szRadioStationID));
		LoadStation(iClient, szRadioStationID);
	}
}

void CreateRadioVolumeMenu()
{
	char szTranslation[64] = "";
	
	g_mRadioVolumeMenu = new Menu(Handler_VolumeMenu);
	
	Format(szTranslation, sizeof(szTranslation), "%T:", "Volume Menu Title", LANG_SERVER);
	g_mRadioVolumeMenu.SetTitle(szTranslation);
	g_mRadioVolumeMenu.AddItem("0.01", "1%");
	g_mRadioVolumeMenu.AddItem("0.05", "5%");
	g_mRadioVolumeMenu.AddItem("0.10", "10%");
	g_mRadioVolumeMenu.AddItem("0.20", "20%");
	g_mRadioVolumeMenu.AddItem("0.40", "40%");
	g_mRadioVolumeMenu.AddItem("0.60", "60%");
	g_mRadioVolumeMenu.AddItem("0.80", "80%");
	g_mRadioVolumeMenu.AddItem("1.0", "100%");
	g_mRadioVolumeMenu.ExitBackButton = true;
}

public Handler_VolumeMenu(Menu mMenu, MenuAction maAction, int iClient, int iParam)
{
	char szStationID[32] = "";
	char szStationVolume[32] = "";
	
	if (maAction == MenuAction_Cancel)
	{
		if (iParam == MenuCancel_ExitBack)
			g_mRadioBaseMenu.Display(iClient, MENU_TIME_FOREVER);
	}
	else if (maAction == MenuAction_Select)
	{
		mMenu.GetItem(iParam, szStationVolume, sizeof(szStationVolume));
		SetClientCookie(iClient, g_hCookieSavedVolume, szStationVolume);
		CPrintToChat(iClient, "%T", "Volume Set To", LANG_SERVER, szStationVolume);
		GetClientCookie(iClient, g_hCookieSavedStation, szStationID, sizeof(szStationID));
		
		if (szStationID[0])
			LoadStation(iClient, szStationID);
	}
}
