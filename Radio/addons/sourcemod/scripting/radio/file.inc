/*
 * SourceMod Radio
 *
 * Adds a radio player to SourceMod that supports direct station links or streaming audio
 *
 * Coded by dubbeh - www.dubbeh.net
 *
 * Licensed under the GPLv3
 *
 */


void GetRadioStationsFromFile()
{
	KeyValues kv = null;
	char szLocalPath[PLATFORM_MAX_PATH + 1];
	char szParentGenreName[64] = "";
	char szChildGenreName[64] = "";
	char szStationName[128] = "";
	int iParentGenreID = 0;
	int iParentGenreCount = 0;
	int iChildGenreID = 0;
	int iChildGenreCount = 0;
	bool bHasChildren = false;
	
	
	g_bStationsParsing = true;
	
	kv = new KeyValues("Radio Stations");
	
	if (g_cVarRadioPlayType.IntValue == RADIO_PLAY_TYPE_DIRECT)
		BuildPath(Path_SM, szLocalPath, sizeof(szLocalPath), "configs/%s", g_szRadioStationsDirectBaseFile);
	else if (g_cVarRadioPlayType.IntValue == RADIO_PLAY_TYPE_VOLUME_CONTROL)
		BuildPath(Path_SM, szLocalPath, sizeof(szLocalPath), "configs/%s", g_szRadioStationsVolumeBaseFile);
	else if (g_cVarRadioPlayType.IntValue == RADIO_PLAY_TYPE_SHOUTCAST)
		BuildPath(Path_SM, szLocalPath, sizeof(szLocalPath), "configs/%s", g_szRadioStationsShoutCastBaseFile);
	
	LogMessage("[SM-RADIO] Importing KeyValues from file \"%s\"", szLocalPath);
	
	if (kv.ImportFromFile(szLocalPath))
	{
		// This is incase we need to append Genre or Station ID's to the Key Values file
		// Keeps the index's persistant across server reboots when admins add new stations or genres without one.
		g_bWriteKVToDisk = false;
		
		kv.GetString("Off Page", g_szRadioOffPage, sizeof(g_szRadioOffPage), "about:blank");
		kv.GetString("Volume Wrapper", g_szVolumeWrapperURL, sizeof(g_szVolumeWrapperURL), "https://dubbeh.net/sm/rvw.html");
		kv.GetString("Browse Fix", g_szBrowseFixURL, sizeof(g_szBrowseFixURL), "https://dubbeh.net/sm/rbf.php");
		kv.GetString("Shoutcast Player", g_szShoutcastURL, sizeof(g_szShoutcastURL), "https://radio.dubbeh.net/index.php");
		g_iShoutCastPlaylistVersion = kv.GetNum("ShoutCast Version", 0);
		g_bUseGenres = view_as<bool>(kv.GetNum("Use Genres", 1));
		
		// Make sure we're always using genres in SHOUTCAST type
		if (g_cVarRadioPlayType.IntValue == RADIO_PLAY_TYPE_SHOUTCAST)
			g_bUseGenres = true;
		
		kv.GotoFirstSubKey(true);
		
		iParentGenreCount = 0;
		iChildGenreCount = 0;
		g_Genres = new RadioGenres();
		g_Stations = new RadioStations();
		
		do {
			if (g_bUseGenres)
			{
				kv.GetSectionName(szParentGenreName, sizeof(szParentGenreName));
				
				if (szParentGenreName[0])
				{
					iParentGenreID = kv.GetNum("Genre ID", 0);
					//LogMessage("[SM-Radio] Parent genre found: Name=%s GenreID=%d", szParentGenreName, iParentGenreID);
					
					IsGenreIDValid(kv, iParentGenreID);
					
					RadioGenre parentgenre = RadioGenre();
					parentgenre.m_iID = iParentGenreID;
					parentgenre.m_iParentID = 0;
					parentgenre.SetName(szParentGenreName);
					bHasChildren = view_as<bool>(kv.GetNum("Has Children", 0));
					parentgenre.m_bHasChildren = bHasChildren;
					g_Genres.AddItem(parentgenre);
					
					kv.GotoFirstSubKey(true);
					
					if (bHasChildren)
					{
						//iChildGenreCount = 0;
						// Run over the child genres
						do {
							kv.GetSectionName(szChildGenreName, sizeof(szChildGenreName));
							if (szChildGenreName[0])
							{
								RadioGenre childgenre = RadioGenre();
								iChildGenreID = kv.GetNum("Genre ID", 0);
								
								IsGenreIDValid(kv, iChildGenreID);
								childgenre.m_iID = iChildGenreID;
								childgenre.m_iParentID = iParentGenreID;
								childgenre.SetName(szChildGenreName);
								childgenre.m_bHasChildren = false;
								g_Genres.AddItem(childgenre);
								iChildGenreCount++;
								
								kv.GotoFirstSubKey(true);
								do {
									ParseStationEntry(kv, iChildGenreID);
								} while (kv.GotoNextKey(true));
								kv.GoBack();
							}
							else
							{
								LogMessage("[SM-RADIO] Child genre at index \"%d\" Has no name set. Please correct this error in the config file", iChildGenreCount + 1);
							}
						} while (kv.GotoNextKey(true));
						
						//LogMessage("[SM-Radio] Child genre(s) added to \"%s\" found: %d nodes", szParentGenreName, iChildGenreCount);
						kv.GoBack();
					}
					// Current key has no children - Parse each base key as a station.
					else
					{
						do {
							ParseStationEntry(kv, iParentGenreID);
						} while (kv.GotoNextKey(true));
						
						kv.GoBack();
					}
					
					iParentGenreCount++;
					
				}
				else
				{
					LogMessage("[SM-RADIO] Parent genre at index \"%d\" Has no name set. Please correct this error in the config file", iParentGenreCount + 1);
				}
			}
			else
			{
				if (kv.GetSectionName(szStationName, sizeof(szStationName)))
				{
					ParseStationEntry(kv, 0);
				}
			}
		} while (kv.GotoNextKey(true));
		
		kv.GoBack();
		
		// Invalid Genre or Station ID's found - Need to update with fixed version
		if (g_bWriteKVToDisk)
		{
			kv.Rewind();
			kv.ExportToFile(szLocalPath);
		}
	}
	else
	{
		LogMessage("[SM-RADIO] Error: unable to load \"%s\". Make sure you have the file in that location.", szLocalPath);
	}
	
	LogMessage("[SM-RADIO] Finishing parsing \"%s\" - Found \"%d\" parents and \"%d\" child genres", szLocalPath, iParentGenreCount, iChildGenreCount);
	
	g_bStationsParsing = false;
	delete kv;
}

void ParseStationEntry(KeyValues kv, int iGenreID)
{
	char szStationName[128] = "";
	char szStreamURL[128] = "";
	int iStationID = 0;
	
	kv.GetSectionName(szStationName, sizeof(szStationName));
	
	if (szStationName[0])
	{
		iStationID = kv.GetNum("Station ID", 0);
		
		// Does the current Station have an ID set?
		if (iStationID == 0)
		{
			// Station has no pre-defined index - need to write to disk
			iStationID = GetURandomInt();
			// Off chance we get 0
			if (iStationID == 0)
				iStationID = GetURandomInt();
			kv.SetNum("Station ID", iStationID);
			g_bWriteKVToDisk = true;
		}
		
		if (g_cVarRadioPlayType.IntValue != RADIO_PLAY_TYPE_SHOUTCAST)
			kv.GetString("Stream URL", szStreamURL, sizeof(szStreamURL), "");
		
		if (g_cVarRadioPlayType.IntValue != RADIO_PLAY_TYPE_SHOUTCAST && !szStreamURL[0])
		{
			g_bWriteKVToDisk = false;
			LogMessage("[SM-RADIO] Station \"%s\" has no Stream URL set. Please correct this problem and reload map.", szStationName);
			return;
		}
		
		RadioStation station = RadioStation();
		station.m_iID = iStationID;
		
		station.SetName(szStationName);
		if (szStreamURL[0])
			station.SetStreamURL(szStreamURL);
		station.m_iGenreID = iGenreID;
		station.m_iLastUpdateTime = 0;
		g_Stations.AddItem(station);
		//LogMessage("[SM-RADIO] Station found: StationID=%d GenreID=%d Name=%s", iStationID, iGenreID, szStationName);
	}
	else
	{
		g_bWriteKVToDisk = false;
		//LogMessage("[SM-RADIO] Station Section after station ID \"%d\" - Has no section name set. Please correct this error and reload the map", g_iStationCount + 1);
	}
}

void IsGenreIDValid(KeyValues kv, int iGenreID)
{
	// Does the current genre have an ID set?
	if (iGenreID == 0)
	{
		iGenreID = GetURandomInt();
		// Off chance we get 0
		if (iGenreID == 0)
			iGenreID = GetURandomInt();
		// Genre has no pre-defined index - need to write to disk
		kv.SetNum("Genre ID", iGenreID);
		g_bWriteKVToDisk = true;
	}
}
