/*
 * SourceMod Radio
 *
 * Adds a radio player to SourceMod that supports direct station links or streaming audio
 *
 * Coded by dubbeh - www.dubbeh.net
 *
 * Licensed under the GPLv3
 *
 */


public Action WelcomeAdvertTimer(Handle hTimer, any iClientSerial)
{
	int iClient;
	char szClientName[MAX_NAME_LENGTH] = "";
	
	
	iClient = GetClientFromSerial(iClientSerial);
	
	if (iClient && IsClientConnected(iClient) && IsClientInGame(iClient))
	{
		if (g_cVarWelcomeMsg.IntValue)
		{
			GetClientName(iClient, szClientName, sizeof(szClientName));
			CPrintToChat(iClient, "%T", "Welcome", LANG_SERVER, szClientName);
			CPrintToChat(iClient, "%T", "Radio Command Info", LANG_SERVER);
		}
		
		if (g_cVarAutoplay.IntValue && AreClientCookiesCached(iClient))
		{
			char szStationID[32] = "";
			
			GetClientCookie(iClient, g_hCookieSavedStation, szStationID, sizeof(szStationID));
			LoadStation(iClient, szStationID);
		}
		
		// Check if a client has cl_disablehtmlmotd set
		if (g_cVarCheckForHTMLMOTD.IntValue)
		{
			QueryClientConVar(iClient, "cl_disablehtmlmotd", CheckDisabledHTMLMOTD);
		}
	}
	
	return Plugin_Stop;
}

public Action ShoutcastUpdateListTimer(Handle hTimer)
{
	if (g_cVarRadioPlayType.IntValue == RADIO_PLAY_TYPE_SHOUTCAST)
	{
		// if the stations are currently parsing, let's make a small 60 seconds delayed 1 shot timer
		if (g_bStationsParsing)
		{
			CreateTimer(60.0, ShoutUpdateDelayedTimer, INVALID_HANDLE, TIMER_DATA_HNDL_CLOSE);
		}
		else
		{
			// Check if any updates are available
			UpdateShoutcastStationsList();
		}
	}
	
	return Plugin_Continue;
}

public Action ShoutUpdateDelayedTimer(Handle hTimer)
{
	if (g_bStationsParsing)
	{
		return Plugin_Continue;
	}
	else
	{
		UpdateShoutcastStationsList();
		return Plugin_Stop;
	}
}
