/*
 * SourceMod Radio
 *
 * Adds a radio player to SourceMod that supports direct station links or streaming audio
 *
 * Coded by dubbeh - www.dubbeh.net
 *
 * Licensed under the GPLv3
 *
 */


void UpdateShoutcastStationsList()
{
	if (STEAMWORKS_AVAILABLE()) {
		UpdateUsingSteamWorks();
	} else {
		LogMessage("[SM-RADIO] Unable to find SteamWorks for auto-updating playlists and keeping everything in sync.");
		LogMessage("[SM-RADIO] Please download from: https://forums.alliedmods.net/showthread.php?t=229556");
	}
}

/*
 * Updater using SteamWorks API Functions
 *
 * Thanks to KyleS for example code
 */

void UpdateUsingSteamWorks()
{
	Handle hHTTPRequest = INVALID_HANDLE;
	
	if (SteamWorks_IsLoaded())
	{
		if ((hHTTPRequest = SteamWorks_CreateHTTPRequest(k_EHTTPMethodGET, g_szShoutCastVersionURL)) != INVALID_HANDLE)
		{
			if (SteamWorks_SetHTTPCallbacks(hHTTPRequest, SteamWorks_OnVersionCheckComplete) && SteamWorks_SendHTTPRequest(hHTTPRequest))
			{
				LogMessage("[SM-RADIO] Running HTTP request using SteamWorks for URL=\"%s\"", g_szShoutCastVersionURL);
			}
			else
			{
				CloseHandle(hHTTPRequest);
				LogMessage("[SM-RADIO] Error: UpdateUsingSteamWorks() Problem sending HTTP request.");
			}
		} else {
			LogMessage("[SM-RADIO] Error creating HTTP request using SteamWorks for URL=\"%s\"", g_szShoutCastVersionURL);
		}
	} else {
		LogMessage("[SM-RADIO] Error querying SteamWorks_IsLoaded(). Please check the plugin is installed correctly and functioning.");
	}
	
	return;
}

public void SteamWorks_OnVersionCheckComplete(Handle hRequest, bool bFailure, bool bRequestSuccessful, EHTTPStatusCode eStatusCode)
{
	if (!bFailure && bRequestSuccessful && eStatusCode == k_EHTTPStatusCode200OK)
	{
		SteamWorks_GetHTTPResponseBodyCallback(hRequest, SteamWorks_VersionAPIWebResponse);
	} else {
		LogMessage("[SM-RADIO] SteamWorks_OnVersionCheckComplete() Error: Response code %d", eStatusCode);
	}
	
	CloseHandle(hRequest);
}

public void SteamWorks_VersionAPIWebResponse(const char[] szResponseBuffer)
{
	int iOnlinePlaylistVersion = 0;
	Handle hHTTPRequest = INVALID_HANDLE;
	
	// Check current playlist against web playlist version
	iOnlinePlaylistVersion = StringToInt(szResponseBuffer);
	
	if (iOnlinePlaylistVersion > g_iShoutCastPlaylistVersion)
	{
		if ((hHTTPRequest = SteamWorks_CreateHTTPRequest(k_EHTTPMethodGET, g_szShoutCastPlaylistURL)) != INVALID_HANDLE)
		{
			if (SteamWorks_SetHTTPCallbacks(hHTTPRequest, SteamWorks_OnPlaylistTransferComplete) && SteamWorks_SendHTTPRequest(hHTTPRequest))
			{
				g_iShoutCastPlaylistVersion = iOnlinePlaylistVersion;
			} else {
				CloseHandle(hHTTPRequest);
				LogMessage("[SM-RADIO] Error creating HTTP callbacks using SteamWorks.");
			}
		} else {
			LogMessage("[SM-RADIO] Error creating HTTP request using SteamWorks for URL=\"%s\"", g_szShoutCastVersionURL);
		}
	} else {
		LogMessage("[SM-RADIO] Online playlist matches local one. No need to update.");
	}
}

public void SteamWorks_OnPlaylistTransferComplete(Handle hRequest, bool bFailure, bool bRequestSuccessful, EHTTPStatusCode eStatusCode)
{
	char szPlaylistPath[PLATFORM_MAX_PATH + 1] = "";
	
	if (!bFailure && bRequestSuccessful && eStatusCode == k_EHTTPStatusCode200OK)
	{
		BuildPath(Path_SM, szPlaylistPath, sizeof(szPlaylistPath), "configs/%s", g_szRadioStationsShoutCastBaseFile);
		
		if (SteamWorks_WriteHTTPResponseBodyToFile(hRequest, szPlaylistPath))
		{
			CPrintToChatAll("[SM-RADIO] ShoutCast station list updated. Please reload the map to apply changes.");
			LogMessage("[SM-RADIO] ShoutCast station list updated. Please reload the map to apply changes.");
		}
	}
	
	CloseHandle(hRequest);
}


