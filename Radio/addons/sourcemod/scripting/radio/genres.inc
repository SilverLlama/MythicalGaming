#if defined _dynamic_collection_radiogenres_
  #endinput
#endif
#define _dynamic_collection_radiogenres_


methodmap RadioGenres < Collection
{
	public RadioGenres()
	{
		return view_as<RadioGenres>(new Collection());
	}
	
	public RadioGenre Items(int index)
	{
		return view_as<RadioGenre>(this.Items(index));
	}
}
