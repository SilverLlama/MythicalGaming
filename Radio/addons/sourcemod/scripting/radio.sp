/*
 * SourceMod Radio
 *
 * Adds a radio player to SourceMod that supports direct station links or streaming audio
 *
 * Coded by dubbeh - www.dubbeh.net
 *
 * Licensed under the GPLv3
 *
 */

#pragma semicolon 1

#include <sourcemod>
#include <clientprefs>
#include <multicolors>
#include <dynamic>
#include <dynamic-collection>

//#pragma newdecls required

#undef REQUIRE_PLUGIN
#undef REQUIRE_EXTENSIONS
#include <updater>
#include <SteamWorks>
#define REQUIRE_EXTENSIONS
#define REQUIRE_PLUGIN

// Methodmaps for Dynamic - Thanks Neuro Toxin
#include "radio\genre.inc"
#include "radio\genres.inc"
#include "radio\station.inc"
#include "radio\stations.inc"


#define PLUGIN_NAME					"Radio"
#define PLUGIN_VERSION				"2.0.0.0 BETA 2"
#define MAX_MOTD_URL_SIZE			192 // Maximum MOTD message size is 192 bytes


#define RADIO_PLAY_TYPE_DIRECT 			1
#define RADIO_PLAY_TYPE_VOLUME_CONTROL 	2
#define RADIO_PLAY_TYPE_SHOUTCAST		3


#define TRACK_UPDATE_TIME 60


// Thanks to GoD-Tony for this macro
#define STEAMWORKS_AVAILABLE()	(GetFeatureStatus(FeatureType_Native, "SteamWorks_IsLoaded") == FeatureStatus_Available)

public Plugin myinfo = 
{
	name = PLUGIN_NAME, 
	author = "dubbeh", 
	description = "Radio stations plugin for SourceMod - Powered by ShoutCast.com", 
	version = PLUGIN_VERSION, 
	url = "www.dubbeh.net"
};

/* cVar Handles */
ConVar g_cVarEnable = null;
ConVar g_cVarStationAdvert = null;
ConVar g_cVarWelcomeMsg = null;
ConVar g_cVarLogging = null;
ConVar g_cVarAutoplay = null;
ConVar g_cVarInitialConnectVolume = null;
ConVar g_cVarAutoUpdate = null;
ConVar g_cVarCheckForHTMLMOTD = null;
ConVar g_cVarRadioPlayType = null;
ConVar g_cVarShoutCastCustom = null;

/* Static menu handles */
Menu g_mRadioBaseMenu = null;
Menu g_mRadioBaseGenresMenu = null;
Menu g_mRadioStationsMenu = null;
Menu g_mRadioVolumeMenu = null;

/* Configs and playlist type file names */
char g_szRadioStationsDirectBaseFile[] = "radiodirect.txt";
char g_szRadioStationsVolumeBaseFile[] = "radiovolume.txt";
char g_szRadioStationsShoutCastBaseFile[] = "radioshoutcast.txt";
char g_szConfigFile[] = "sourcemod/plugin.radio.cfg";

/* URL's for various things */
char g_szRadioOffPage[MAX_MOTD_URL_SIZE + 1] = "about:blank";
char g_szVolumeWrapperURL[MAX_MOTD_URL_SIZE + 1] = "https://dubbeh.net/sm/rvw.html";
char g_szBrowseFixURL[MAX_MOTD_URL_SIZE + 1] = "https://dubbeh.net/sm/rbf.php";
char g_szUpdateURL[] = "https://update.dubbeh.net/radio2/radio.txt";
char g_szShoutcastURL[] = "https://radio.dubbeh.net/index.php";
char g_szShoutcastQueryURL[] = "https://radio.dubbeh.net/query.php";
char g_szShoutCastVersionURL[] = "https://radio.dubbeh.net/stationversion.txt";
char g_szShoutCastPlaylistURL[] = "https://radio.dubbeh.net/radiostations.txt";


/* Dynamic global collections */
RadioGenres g_Genres;
RadioStations g_Stations;

/* Cookie handles */
Handle g_hCookieSavedStation = INVALID_HANDLE;
Handle g_hCookieSavedVolume = INVALID_HANDLE;

/* Timer handles */
Handle g_hUpdateTimer = INVALID_HANDLE;

/* Everything else */
int g_iShoutCastPlaylistVersion = 0;
EngineVersion g_EngineVersion = Engine_Unknown;
bool g_bUpdaterAvail = false;
bool g_bUseGenres = true;
bool g_bWriteKVToDisk = false;
bool g_bStationsParsing = false;


#include "radio/helpers.inc"
#include "radio/menus.inc"
#include "radio/commands.inc"
#include "radio/file.inc"
#include "radio/timers.inc"
#include "radio/track_query.inc"
#include "radio/updater.inc"

public void OnPluginStart()
{
	CreateConVar("sm_radio_version", PLUGIN_VERSION, "SourceMod Radio version", FCVAR_SPONLY | FCVAR_REPLICATED | FCVAR_NOTIFY | FCVAR_DONTRECORD);
	g_cVarEnable = CreateConVar("sm_radio_enable", "1.0", "Enable SourceMod Radio", 0, true, 0.0, true, 1.0);
	g_cVarStationAdvert = CreateConVar("sm_radio_advert", "1.0", "Enable advertising the users radio station choice", 0, true, 0.0, true, 1.0);
	g_cVarWelcomeMsg = CreateConVar("sm_radio_welcome", "1.0", "Enable the welcome message", 0, true, 0.0, true, 1.0);
	g_cVarLogging = CreateConVar("sm_radio_logging", "1.0", "Enable logging to the server console and log file", 0, true, 0.0, true, 1.0);
	g_cVarAutoplay = CreateConVar("sm_radio_autoplay", "1.0", "Enable auto playing a users radio choice on map change", 0, true, 0.0, true, 1.0);
	g_cVarInitialConnectVolume = CreateConVar("sm_radio_init_conn_vol", "0.10", "Initial volume set for a user on first connect", 0, true, 0.01, true, 1.0);
	g_cVarAutoUpdate = CreateConVar("sm_radio_auto_update", "1.0", "Enable the plugin to automatically update (if available updates found)", 0, true, 0.0, true, 1.0);
	g_cVarCheckForHTMLMOTD = CreateConVar("sm_radio_check_html_motd", "1.0", "Enable to check if \"cl_disablehtmlmotd\" is enabled and warn the user", 0, true, 0.0, true, 1.0);
	g_cVarRadioPlayType = CreateConVar("sm_radio_play_type", "3.0", "Play type for Radio, 1 = Direct, 2 = Volume Wrapper, 3 = ShoutCast Powered", 0, true, 1.0, true, 3.0);
	// TODO: This will be added in BETA 3
	g_cVarShoutCastCustom = CreateConVar("sm_radio_shoutcast_custom", "1.0", "Append custom stations using the wrapper page under ShoutCast mode", 0, true, 0.0, true, 1.0);
	
	if ((g_cVarEnable == null) || 
		(g_cVarStationAdvert == null) || 
		(g_cVarWelcomeMsg == null) || 
		(g_cVarLogging == null) || 
		(g_cVarAutoplay == null) || 
		(g_cVarInitialConnectVolume == null) || 
		(g_cVarAutoUpdate == null) || 
		(g_cVarCheckForHTMLMOTD == null) || 
		(g_cVarRadioPlayType == null) ||
		(g_cVarShoutCastCustom == null))
	SetFailState("[SM-RADIO] Error - Unable to create a console variable");
	
	LoadTranslations("radio.phrases");
	
	RegConsoleCmd("sm_radio", Command_Radio);
	RegConsoleCmd("sm_radiooff", Command_RadioOff);
	RegConsoleCmd("sm_browse", Command_Browse);
	RegConsoleCmd("sm_volume", Command_Volume);
	RegConsoleCmd("sm_track", Command_Track);
	RegConsoleCmd("sm_song", Command_Track);
	RegConsoleCmd("sm_genres", Command_Genres);
	RegConsoleCmd("sm_genre", Command_Genres);
	
	g_hCookieSavedStation = RegClientCookie("Radio Saved Station v2", "Radio Saved Station Version 2", CookieAccess_Private);
	g_hCookieSavedVolume = RegClientCookie("Radio Saved Volume v2", "Radio Saved Volume Version 2", CookieAccess_Private);
	
	g_EngineVersion = GetEngineVersion();
	
	if (g_EngineVersion == Engine_CSGO)
	{
		if (g_cVarLogging.IntValue) { LogMessage("[SM-RADIO] Running under CS:GO detected"); }
	}
	else if (g_EngineVersion == Engine_Unknown)
	{
		if (g_cVarLogging.IntValue) { LogMessage("[SM-RADIO] Unknown mod found. Please contact the SourceMod authors for support."); }
		SetFailState("[SM-RADIO] Unknown mod found. Please contact the SourceMod authors for support");
	}
	
	// Auto create the config file, if it doesn't exist
	AutoExecConfig(true, "plugin.radio", "sourcemod");
	
	ServerCommand("exec %s", g_szConfigFile);
	
	// Create the radioshoutcast.txt auto-updater schedule - This repeats every hour
	g_hUpdateTimer = CreateTimer(3600.0, ShoutcastUpdateListTimer, INVALID_HANDLE, TIMER_REPEAT);
	if (g_hUpdateTimer == INVALID_HANDLE)
		SetFailState("[SM-RADIO] Error: Unable to create the ShoutCast update list timer.");
}

public OnAllPluginsLoaded()
{
	if (!LibraryExists("dynamic"))
		SetFailState("[SM-RADIO] Unable to find Dynamic (Plugin cannot function without it). Please install from https://forums.alliedmods.net/showthread.php?t=270519");
	
	g_bUpdaterAvail = LibraryExists("updater");
	if (!g_bUpdaterAvail)
		SetFailState("[SM-RADIO] Plugin unable to run without updater installed during the ALPHA/BETA test phase.");
	
	if (STEAMWORKS_AVAILABLE())
	{
		LogMessage("[SM-RADIO] SteamWorks extension found.");
	}
}

public OnLibraryAdded(const char[] name)
{
	if (LibraryExists("updater"))
	{
		g_bUpdaterAvail = true;
	}
}

public OnLibraryRemoved(const char[] name)
{
	if (StrEqual(name, "updater"))
	{
		g_bUpdaterAvail = false;
	}
}

public void OnPluginEnd()
{
	PluginCleanup();
	if (g_hUpdateTimer != INVALID_HANDLE)
		CloseHandle(g_hUpdateTimer);
}

public void OnMapStart()
{
	// Make sure the config file is executed
	ServerCommand("exec %s", g_szConfigFile);
}

// Moved from OnMapStart to OnConfigsExecuted for plugin intialization (Much more reliable here)
public void OnConfigsExecuted()
{
	// Load the stations key values file and create the menus
	CreateRadioMenus();
	
	if (g_cVarAutoUpdate.IntValue)
	{
		if (g_bUpdaterAvail)
		{
			Updater_AddPlugin(g_szUpdateURL);
		}
		else
		{
			LogMessage("[SM-RADIO] Auto-Updating enabled but unable to find the SourceMod Updater plugin.");
			LogMessage("[SM-RADIO] Download it from : https://forums.alliedmods.net/showthread.php?t=169095");
		}
	}
}

public void OnMapEnd()
{
	PluginCleanup();
}

public void OnClientPostAdminCheck(int iClient)
{
	if ((!iClient) || !g_cVarEnable.IntValue || !IsClientConnected(iClient) || IsFakeClient(iClient))
		return;
	
	// Extra check here - Just incase we don't need to waste a thread
	if (g_cVarWelcomeMsg.IntValue || g_cVarAutoplay.IntValue || g_cVarCheckForHTMLMOTD.IntValue)
		CreateTimer(30.0, WelcomeAdvertTimer, GetClientSerial(iClient));
}

// Thanks to psychonic for the cl_disablehtmlmotd query code - modified it a little
public CheckDisabledHTMLMOTD(QueryCookie qcCookie, iClient, ConVarQueryResult cqrResult, const char[] szCvarName, const char[] szCvarValue)
{
	if (g_cVarEnable.IntValue)
	{
		if (cqrResult == ConVarQuery_Okay && StringToInt(szCvarValue) == 1 || cqrResult != ConVarQuery_Okay)
		{
			CPrintToChat(iClient, "%T", "Disabled HTML MOTD On", LANG_SERVER);
		}
	}
}

/* Thanks to GoD-Tony for this - Modified for SteamWorks */
public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	// SteamWorks Natives
	MarkNativeAsOptional("SteamWorks_IsLoaded");
	MarkNativeAsOptional("SteamWorks_CreateHTTPRequest");
	MarkNativeAsOptional("SteamWorks_GetHTTPResponseBodyCallback");
	MarkNativeAsOptional("SteamWorks_SetHTTPCallbacks");
	MarkNativeAsOptional("SteamWorks_SetHTTPRequestHeaderValue");
	MarkNativeAsOptional("SteamWorks_SendHTTPRequest");
	MarkNativeAsOptional("SteamWorks_WriteHTTPResponseBody");
	MarkNativeAsOptional("SteamWorks_ReleaseHTTPRequest");
	
	return APLRes_Success;
} 