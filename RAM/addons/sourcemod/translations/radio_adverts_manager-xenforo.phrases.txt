"Phrases"
{
	"RegisterForums"
	{
		"en"			"{lime}You need to register to our forums \"www.fotg.net\" to get access to this feature."
		"de"			"{lime}Du musst dich leider in unserem Forum \"www.fotg.net\" registrieren um dies nutzen zu können."
	}
	"AdvertStarted"
	{
		"en"			"{lime}Players get rewarded with 'coins' for each sec. they allow the advert. More info visit: \"www.fotg.net\""
		"de"			"{lime}Spieler bekommen 'coins' für jede Sekunde, die sie werbung zulassen. Mehr Informationen unter \"www.fotg.net\""
	}
	"AdvertEnded"
	{
		"#format"		"{1:d}"
		"en"			"{purple}The adverts has been stopped, you got {1} coins, thanks for supportung our server."
		"de"			"{purple}Die Werbung wurde beendet, du bekommst {1} coins für deine Unterstützung der Server."
	}
}