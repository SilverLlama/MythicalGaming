#include <sourcemod>
#include <store>
#include <radio_adverts_manager>

Handle AbuseTimer[MAXPLAYERS+1];

public int RAM_OnClientAdvertFinished(int client, int provider, int time, bool abort)
{
	
	//PrintToChatAll("client: %i, provider: %i, time:%i, abort: %b", client, provider, time, abort);
	if (AbuseTimer[client] != null)
		return;
	
	if(time <= 10)
		return;

	if(!Store_IsClientLoaded(client))
		return;
	AbuseTimer[client] = CreateTimer(300.0, AbuseStop, client);
	Store_SetClientCredits(client, Store_GetClientCredits(client) + 10);
	PrintToChat(client, "[\x0EMythical Ads\x01] You got \x0410 \x01credits for finishing the advertisement!");
}

public Action AbuseStop(Handle timer, any client)
{
	AbuseTimer[client] = null;
}