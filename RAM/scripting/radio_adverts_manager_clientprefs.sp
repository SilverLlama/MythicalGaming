#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "Radio Adverts Manager - Clientprefs"
#define PLUGIN_VERSION "1.0"
#define PLUGIN_DESCRIPTION ""
#define PLUGIN_URL "https://gitlab.com/Zipcore/RadioAdvertsManager"

#include <sourcemod>
#include <smlib>
#include <multicolors>
#include <clientprefs>
#include <radio_adverts_manager>

#define LoopIngameClients(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1))

Handle g_hRadioVolume = null;
Handle g_hRadioChannel = null;
Handle g_hAdvertsMode = null;

public void OnPluginStart()
{
	g_hRadioVolume = RegClientCookie("ram_radio_volume", "", CookieAccess_Protected);
	g_hRadioChannel = RegClientCookie("ram_radio_channel", "", CookieAccess_Protected);
	g_hAdvertsMode = RegClientCookie("ram_adverts_mode", "", CookieAccess_Protected);
	
	// Late load Support
	LoopIngameClients(iClient)
	{
		OnClientCookiesCached(iClient);
	}
}

public void OnClientCookiesCached(int iClient)
{
	if(IsFakeClient(iClient))
		return;
	
	char sBuffer[16];

	GetClientCookie(iClient, g_hRadioVolume, sBuffer, sizeof(sBuffer));
	if(!StrEqual(sBuffer, ""))
		RAM_SetClientRadioVolume(iClient, StringToInt(sBuffer));

	GetClientCookie(iClient, g_hRadioChannel, sBuffer, sizeof(sBuffer));
	if(!StrEqual(sBuffer, ""))
		RAM_SetClientRadioChannel(iClient, StringToInt(sBuffer));

	GetClientCookie(iClient, g_hAdvertsMode, sBuffer, sizeof(sBuffer));
	if(!StrEqual(sBuffer, ""))
		RAM_SetClientAdvertsMode(iClient, StringToInt(sBuffer));
}

public int RAM_OnClientRadioVolumeChanged(int iClient, int newvalue, int oldvalue)
{
	char sBuffer[16];
	IntToString(newvalue, sBuffer, sizeof(sBuffer));
	
	SetClientCookie(iClient, g_hRadioVolume, sBuffer);
}

public int RAM_OnClientRadioChannelChanged(int iClient, int newvalue, int oldvalue)
{
	char sBuffer[16];
	IntToString(newvalue, sBuffer, sizeof(sBuffer));
	
	SetClientCookie(iClient, g_hRadioChannel, sBuffer);
}

public int RAM_OnClientAdvertsModeChanged(int iClient, int newvalue, int oldvalue)
{
	char sBuffer[16];
	IntToString(newvalue, sBuffer, sizeof(sBuffer));
	
	SetClientCookie(iClient, g_hAdvertsMode, sBuffer);
}