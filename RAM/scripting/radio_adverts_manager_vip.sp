#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "Radio Adverts Manager - VIP"
#define PLUGIN_VERSION "1.0"
#define PLUGIN_DESCRIPTION ""
#define PLUGIN_URL "https://gitlab.com/Zipcore/RadioAdvertsManager"

#include <sourcemod>
#include <clientprefs>
#include <smlib>
#include <multicolors>
#include <radio_adverts_manager>

#define LoopIngameClients(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1))

public void OnPluginStart()
{
	// Late load Support
	LoopIngameClients(iClient)
	{
		OnClientCookiesCached(iClient);
	}
}

public int RAM_OnClientAdvertsModeChanged(int iClient, int newvalue, int oldvalue)
{
	if(newvalue != 0 || Client_HasAdminFlags(iClient, ADMFLAG_CUSTOM1))
		return;
	
	RAM_SetClientAdvertsMode(iClient, oldvalue);
	CPrintToChat(iClient, "{green}Only VIPs are allowed to disable adverts completly.");
}

public void OnClientCookiesCached(int iClient)
{
	if(IsFakeClient(iClient))
		return;
	
	CreateTimer(0.0, Timer_Check, GetClientUserId(iClient));
}

// Enable adverts if client is not VIP anymore

public Action Timer_Check(Handle timer, any userid)
{
	int iClient = GetClientOfUserId(userid);
	
	if(iClient > 0 && !Client_HasAdminFlags(iClient, ADMFLAG_CUSTOM1) && RAM_GetClientAdvertsMode(iClient) == 0)
	{
		RAM_SetClientAdvertsMode(iClient, 2);
	}
	
	return Plugin_Handled;
}