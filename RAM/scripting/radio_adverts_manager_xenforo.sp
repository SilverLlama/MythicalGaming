#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "Radio Adverts Manager - Xenforo"
#define PLUGIN_VERSION "1.0"
#define PLUGIN_DESCRIPTION ""
#define PLUGIN_URL "https://gitlab.com/Zipcore/RadioAdvertsManager"

#include <sourcemod>
#include <multicolors>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>
#include <geoip>
#include <radio_adverts_manager>
#include <xenforo_api>

#define LoopClients(%1) for(int %1 = 1; %1 <= MaxClients; %1++)

#define LoopIngameClients(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1))

#define LoopIngamePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && !IsFakeClient(%1))

#define LoopAlivePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && IsPlayerAlive(%1))

ConVar g_cAdvertsModeXenforo = null;
ConVar g_cChannelXenforo = null;

ConVar g_cCoinsAdvert = null;
ConVar g_cCoinsPlaytime = null;
ConVar g_cCoinsXenforo = null;

float g_fCoins2Add[MAXPLAYERS + 1];

/* Cache */

int g_iUserID[MAXPLAYERS+1];

bool g_bUserBanned[MAXPLAYERS + 1];

bool g_bUserFemale[MAXPLAYERS + 1];
int g_iUserLikes[MAXPLAYERS + 1];
int g_iUserTrophyPoints[MAXPLAYERS + 1];
int g_iUserAlertsUnread[MAXPLAYERS + 1];

Handle g_aUserGroups[MAXPLAYERS + 1];
Handle g_aUserFieldNames[MAXPLAYERS+1] = {null, ...};
Handle g_aUserFieldSettings[MAXPLAYERS+1] = {null, ...};

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	RegPluginLibrary("radio_adverts_manager-xenforo");
	
	return APLRes_Success;
}

public void OnPluginStart()
{
	LoadTranslations("radio_adverts_manager-xenforo.phrases");
	
	g_cCoinsAdvert = CreateConVar("ram_coins_advert", "5.0", "Coins for watching an add each second.");
	g_cCoinsPlaytime = CreateConVar("ram_coins_playtime", "0.0832158", "Coins for playing on the server each second.");
	g_cCoinsXenforo = CreateConVar("ram_coins_xenforo", "coins", "Xenforo custom user field name for coins");
	
	g_cAdvertsModeXenforo = CreateConVar("ram_adverts_mode_xenforo", "advert_mode", "Xenforo custom user field name for adverts mode");
	g_cChannelXenforo = CreateConVar("ram_radio_channel_xenforo", "radio_channel", "Xenforo custom user field name for radio channel");
}

public void OnMapStart()
{
	CreateTimer(1.0, Timer_Check, _, TIMER_REPEAT | TIMER_FLAG_NO_MAPCHANGE);
}

public void OnClientDisconnect(int iClient)
{
	g_bUserFemale[iClient] = false;
	g_iUserLikes[iClient] = 0;
	g_iUserTrophyPoints[iClient] = 0;
	g_iUserAlertsUnread[iClient] = 0;
	
	g_aUserGroups[iClient] = null;
	g_aUserFieldNames[iClient] = null;
	g_aUserFieldSettings[iClient] = null;
}

public int XF_OnProcessed(int iClient, int userID, bool banned, bool admin, bool moderator, bool staff, int register, bool female, int likes, int points, int alerts, Handle groups, Handle fields, Handle settings)
{
	g_iUserID[iClient] = userID;
	
	g_bUserBanned[iClient] = banned;
	g_bUserFemale[iClient] = female;
	
	g_iUserLikes[iClient] = likes;
	g_iUserTrophyPoints[iClient] = points;
	g_iUserAlertsUnread[iClient] = alerts;
	
	g_aUserGroups[iClient] = groups;
	g_aUserFieldNames[iClient] = fields;
	g_aUserFieldSettings[iClient] = settings;
	
	int iIndex = -1;
	char sBuffer[512];
	char sSetting[512];
	
	/* Coins */
	
	g_cCoinsXenforo.GetString(sBuffer, sizeof(sBuffer));
	iIndex = FindStringInArray(g_aUserFieldNames[iClient], sBuffer);
	
	if(iIndex != -1)
	{
		// IDK yet
	}
	else CreateField(userID, sBuffer);
	
	/* Radio Channel */
	
	g_cChannelXenforo.GetString(sBuffer, sizeof(sBuffer));
	iIndex = FindStringInArray(g_aUserFieldNames[iClient], sBuffer);
	
	if(iIndex != -1)
	{
		GetArrayString(g_aUserFieldSettings[iClient], iIndex, sSetting, sizeof(sSetting));
		RAM_SetClientRadioChannel(iClient, StringToInt(sSetting));
	}
	else CreateField(userID, sBuffer);
	
	/* Adverts Mode */
	
	g_cAdvertsModeXenforo.GetString(sBuffer, sizeof(sBuffer));
	iIndex = FindStringInArray(g_aUserFieldNames[iClient], sBuffer);
	
	if(iIndex != -1)
	{
		GetArrayString(g_aUserFieldSettings[iClient], iIndex, sSetting, sizeof(sSetting));
		RAM_SetClientAdvertsMode(iClient, StringToInt(sSetting))
	}
	else CreateField(userID, sBuffer);
}

public Action Timer_Check(Handle timer, any data)
{
	// Give coins
	
	char sCoins[32];
	g_cCoinsXenforo.GetString(sCoins, sizeof(sCoins));
	
	LoopIngameClients(iClient)
	{
		if(!XenForo_IsProcessed(iClient))
			continue;
		
		if(GetClientTeam(iClient) == 2 || GetClientTeam(iClient) == 3)
			g_fCoins2Add[iClient] += g_cCoinsPlaytime.FloatValue;
		
		if(g_fCoins2Add[iClient] > 1.0)
		{
			char sQuery[256];
			Format(sQuery, sizeof(sQuery), "UPDATE `xf_user_field_value` SET `field_value`=`field_value`+'%d' WHERE `user_id`='%d' AND `field_id`='%s'", g_fCoins2Add[iClient], XenForo_GrabClientID(iClient), sCoins);
			
			XenForo_TExecute(sQuery);
			
			g_fCoins2Add[iClient] = 0.0;
		}
	}
	
	return Plugin_Continue;
}

void CreateField(int userID, char[] name)
{
	char sQuery[256];
	Format(sQuery, sizeof(sQuery), "INSERT INTO `xf_user_field_value` (`user_id`, `field_id`, `field_value`) VALUES (%d, '%s', 0);", userID, name);
	XenForo_TExecute(sQuery);
}

int SetFieldInt(int iClient, char[] field, int value)
{
	if(!XenForo_IsProcessed(iClient))
		return -1;
	
	return XenForo_UpdateFieldInt(iClient, field, value);
}

void MsgRegisterForums(int iClient)
{
	CPrintToChat(iClient, "%T", "RegisterForums", iClient);
}

/* RAM Forwards */

public int RAM_OnClientAdvertStarted(int iClient, int iProvider)
{
	CPrintToChat(iClient, "%T", "AdvertStarted", iClient);
}

public int RAM_OnClientAdvertFinished(int iClient, int iProvider, int time, bool abort)
{
	int iTime = GetTime();
	
	int iCoins = g_cCoinsAdvert.IntValue * iTime;
	
	g_fCoins2Add[iClient] += float(iCoins);
	
	if(iCoins > 0)
		CPrintToChat(iClient, "%T", "AdvertEnded", iClient, iCoins);
}

public int RAM_OnClientRadioChannelChanged(int iClient, int newvalue, int oldvalue)
{
	char sBuffer[512];
	g_cChannelXenforo.GetString(sBuffer, sizeof(sBuffer));
	SetFieldInt(iClient, sBuffer, newvalue);
}

public int RAM_OnClientAdvertsModeChanged(int iClient, int newvalue, int oldvalue)
{
	char sBuffer[512];
	g_cAdvertsModeXenforo.GetString(sBuffer, sizeof(sBuffer));
	SetFieldInt(iClient, sBuffer, newvalue);
}

public Action RAM_OnClientAccessAdvertSettings(int iClient)
{
	if(!XenForo_IsProcessed(iClient))
	{
		MsgRegisterForums(iClient);
		return Plugin_Handled;
	}
	
	return Plugin_Continue;
}