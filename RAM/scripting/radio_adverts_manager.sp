#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "Radio Adverts Manager"
#define PLUGIN_VERSION "1.0"
#define PLUGIN_DESCRIPTION "Radio Channel and Adverts Manager"
#define PLUGIN_URL "https://gitlab.com/Zipcore/RadioAdvertsManager"

#include <sourcemod>
#include <multicolors>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>
#include <geoip>
#include <radio_adverts_manager>

/* Late included code parts */

#pragma newdecls required

char g_sGameDir[255];
char g_sServerIP[255];
int g_iServerPort;
	
Handle g_OnClientShouldShowAdvert = null;
Handle g_OnClientAccessAdvertSettings = null;
Handle g_OnClientAdvertStarted = null;
Handle g_OnClientAdvertFinished = null;
Handle g_OnClientRadioTurnOn = null;
Handle g_OnClientRadioTurnOff = null;
Handle g_OnClientRadioResumed = null;
Handle g_OnClientRadioChannelChanged = null;
Handle g_OnClientRadioVolumeChanged = null;
Handle g_OnClientAdvertsModeChanged = null;

Handle g_aChannelNames = null;
Handle g_aChannelUrls = null;

int g_iLastMessage[MAXPLAYERS + 1];
bool g_bVGUI[MAXPLAYERS + 1];

bool g_bRadio[MAXPLAYERS + 1];
bool g_bRadioPaused[MAXPLAYERS + 1];

bool g_bAdvert[MAXPLAYERS + 1];
int g_iAdvertStarted[MAXPLAYERS + 1];
int g_iLastAdvertStarted[MAXPLAYERS + 1];

int g_iRadioChannel[MAXPLAYERS + 1] = {0, ...};
int g_iRadioVolume[MAXPLAYERS + 1] = {40, ...};
int g_iAdvertsMode[MAXPLAYERS + 1] = {3, ...};

Handle g_hAdvertTimers[MAXPLAYERS + 1] = {null, ...};

Handle g_aAdvertCountrys = null;

/* Advert Modes */

char g_sAdvertsModePhrases[][] = {
	"Adverts_None",
	"Adverts_Min",
	"Adverts_Mid",
	"Adverts_Max"
};

#include "radio_adverts_manager/stocks.sp"
#include "radio_adverts_manager/convars.sp"
#include "radio_adverts_manager/adrotation.sp"
#include "radio_adverts_manager/natives.sp"
#include "radio_adverts_manager/motdhandler.sp"
#include "radio_adverts_manager/clientsettings.sp"

void ResetClient(int iClient)
{
	/* Radio */
	
	g_bRadio[iClient] = false;
	g_bRadioPaused[iClient] = false;
	
	/* Adverts*/
	
	g_bAdvert[iClient] = false;
	g_iAdvertStarted[iClient] = 0;
	g_iLastAdvertStarted[iClient] = 0;
	
	/* Settings */
	
	g_iRadioChannel[iClient] = -1;
	g_iRadioVolume[iClient] = 40;
	g_iAdvertsMode[iClient] = g_iAdvertsModeDefault;
}

/* Plugins */

public Plugin myinfo = 
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
}

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	RegPluginLibrary("radio_adverts_manager");
	
	CreateNative("RAM_GetClientRadioChannel", Native_GetClientRadioChannel);
	CreateNative("RAM_SetClientRadioChannel", Native_SetClientRadioChannel);
	CreateNative("RAM_GetClientRadioVolume", Native_GetClientRadioVolume);
	CreateNative("RAM_SetClientRadioVolume", Native_SetClientRadioVolume);
	CreateNative("RAM_IsClientRadioEnabled", Native_IsClientRadioEnabled);
	CreateNative("RAM_IsClientAdvertPlaying", Native_IsClientAdvertPlaying);
	CreateNative("RAM_AbortClientAdvert", Native_AbortClientAdvert);
	CreateNative("RAM_GetRadioChannelName", Native_GetRadioChannelName);
	CreateNative("RAM_GetClientAdvertsMode", Native_GetClientAdvertsMode);
	CreateNative("RAM_SetClientAdvertsMode", Native_SetClientAdvertsMode);
	CreateNative("RAM_StartAdvert", Native_StartAdvert);
	
	g_OnClientShouldShowAdvert = CreateGlobalForward("RAM_OnClientShouldShowAdvert", ET_Event, Param_Cell);
	g_OnClientAccessAdvertSettings = CreateGlobalForward("RAM_OnClientAccessAdvertSettings", ET_Event, Param_Cell);
	g_OnClientAdvertStarted = CreateGlobalForward("RAM_OnClientAdvertStarted", ET_Ignore, Param_Cell, Param_Cell, Param_Cell);
	g_OnClientAdvertFinished = CreateGlobalForward("RAM_OnClientAdvertFinished", ET_Ignore, Param_Cell, Param_Cell, Param_Cell, Param_Cell);
	g_OnClientRadioTurnOn = CreateGlobalForward("RAM_OnClientRadioTurnOn", ET_Ignore, Param_Cell, Param_Cell);
	g_OnClientRadioTurnOff = CreateGlobalForward("RAM_OnClientRadioTurnOff", ET_Ignore, Param_Cell, Param_Cell);
	g_OnClientRadioResumed = CreateGlobalForward("RAM_OnClientRadioResumed", ET_Ignore, Param_Cell, Param_Cell);
	
	g_OnClientRadioChannelChanged = CreateGlobalForward("RAM_OnClientRadioChannelChanged", ET_Ignore, Param_Cell, Param_Cell, Param_Cell);
	g_OnClientRadioVolumeChanged = CreateGlobalForward("RAM_OnClientRadioVolumeChanged", ET_Ignore, Param_Cell, Param_Cell, Param_Cell);
	g_OnClientAdvertsModeChanged = CreateGlobalForward("RAM_OnClientAdvertsModeChanged", ET_Ignore, Param_Cell, Param_Cell, Param_Cell);
	
	return APLRes_Success;
}

/* Plugin Start */

public void OnPluginStart()
{
	/* User Messages */
	
	UserMsg VGUIMenu = GetUserMessageId("VGUIMenu");
	if (VGUIMenu != INVALID_MESSAGE_ID)
		HookUserMessage(VGUIMenu, VGUIMenu_Callback, true);
	else SetFailState("[Radio Adverts Manager] The server's engine version doesn't supports VGUI menus.");
	
	AddCommandListener(OnJoingame, "joingame");
	HookEventEx("player_connect_full", Event_OnConnectFull);
	
	/* Convars */
	
	CreateConVars();
	
	/* Translations */
	
	LoadTranslations("radio_adverts_manager.phrases");

	/* Game Events */
	
	HookEvent("player_death", Event_PlayerDeath, EventHookMode_Post);
	HookEvent("player_spawn", Event_PlayerSpawn);
	
	HookEvent("round_poststart", Event_OnRoundPostStart);
	
	/* Player Commands */
	
	RegConsoleCmd("radio", Cmd_Radio);
	RegConsoleCmd("sm_radio", Cmd_Radio);
	
	RegConsoleCmd("radio_stop", Cmd_StopRadio);
	RegConsoleCmd("sm_radio_stop", Cmd_StopRadio);
	
	RegConsoleCmd("stopradio", Cmd_StopRadio);
	RegConsoleCmd("sm_stopradio", Cmd_StopRadio);
	
	RegConsoleCmd("stop_radio", Cmd_StopRadio);
	RegConsoleCmd("sm_stop_radio", Cmd_StopRadio);
	
	RegConsoleCmd("ads", Cmd_Adverts);
	RegConsoleCmd("adverts", Cmd_Adverts);
	
	/* Game Info */
	
	GetGameFolderName(g_sGameDir, sizeof(g_sGameDir));
	
	/* Get Server IP */
	
	Handle hServerIP = FindConVar("hostip");
	int iIP = GetConVarInt(hServerIP);
	Format(g_sServerIP, sizeof(g_sServerIP), "%d.%d.%d.%d", iIP >>> 24 & 255, iIP >>> 16 & 255, iIP >>> 8 & 255, iIP & 255);
	
	/* Get Server Port */
	
	Handle hServerPort = FindConVar("hostport");
	g_iServerPort = GetConVarInt(hServerPort);
}

public void OnMapStart()
{
	CreateTimer(1.0, Timer_Check, _, TIMER_REPEAT | TIMER_FLAG_NO_MAPCHANGE);
}

public void OnMapEnd()
{
	LoopClients(iClient)	
		g_hAdvertTimers[iClient] = null;
}

public bool OnClientConnect(int iClient, char[] rejectmsg, int len)
{
	g_bVGUI[iClient] = false;
	
	return true;
}

public void OnClientDisconnect(int iClient)
{
	ResetClient(iClient);
}

bool IsClientValid(int iClient)
{
	if (iClient > 0 && iClient <= MaxClients)
		if (IsClientInGame(iClient))
			return true;
	return false;
}

public Action Timer_Check(Handle timer, any data)
{
	int iTime = GetTime();
	
	// Show advert
	LoopIngamePlayers(iClient)
	{
		// Not fully connected
		if(!g_bVGUI[iClient])
			continue;
		
		if(GetClientTeam(iClient) == CS_TEAM_NONE)
			continue;
		
		// Already watching an advert
		if(g_bAdvert[iClient])
			continue;
		
		// Has adverts disabled
		if(g_iAdvertsMode[iClient] == 0)
			continue;
		
		if(iTime - g_iLastAdvertStarted[iClient] < g_iAdvertsInterval[g_iAdvertsMode[iClient]-1])
			continue;
		
		ShowAdsPre(iClient, GetNextProvider(iClient));
	}
	
	return Plugin_Continue;
}