public Action Event_PlayerSpawn(Event event, const char[] name, bool dontBroadcast)
{
	int iClient = GetClientOfUserId(event.GetInt("userid"));
	
	if(g_bAdvertsSpawnAbort)
		EndAdvert(iClient, true);
	
	return Plugin_Continue;
}

public Action Event_PlayerDeath(Event event, const char[] name, bool dontBroadcast)
{
	int iClient = GetClientOfUserId(event.GetInt("userid"));
	
	if(IsClientValid(iClient))
	{
		int iProvider = GetNextProvider(iClient);
		
		if(iProvider != 0)
		{
			DataPack pack = new DataPack();
			CreateDataTimer(g_fAdvertsDeathDelay, Timer_DisplayAd, pack, TIMER_FLAG_NO_MAPCHANGE);
			pack.WriteCell(GetClientUserId(iClient));
			pack.WriteCell(iProvider);
			pack.WriteCell(true);
		}
	}
	
	return Plugin_Continue;
}

public Action Event_OnRoundPostStart(Event event, const char[] name, bool dontBroadcast)
{
	CreateTimer(0.0, Timer_ForceEndAdverts, _, TIMER_FLAG_NO_MAPCHANGE);
	CreateTimer(1.0, Timer_ForceEndAdverts, _, TIMER_FLAG_NO_MAPCHANGE);
	CreateTimer(2.0, Timer_ForceEndAdverts, _, TIMER_FLAG_NO_MAPCHANGE);
	
	return Plugin_Continue;
}

public Action Timer_ForceEndAdverts(Handle timer, any data)
{
	if(g_bAdvertsSpawnAbort)
		LoopIngameClients(iClient)
			EndAdvert(iClient, true);
	
	return Plugin_Handled;
}

public Action Timer_DisplayAd(Handle timer, any pack)
{
	ResetPack(pack);
	
	int userid = ReadPackCell(pack);
	int iProvider = ReadPackCell(pack);
	bool alive = ReadPackCell(pack);
	
	int iClient = GetClientOfUserId(userid);
	
	// player  must valid
	if (!IsClientValid(iClient))
		return Plugin_Handled;

	// must player alive?
	if(alive && IsPlayerAlive(iClient))
		return Plugin_Handled;
	
	ShowAdsPre(iClient, iProvider);
	
	return Plugin_Handled;
}

void ShowAdsPre(int iClient, int iProvider)
{
	if(g_bEnableHtmlCheck)
		QueryClientConVar(iClient, "cl_disablehtmlmotd", view_as<ConVarQueryFinished>(ClientConVar), iProvider);
	else ShowAds(iClient, iProvider);
}

int g_iLastSpamMsg[MAXPLAYERS + 1];

public void ClientConVar(QueryCookie cookie, int iClient, ConVarQueryResult result, const char[] name, const char[] value, any iProvider)
{
	// player  must valid
	if (!IsClientValid(iClient))
		return;
	
	if (StringToInt(value) == 0)
		ShowAds(iClient, iProvider);
	else if(g_iEnableHtmlCheckSpamTime > 0 && g_bEnableSpamProtection && (g_iLastMessage[iClient] > 0 || AllowMessage(iClient)))
	{
		if(GetTime() - g_iLastSpamMsg[iClient] > g_iEnableHtmlCheckSpamTime)
		{
			g_iLastSpamMsg[iClient] = GetTime();
			CPrintToChat(iClient, "%T", "Motd_disabled1", iClient);
			CPrintToChat(iClient, "%T", "Motd_disabled2", iClient);
			CPrintToChat(iClient, "%T", "Motd_disabled3", iClient);
			CPrintToChat(iClient, "%T", "Motd_disabled4", iClient);
			CPrintToChat(iClient, "%T", "Motd_disabled5", iClient);
		}
		
		UpdateSpamTime(iClient);
	}
}

bool AllowMessage(int iClient)
{
	if(g_iLastMessage[iClient] <= (GetTime() - g_iSpamProtectionTime))
		return true;
	return false;
}

void UpdateSpamTime(int iClient)
{
	g_iLastMessage[iClient] = GetTime();
}

void ShowAds(int iClient, int iProvider)
{
	if(!g_bVGUI[iClient])
		return;
	
	if(g_bAdvert[iClient])
		return;
		
	if(g_bAdvertsTimerDeadOnly && IsPlayerAlive(iClient))
		return;
	
	if(g_iAdvertsMode[iClient] == 0)
		return;
	
	Action result;
	Call_StartForward(g_OnClientShouldShowAdvert);
	Call_PushCell(iClient);
	Call_Finish(result);
	
	if(result == Plugin_Handled || result == Plugin_Stop)
		return;
	
	iProvider = GetNextProvider(iClient);
	
	if(iProvider == VPP)
		VPP_ShowAds(iClient);
	else if(iProvider == MOTDGD)
		MOTDGD_ShowAds(iClient);
	else if(iProvider == PINION)
		PINION_ShowAds(iClient);
}

Handle g_hTimerBlockMOTD[MAXPLAYERS+1] = {null, ...};

public Action Event_OnConnectFull(Handle event, const char[] name, bool dontBroadcast)
{
	int iClient = GetClientOfUserId(GetEventInt(event, "userid"));
	g_hTimerBlockMOTD[iClient] = CreateTimer(0.1, Timer_CloseMOTD, iClient, TIMER_REPEAT);
	
	return Plugin_Continue;
}

public Action OnJoingame(int iClient, const char[] command, int argc)
{
	if(g_hTimerBlockMOTD[iClient] != null)
	{
		KillTimer(g_hTimerBlockMOTD[iClient]);
		g_hTimerBlockMOTD[iClient] = null;
	}
	
	g_bVGUI[iClient] = true;
	
	return Plugin_Continue;
}

public Action Timer_CloseMOTD(Handle timer, any iClient)
{
    if(iClient == 0 || !IsClientInGame(iClient) || IsFakeClient(iClient))
    {
        g_hTimerBlockMOTD[iClient] = null;
        return Plugin_Stop;
    }

    Handle pb = StartMessageOne("VGUIMenu", iClient);
    PbSetString(pb, "name", "info");
    PbSetBool(pb, "show", false);
    EndMessage();

    return Plugin_Continue;

}

public Action VGUIMenu_Callback(UserMsg msg_id, Handle hPb, const players[], int playersNum, bool reliable, bool init)
{
	if(!(playersNum > 0))
		return Plugin_Handled;
	
	int iClient = players[0];
	
	if (playersNum > 1 || !IsValidClient(iClient))
		return Plugin_Continue;
	
	if(g_bVGUI[iClient])
	{
		char chBuffer[64]; PbReadString(hPb, "name", chBuffer, sizeof(chBuffer));
		
		if (StrEqual(chBuffer, "info", false))
		{
			int iCount = PbGetRepeatedFieldCount(hPb, "subkeys");
			char chName[128]; char chWebsite[420];
			
			for (int i = 0; i < iCount; i++)
			{
				Handle hSubKey = PbReadRepeatedMessage(hPb, "subkeys", i);
				PbReadString(hSubKey, "name", chName, 128);
				
				if (!StrEqual(chName, "msg", false))
					continue;
				
				PbReadString(hSubKey, "str", chWebsite, 420);
				
				if (g_bAdvert[iClient])
				{
					/* Old method which blocks the motd window from being used aslong an advert is running
					CreateTimer(0.0, Timer_Cancelled, iClient, TIMER_FLAG_NO_MAPCHANGE);
					return Plugin_Handled;
					*/
					
					/* Stop the advert */
					CreateTimer(0.0, Timer_AbortAdvert, iClient, TIMER_FLAG_NO_MAPCHANGE);
					return Plugin_Continue;
				}
			}
		}
	}
	
	return Plugin_Continue;
}

public Action Timer_Cancelled(Handle timer, any iClient)
{
	if(iClient && IsClientInGame(iClient))
		CPrintToChat(iClient, "Action cancelled! Reason: An advert is running.");
	
	return Plugin_Handled;
}

public Action Timer_AbortAdvert(Handle timer, any iClient)
{
	if(iClient && IsClientInGame(iClient))
	{
		EndAdvert(iClient, true, false);
	}
	
	return Plugin_Handled;
}

/* Show Adverts */

void MOTDGD_ShowAds(int iClient)
{
	char sUrl[512];
	strcopy(sUrl, sizeof(sUrl), g_sMotdGdUrl);
	
	char sID[32];
	IntToString(g_iMotdGdUserID, sID, sizeof(sID));
	ReplaceString(sUrl, sizeof(sUrl), "{MOTDGD-ID}", sID);
	
	ReplaceString(sUrl, sizeof(sUrl), "{MOTDGD-VERSION}", g_sMotdGdVersion);
	
	char sSteam[255]; 
	if(!GetClientAuthId(iClient, AuthId_Steam2, sSteam, sizeof(sSteam)))
		Format(sSteam, sizeof(sSteam), "NULL");
	ReplaceString(sUrl, sizeof(sUrl), "{STEAMID}", sSteam);
	
	char sName[MAX_NAME_LENGTH];
	GetClientName(iClient, sName, sizeof(sName));
	
	char sNameEncoded[MAX_NAME_LENGTH*2];
	urlencode(sName, sNameEncoded, sizeof(sNameEncoded));
	ReplaceString(sUrl, sizeof(sUrl), "{NICKNAME}", sNameEncoded);
	
	ReplaceString(sUrl, sizeof(sUrl), "{IP}", g_sServerIP);
	
	char sPort[32];
	IntToString(g_iServerPort, sPort, sizeof(sPort));
	ReplaceString(sUrl, sizeof(sUrl), "{PORT}", sPort);
	
	ReplaceString(sUrl, sizeof(sUrl), "{GAME}", g_sGameDir);
	
	Radio_Pause(iClient);
	MOTD_OpenWindow(iClient, sUrl);
	StartAdvertsTimer(iClient, MOTDGD);
	g_bAdvert[iClient] = true;
}

void VPP_ShowAds(int iClient)
{
	char sUrl[512];
	strcopy(sUrl, sizeof(sUrl), g_sVppUrl);
	
	char sID[32];
	IntToString(g_iVppUserID, sID, sizeof(sID));
	
	ReplaceString(sUrl, sizeof(sUrl), "{VPP-ID}", sID);
	
	Radio_Pause(iClient);
	MOTD_OpenWindow(iClient, sUrl);
	StartAdvertsTimer(iClient, VPP);
	g_bAdvert[iClient] = true;
}

void PINION_ShowAds(int iClient)
{
	char sUrl[512];
	strcopy(sUrl, sizeof(sUrl), g_sPinionUrl);
	
	ReplaceString(sUrl, sizeof(sUrl), "{IP}", g_sServerIP);
	
	char sPort[32];
	IntToString(g_iServerPort, sPort, sizeof(sPort));
	ReplaceString(sUrl, sizeof(sUrl), "{PORT}", sPort);
	
	char sSteam[255]; 
	GetClientAuthId(iClient, AuthId_Steam2, sSteam, sizeof(sSteam));
	ReplaceString(sUrl, sizeof(sUrl), "{STEAMID}", sSteam);
	
	ReplaceString(sUrl, sizeof(sUrl), "{PINION-VERSION}", g_sPinionVersion);
	
	ReplaceString(sUrl, sizeof(sUrl), "{COMMUNITY}", g_sPinionCommunityName);
	
	ReplaceString(sUrl, sizeof(sUrl), "{PROFILE}", g_sPinionGameProfile);
	
	if(GetRandomInt(0, 5) < 5)
		ReplaceString(sUrl, sizeof(sUrl), "{TRIGGER}", "4");
	else ReplaceString(sUrl, sizeof(sUrl), "{TRIGGER}", "5");
	
	Radio_Pause(iClient);
	MOTD_OpenWindow(iClient, sUrl);
	StartAdvertsTimer(iClient, PINION);
	g_bAdvert[iClient] = true;
}

/* Radio */

void Radio_Pause(int iClient)
{
	if(g_bRadio[iClient])
	{
		g_bRadio[iClient] = false;
		g_bRadioPaused[iClient] = true;
	}
}

void Radio_Stop(int iClient)
{
	g_bRadio[iClient] = false;
	g_bRadioPaused[iClient] = false;
	
	if(g_bAdvert[iClient])
		return;
	
	Call_StartForward(g_OnClientRadioTurnOff);
	Call_PushCell(iClient);
	Call_Finish(_);
	
	if(!g_bVGUI[iClient])
		return;
	
	MOTD_OpenWindow(iClient, "about:blank");
}

void Radio_Resume(int iClient)
{
	if(g_bRadioPaused[iClient] && !g_bRadio[iClient])
	{
		Call_StartForward(g_OnClientRadioResumed);
		Call_PushCell(iClient);
		Call_Finish(_);
		
		Radio_Start(iClient);
	}
}

bool Radio_Start(int iClient)
{
	if(g_iRadioChannel[iClient] < 0)
		return false;
	
	g_bRadioPaused[iClient] = true;
	
	// Advert is running
	if(g_bAdvert[iClient])
		return false;
	
	g_bRadio[iClient] = true;
	
	Call_StartForward(g_OnClientRadioTurnOn);
	Call_PushCell(iClient);
	Call_Finish(_);
	
	if(!g_bVGUI[iClient])
		return false;
	
	char sURL[512];
	GetArrayString(g_aChannelUrls, g_iRadioChannel[iClient], sURL, sizeof(sURL));
	
	char sVolume[12];
	IntToString(g_iRadioVolume[iClient], sVolume, sizeof(sVolume));
	ReplaceString(sURL, sizeof(sURL), "{VOLUME}", sVolume);
	
	MOTD_OpenWindow(iClient, sURL);
	
	return true;
}

void ResetAdvertsTimer(int iClient, bool close)
{
	if(close && g_hAdvertTimers[iClient] != null)
		CloseHandle(g_hAdvertTimers[iClient]);
	
	g_hAdvertTimers[iClient] = null;
}

int g_iProvider[MAXPLAYERS + 1];

void StartAdvertsTimer(int iClient, int iProvider)
{
	g_iAdvertStarted[iClient] = GetTime();
	g_iLastAdvertStarted[iClient] = g_iAdvertStarted[iClient];
	
	ResetAdvertsTimer(iClient, true);
	
	float fDelay;
	
	if(g_bRadioPaused[iClient])
		fDelay = float(g_iAdvertsLengthRadio[g_iAdvertsMode[iClient]-1]);
	else fDelay = float(g_iAdvertsLength[g_iAdvertsMode[iClient]-1]);
	
	g_hAdvertTimers[iClient] = CreateTimer(fDelay, Timer_EndAdvert, iClient, TIMER_FLAG_NO_MAPCHANGE);
	
	g_iProvider[iClient] = iProvider;
	
	CPrintToChat(iClient, "%T", "AdvertStarted", iClient);
	
	Call_StartForward(g_OnClientAdvertStarted);
	Call_PushCell(iClient);
	Call_PushCell(iProvider);
	Call_Finish(_);
}

public Action Timer_EndAdvert(Handle timer, any iClient)
{
	g_hAdvertTimers[iClient] = null;
	
	EndAdvert(iClient, false);
	
	return Plugin_Handled
}

void EndAdvert(int iClient, bool abort, bool resume = true)
{
	if(!g_bAdvert[iClient])
		return;
	
	g_bAdvert[iClient] = false;
	
	ResetAdvertsTimer(iClient, true);
	
	if(!IsClientInGame(iClient))
		return;
	
	int iTime = GetTime() - g_iAdvertStarted[iClient];
	g_iAdvertStarted[iClient] = 0;
	
	Call_StartForward(g_OnClientAdvertFinished);
	Call_PushCell(iClient);
	Call_PushCell(g_iProvider[iClient]);
	Call_PushCell(iTime);
	Call_PushCell(abort);
	Call_Finish(_);
	
	if(!resume)
		return;
	
	if(g_bRadioPaused[iClient])
		Radio_Resume(iClient);
	else MOTD_OpenWindow(iClient, "about:blank");
}

stock void MOTD_OpenWindow(int iClient, char[] sUrl)
{
	Handle kv = CreateKeyValues("data");
	KvSetNum(kv, "cmd", 5);
	KvSetString(kv, "msg", sUrl);
	KvSetString(kv, "title", "Goomba Stomp");
	KvSetNum(kv, "type", MOTDPANEL_TYPE_URL);
	ShowVGUIPanel(iClient, "info", kv, !g_bHideWindow);
	CloseHandle(kv);
}