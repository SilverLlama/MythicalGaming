ArrayList g_aAds[MAXPLAYERS + 1] = {null, ...};

int GetNextProvider(int iClient)
{
	if(g_aAds[iClient] == null)
		g_aAds[iClient] = new ArrayList();
	
	// No provider left in rotation, lets fill up the rotation
	
	if(g_aAds[iClient].Length == 0)
	{
		// Get advert provider flags for player GEO-IP country
		
		char sIP[32];
		char sCountryCode[3]; 
		if(GetClientIP(iClient, sIP, sizeof(sIP)) && GeoipCode2(sIP, sCountryCode))
		{
			int iProvider = 0;
			GetTrieValue(g_aAdvertCountrys, sCountryCode, iProvider);
			
			if(iProvider & VPP)
				g_aAds[iClient].Push(VPP);
			
			if(iProvider & MOTDGD)
				g_aAds[iClient].Push(MOTDGD);
			
			if(iProvider & PINION)
				g_aAds[iClient].Push(PINION);
		}
		
		// If there are no flags set use the default flags
		
		if(g_aAds[iClient].Length == 0)
		{
			if(StrContains(g_sAdvertsProvider, "vpp") != -1)
				g_aAds[iClient].Push(VPP);
			
			if(StrContains(g_sAdvertsProvider, "motdgd") != -1)
				g_aAds[iClient].Push(MOTDGD);
			
			if(StrContains(g_sAdvertsProvider, "pinion") != -1)
				g_aAds[iClient].Push(PINION);
		}
	}
	
	// No advert providers available
	
	if(g_aAds[iClient].Length == 0)
		return 0;
	
	// Get the first provider from our list and return it
	
	int iProvider = GetArrayCell(g_aAds[iClient], 0);
	g_aAds[iClient].Erase(0);
	
	return iProvider;
}

int GetAdvertProviderFlags(char sProvider[32]) 
{ 
	int iFlags; 
	
	if(StrContains(sProvider, "vpp") != -1) 
		iFlags |= VPP; 
	
	if(StrContains(sProvider, "motdgd") != -1) 
		iFlags |= MOTDGD; 
	
	if(StrContains(sProvider, "pinion") != -1) 
		iFlags |= PINION; 
	
	return iFlags; 
} 