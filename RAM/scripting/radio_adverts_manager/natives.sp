public int Native_GetClientRadioChannel(Handle plugin, int numParams)
{
	int iClient = GetNativeCell(1);
	return g_iRadioChannel[iClient];
}

public int Native_SetClientRadioChannel(Handle plugin, int numParams)
{
	int iClient = GetNativeCell(1);
	int iChannel = GetNativeCell(2);
	g_iRadioChannel[iClient] = iChannel;
}

public int Native_GetClientRadioVolume(Handle plugin, int numParams)
{
	int iClient = GetNativeCell(1);
	return g_iRadioVolume[iClient];
}

public int Native_SetClientRadioVolume(Handle plugin, int numParams)
{
	int iClient = GetNativeCell(1);
	int iVolume = GetNativeCell(2);
	g_iRadioVolume[iClient] = iVolume;
}

public int Native_GetClientAdvertsMode(Handle plugin, int numParams)
{
	int iClient = GetNativeCell(1);
	return g_iAdvertsMode[iClient];
}

public int Native_SetClientAdvertsMode(Handle plugin, int numParams)
{
	int iClient = GetNativeCell(1);
	int iMode = GetNativeCell(2);
	
	int iModeOld = g_iAdvertsMode[iClient];
	g_iAdvertsMode[iClient] = iMode;
	
	Call_StartForward(g_OnClientAdvertsModeChanged);
	Call_PushCell(iClient);
	Call_PushCell(iMode);
	Call_PushCell(iModeOld);
	Call_Finish(_);
}

public int Native_IsClientRadioEnabled(Handle plugin, int numParams)
{
	int iClient = GetNativeCell(1);
	return g_bRadio[iClient];
}

public int Native_IsClientAdvertPlaying(Handle plugin, int numParams)
{
	int iClient = GetNativeCell(1);
	return g_bAdvert[iClient];
}

public int Native_AbortClientAdvert(Handle plugin, int numParams)
{
	int iClient = GetNativeCell(1);
	EndAdvert(iClient, true);
}

public int Native_GetRadioChannelName(Handle plugin, int numParams)
{
	int iClient = GetNativeCell(1);
		
	if(g_iRadioChannel[iClient] < 0)
		return false;
	
	char sBuffer[32];
	GetArrayString(g_aChannelNames, g_iRadioChannel[iClient], sBuffer, sizeof(sBuffer));
	
	if (SetNativeString(4, sBuffer, 32, true) == SP_ERROR_NONE)
		return true;

	return false;
}

public int Native_StartAdvert(Handle plugin, int numParams)
{
	int iClient = GetNativeCell(1);
	int iProvider = GetNativeCell(2);
	
	if(iProvider <= 0)
		iProvider = GetNextProvider(iClient);
	
	ShowAds(iClient, iProvider);
}