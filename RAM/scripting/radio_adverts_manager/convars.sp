ConVar g_cEnableHtmlCheck = null;
bool g_bEnableHtmlCheck;

ConVar g_cEnableHtmlCheckSpamTime = null;
int g_iEnableHtmlCheckSpamTime;

ConVar g_cEnableSpamProtection = null;
bool g_bEnableSpamProtection;

ConVar g_cHideWindow = null;
bool g_bHideWindow;

ConVar g_cSpamProtectionTime = null;
int g_iSpamProtectionTime;

ConVar g_cAdvertsModeDefault = null;
int g_iAdvertsModeDefault;

ConVar g_cAdvertsProvider = null;
char g_sAdvertsProvider[32];

ConVar g_cAdvertsProviderConfig = null;
char g_sAdvertsProviderConfig[255];

ConVar g_cAdvertsDeathDelay = null;
float g_fAdvertsDeathDelay;

ConVar g_cAdvertsTimerDeadOnly = null;
bool g_bAdvertsTimerDeadOnly;

ConVar g_cAdvertsLength[3] = {null, ...};
int g_iAdvertsLength[3];

ConVar g_cAdvertsLengthRadio[3] = {null, ...};
int g_iAdvertsLengthRadio[3];

ConVar g_cAdvertsInterval[3] = {null, ...};
int g_iAdvertsInterval[3];

ConVar g_cAdvertsSpawnAbort = null;
bool g_bAdvertsSpawnAbort;

ConVar g_cMotdGdUserID = null;
int g_iMotdGdUserID;

ConVar g_cMotdGdVersion = null;
char g_sMotdGdVersion[32];

ConVar g_cMotdGdUrl = null;
char g_sMotdGdUrl[255];

ConVar g_cVppUserID = null;
int g_iVppUserID;

ConVar g_cVppUrl = null;
char g_sVppUrl[255];

ConVar g_cPinionUrl = null;
char g_sPinionUrl[255];

ConVar g_cPinionVersion = null;
char g_sPinionVersion[32];

ConVar g_cPinionCommunityName = null;
char g_sPinionCommunityName[255];

ConVar g_cPinionGameProfile = null;
char g_sPinionGameProfile[255];

ConVar g_cRadioConfigPath = null;
char g_sRadioConfigPath[255];

void CreateConVars()
{
	/* Adverts */
	
	g_cEnableHtmlCheck = CreateConVar("ram_enable_cl_disablehtmlmotd_check", "1", "Enable client check for cl_disablehtmlmotd", _, true, 0.0, true, 1.0);
	g_bEnableHtmlCheck = GetConVarBool(g_cEnableHtmlCheck);
	HookConVarChange(g_cEnableHtmlCheck, OnSettingChanged);
	
	g_cEnableHtmlCheckSpamTime = CreateConVar("ram_enable_cl_disablehtmlmotd_check_spam_time", "120", "How often to spam a player that he has cl_disablehtmlmotd enabled.");
	g_iEnableHtmlCheckSpamTime = GetConVarInt(g_cEnableHtmlCheckSpamTime);
	HookConVarChange(g_cEnableHtmlCheckSpamTime, OnSettingChanged);
	
	g_cHideWindow = CreateConVar("ram_hide_window", "1", "Hide the MOTD window or not.", _, true, 0.0, true, 1.0);
	g_bHideWindow = GetConVarBool(g_cHideWindow);
	HookConVarChange(g_cHideWindow, OnSettingChanged);
	
	g_cEnableSpamProtection = CreateConVar("ram_spam_protection_enable", "1", "Enable spam protection?", _, true, 0.0, true, 1.0);
	g_bEnableSpamProtection = GetConVarBool(g_cEnableSpamProtection);
	HookConVarChange(g_cEnableSpamProtection, OnSettingChanged);
	
	g_cSpamProtectionTime = CreateConVar("ram_spam_protection_time", "30", "Time between spam messages (time in seconds) (adverts_enable_spam_protection must be 1)");
	g_iSpamProtectionTime = GetConVarInt(g_cSpamProtectionTime);
	HookConVarChange(g_cSpamProtectionTime, OnSettingChanged);
	
	g_cAdvertsProvider = CreateConVar("ram_adverts_provider", "motdgd", "Which provider for ads? (supported providers: vpp,motdgd,pinion)");
	GetConVarString(g_cAdvertsProvider, g_sAdvertsProvider, sizeof(g_sAdvertsProvider));
	HookConVarChange(g_cSpamProtectionTime, OnSettingChanged);
	
	g_cAdvertsProviderConfig = CreateConVar("ram_adverts_provider_country_filter", "configs/advert_country_provider.txt", "Path to our countryfilter config.");
	GetConVarString(g_cAdvertsProviderConfig, g_sAdvertsProviderConfig, sizeof(g_sAdvertsProviderConfig));
	HookConVarChange(g_cAdvertsProviderConfig, OnSettingChanged);
	
	g_cAdvertsModeDefault = CreateConVar("ram_adverts_mode_default", "3", "Set default adverts mode (0-3)", _, true, 0.0, true, 3.0);
	g_iAdvertsModeDefault = GetConVarInt(g_cAdvertsModeDefault);
	HookConVarChange(g_cAdvertsModeDefault, OnSettingChanged);
	
	g_cAdvertsTimerDeadOnly = CreateConVar("ram_adverts_timer_dead_only", "1", "If set to one the plugin shows adverts only to dead palyers", _, true, 0.0, true, 1.0);
	g_bAdvertsTimerDeadOnly = GetConVarBool(g_cAdvertsTimerDeadOnly);
	HookConVarChange(g_cAdvertsTimerDeadOnly, OnSettingChanged);
	
	g_cAdvertsSpawnAbort = CreateConVar("ram_adverts_spawn_abort", "1", "If set to one the plugin stops ads when the player spawns", _, true, 0.0, true, 1.0);
	g_bAdvertsSpawnAbort = GetConVarBool(g_cAdvertsSpawnAbort);
	HookConVarChange(g_cAdvertsSpawnAbort, OnSettingChanged);
	
	g_cAdvertsDeathDelay = CreateConVar("ram_adverts_death_delay", "3.5", "After what amount of time should ads be displayed (adverts_death_enable must be 1)");
	g_fAdvertsDeathDelay = GetConVarFloat(g_cAdvertsDeathDelay);
	HookConVarChange(g_cAdvertsDeathDelay, OnSettingChanged);
	
	g_cAdvertsLength[0] = CreateConVar("ram_adverts_length_min", "35", "Max time to wait for the advert to complete for mode min");
	g_iAdvertsLength[0] = GetConVarInt(g_cAdvertsLength[0]);
	HookConVarChange(g_cAdvertsLength[0], OnSettingChanged);
	
	g_cAdvertsLength[1] = CreateConVar("ram_adverts_length_mid", "42", "Max time to wait for the advert to complete for mode mid");
	g_iAdvertsLength[1] = GetConVarInt(g_cAdvertsLength[1]);
	HookConVarChange(g_cAdvertsLength[1], OnSettingChanged);
	
	g_cAdvertsLength[2] = CreateConVar("ram_adverts_length_max", "50", "Max time to wait for the advert to complete for mode max");
	g_iAdvertsLength[2] = GetConVarInt(g_cAdvertsLength[2]);
	HookConVarChange(g_cAdvertsLength[2], OnSettingChanged);
	
	g_cAdvertsLengthRadio[0] = CreateConVar("ram_adverts_length_radio_min", "30", "Max time to wait for the advert to complete if radio is paused for mode min");
	g_iAdvertsLengthRadio[0] = GetConVarInt(g_cAdvertsLengthRadio[0]);
	HookConVarChange(g_cAdvertsLengthRadio[0], OnSettingChanged);
	
	g_cAdvertsLengthRadio[1] = CreateConVar("ram_adverts_length_radio_mid", "35", "Max time to wait for the advert to complete if radio is paused for mode mid");
	g_iAdvertsLengthRadio[1] = GetConVarInt(g_cAdvertsLengthRadio[1]);
	HookConVarChange(g_cAdvertsLengthRadio[1], OnSettingChanged);
	
	g_cAdvertsLengthRadio[2] = CreateConVar("ram_adverts_length_radio_max", "40", "Max time to wait for the advert to complete if radio is paused for mode max");
	g_iAdvertsLengthRadio[2] = GetConVarInt(g_cAdvertsLengthRadio[2]);
	HookConVarChange(g_cAdvertsLengthRadio[2], OnSettingChanged);
	
	g_cAdvertsInterval[0] = CreateConVar("ram_adverts_min_interval_min", "240", "Min interval between adverts for mode min");
	g_iAdvertsInterval[0] = GetConVarInt(g_cAdvertsInterval[0]);
	HookConVarChange(g_cAdvertsInterval[0], OnSettingChanged);
	
	g_cAdvertsInterval[1] = CreateConVar("ram_adverts_min_interval_mid", "180", "Min interval between adverts for mode mid");
	g_iAdvertsInterval[1] = GetConVarInt(g_cAdvertsInterval[1]);
	HookConVarChange(g_cAdvertsInterval[1], OnSettingChanged);
	
	g_cAdvertsInterval[2] = CreateConVar("ram_adverts_min_interval_max", "120", "Min interval between adverts for mode max");
	g_iAdvertsInterval[2] = GetConVarInt(g_cAdvertsInterval[2]);
	HookConVarChange(g_cAdvertsInterval[2], OnSettingChanged);
	
	/* MOTDgd*/
	
	g_cMotdGdUserID = CreateConVar("ram_adverts_motdgd_userid", "4644", "MOTDgd user ID");
	g_iMotdGdUserID = GetConVarInt(g_cMotdGdUserID);
	HookConVarChange(g_cMotdGdUserID, OnSettingChanged);
	
	g_cMotdGdVersion = CreateConVar("ram_adverts_motdgd_version", "2.4.87", "MOTDgd Version");
	GetConVarString(g_cMotdGdVersion, g_sMotdGdVersion, sizeof(g_sMotdGdVersion));
	HookConVarChange(g_cMotdGdVersion, OnSettingChanged);
	
	g_cMotdGdUrl = CreateConVar("ram_adverts_motdgd_url", "http://motdgd.com/motd/?user={MOTDGD-ID}&ip={IP}&pt={PORT}&v={MOTDGD-VERSION}&st={STEAMID}&gm={GAME}&name={NICKNAME}", "MOTDgd User ID");
	GetConVarString(g_cMotdGdUrl, g_sMotdGdUrl, sizeof(g_sMotdGdUrl));
	HookConVarChange(g_cMotdGdUrl, OnSettingChanged);
	
	/* VPP */
	
	g_cVppUserID = CreateConVar("ram_adverts_vpp_userid", "0", "VPP user ID");
	g_iVppUserID = GetConVarInt(g_cVppUserID);
	HookConVarChange(g_cVppUserID, OnSettingChanged);
	
	
	g_cVppUrl = CreateConVar("ram_adverts_vpp_url", "http://vppgamingnetwork.com/Client/Content/{VPP-ID}", "VPP URL");
	GetConVarString(g_cVppUrl, g_sVppUrl, sizeof(g_sVppUrl));
	HookConVarChange(g_cVppUrl, OnSettingChanged);
	
	/* Pinion Adverts */
	
	g_cPinionUrl = CreateConVar("ram_adverts_pinion_url", "http://bin.pinion.gg/bin/pogcsgo/index.html?{COMMUNITY}/{PROFILE}/motd.html?ip={IP}&po={PORT}&si={STEAMID}&pv={PINION-VERSION}&tr={TRIGGER}", "PINION URL");
	GetConVarString(g_cPinionUrl, g_sPinionUrl, sizeof(g_sPinionUrl));
	HookConVarChange(g_cPinionUrl, OnSettingChanged);
	
	g_cPinionVersion = CreateConVar("ram_adverts_pinion_version", "1.16.12", "PINION Version");
	GetConVarString(g_cPinionVersion, g_sPinionVersion, sizeof(g_sPinionVersion));
	HookConVarChange(g_cPinionVersion, OnSettingChanged);
	
	g_cPinionCommunityName = CreateConVar("ram_adverts_pinion_community", "mycommunity", "PINION Community Name");
	GetConVarString(g_cPinionCommunityName, g_sPinionCommunityName, sizeof(g_sPinionCommunityName));
	HookConVarChange(g_cPinionCommunityName, OnSettingChanged);
	
	g_cPinionGameProfile = CreateConVar("ram_adverts_pinion_community", "idkyet", "PINION Game Profile");
	GetConVarString(g_cPinionGameProfile, g_sPinionGameProfile, sizeof(g_sPinionCommunityName));
	HookConVarChange(g_cPinionGameProfile, OnSettingChanged);
	
	/* Radio */
	
	g_cRadioConfigPath = CreateConVar("ram_radio_config", "configs/radiolist.txt");
	GetConVarString(g_cRadioConfigPath, g_sRadioConfigPath, sizeof(g_sRadioConfigPath));
	HookConVarChange(g_cRadioConfigPath, OnSettingChanged);
	
	AutoExecConfig(true, "radio_adverts_manager");
	
	LoadCountryProviderList();
	LoadRadioList();
}

public int OnSettingChanged(Handle convar, const char[] oldValue, const char[] newValue)
{
	if(convar == g_cEnableHtmlCheck)
		g_bEnableHtmlCheck = view_as<bool>(StringToInt(newValue));
	else if(convar == g_cEnableHtmlCheckSpamTime)
		g_iEnableHtmlCheckSpamTime = StringToInt(newValue);
	else if(convar == g_cHideWindow)
		g_bHideWindow = view_as<bool>(StringToInt(newValue));
	else if(convar == g_cEnableSpamProtection)
		g_bEnableSpamProtection = view_as<bool>(StringToInt(newValue));
	else if(convar == g_cSpamProtectionTime)
		g_iSpamProtectionTime = StringToInt(newValue);
	else if(convar == g_cAdvertsModeDefault)
		g_iAdvertsModeDefault = StringToInt(newValue);
	else if(convar == g_cAdvertsProvider)
		strcopy(g_sAdvertsProvider, sizeof(g_sAdvertsProvider), newValue);
	else if(convar == g_cAdvertsProviderConfig)
		strcopy(g_sAdvertsProviderConfig, sizeof(g_sAdvertsProviderConfig), newValue);
	else if(convar == g_cAdvertsDeathDelay)
		g_fAdvertsDeathDelay = StringToFloat(newValue);
	else if(convar == g_cAdvertsTimerDeadOnly)
		g_bAdvertsTimerDeadOnly = view_as<bool>(StringToInt(newValue));
	else if (convar == g_cAdvertsLength[0])
		g_iAdvertsLength[0] = StringToInt(newValue);
	else if (convar == g_cAdvertsLength[1])
		g_iAdvertsLength[1] = StringToInt(newValue);
	else if (convar == g_cAdvertsLength[2])
		g_iAdvertsLength[2] = StringToInt(newValue);
	else if (convar == g_cAdvertsLengthRadio[0])
		g_iAdvertsLengthRadio[0] = StringToInt(newValue);
	else if (convar == g_cAdvertsLengthRadio[1])
		g_iAdvertsLengthRadio[1] = StringToInt(newValue);
	else if (convar == g_cAdvertsLengthRadio[2])
		g_iAdvertsLengthRadio[2] = StringToInt(newValue);
	else if (convar == g_cAdvertsInterval[0])
		g_iAdvertsInterval[0] = StringToInt(newValue);
	else if (convar == g_cAdvertsInterval[1])
		g_iAdvertsInterval[1] = StringToInt(newValue);
	else if (convar == g_cAdvertsInterval[2])
		g_iAdvertsInterval[2] = StringToInt(newValue);
	else if(convar == g_cAdvertsSpawnAbort)
		g_bAdvertsSpawnAbort = view_as<bool>(StringToInt(newValue));
	else if(convar == g_cMotdGdUserID)
		g_iMotdGdUserID = StringToInt(newValue);
	else if(convar == g_cMotdGdVersion)
		strcopy(g_sMotdGdVersion, sizeof(g_sMotdGdVersion), newValue);
	else if(convar == g_cMotdGdUrl)
		strcopy(g_sMotdGdUrl, sizeof(g_sMotdGdUrl), newValue);
	else if(convar == g_cVppUserID)
		g_iVppUserID = StringToInt(newValue);
	else if(convar == g_cVppUrl)
		strcopy(g_sVppUrl, sizeof(g_sVppUrl), newValue);
	else if(convar == g_cPinionUrl)
		strcopy(g_sPinionUrl, sizeof(g_sPinionUrl), newValue);
	else if(convar == g_cPinionVersion)
		strcopy(g_sPinionVersion, sizeof(g_sPinionVersion), newValue);
	else if(convar == g_cPinionCommunityName)
		strcopy(g_sPinionCommunityName, sizeof(g_sPinionCommunityName), newValue);
	else if(convar == g_cPinionGameProfile)
		strcopy(g_sPinionGameProfile, sizeof(g_sPinionGameProfile), newValue);
	else if(convar == g_cRadioConfigPath)
		strcopy(g_sRadioConfigPath, sizeof(g_sRadioConfigPath), newValue);
}

void LoadRadioList()
{
	if(g_aChannelNames == null)
		g_aChannelNames = CreateArray(32);
	else ClearArray(g_aChannelNames);
	
	if(g_aChannelUrls == null)
		g_aChannelUrls = CreateArray(512);
	else ClearArray(g_aChannelUrls);
	
	char sPath[PLATFORM_MAX_PATH];
	g_cRadioConfigPath.GetString(sPath, sizeof(sPath));
	BuildPath(Path_SM, sPath, sizeof(sPath), "%s", sPath);
	
	if(!FileExists(sPath))
	{
		LogError("[RAM] Can't load radio channel list, config not found: %s.", sPath);
		return;
	}
	
	Handle hFile = OpenFile(sPath, "r");
	if (hFile == null)
	{
		LogError("[RAM] Can't load radio channel list, config not accessible: %s.", sPath);
		return;
	}
	
	char sBuffer[1024];
	char datas[2][512];
	
	while (ReadFileLine(hFile, sBuffer, sizeof(sBuffer))) 
	{
		TrimString(sBuffer);
		ExplodeString(sBuffer, ";", datas, sizeof(datas), sizeof(sBuffer));
		
		PushArrayString(g_aChannelNames, datas[0]);
		PushArrayString(g_aChannelUrls, datas[1]); 
	}
	CloseHandle(hFile);
}

void LoadCountryProviderList()
{
	if(g_aAdvertCountrys == null)
		g_aAdvertCountrys = CreateTrie();
	else ClearTrie(g_aAdvertCountrys);
	
	char sPath[PLATFORM_MAX_PATH];
	g_cAdvertsProviderConfig.GetString(sPath, sizeof(sPath));
	BuildPath(Path_SM, sPath, sizeof(sPath), "%s", sPath);
	
	if(!FileExists(sPath))
	{
		LogError("[RAM] Can't load country provider list, config not found: %s.", sPath);
		return;
	}
	
	Handle hFile = OpenFile(sPath, "r");
	if (hFile == null)
	{
		LogError("[RAM] Can't load country provider list, config not accessible: %s.", sPath);
		return;
	}
	
	char sBuffer[64];
	char datas[2][32];
	
	int iLine;
	while (ReadFileLine(hFile, sBuffer, sizeof(sBuffer))) 
	{
		iLine++;
		TrimString(sBuffer);
		ExplodeString(sBuffer, ";", datas, sizeof(datas), sizeof(sBuffer));
		
		if(!SetTrieValue(g_aAdvertCountrys, datas[0], GetAdvertProviderFlags(datas[1]), false))
			LogError("[RAM] Only one rule allowed per country %s, line %d. Location: %s", datas[0], iLine, sPath);
	}
	CloseHandle(hFile);
}