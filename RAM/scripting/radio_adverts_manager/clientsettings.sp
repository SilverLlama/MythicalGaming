public Action Cmd_Radio(int iClient, int args)
{
	if(!iClient)
		return Plugin_Handled;
		
	if(!IsClientInGame(iClient))
		return Plugin_Handled;
		
	if(!IsClientInGame(iClient))
		return Plugin_Handled;
		
	Menu_RadioMain(iClient);
	
	return Plugin_Handled;
}

public Action Cmd_StopRadio(int iClient, int args)
{
	if(!iClient)
		return Plugin_Handled;
		
	if(!IsClientInGame(iClient))
		return Plugin_Handled;
		
	if(!IsClientInGame(iClient))
		return Plugin_Handled;
	
	Radio_Stop(iClient);
	return Plugin_Handled;
}

public Action Cmd_Adverts(int iClient, int args)
{
	if(!iClient)
		return Plugin_Handled;
		
	if(!IsClientInGame(iClient))
		return Plugin_Handled;
		
	if(!IsClientInGame(iClient))
		return Plugin_Handled;
	
	Action result;
	Call_StartForward(g_OnClientAccessAdvertSettings);
	Call_PushCell(iClient);
	Call_Finish(result);
	
	if(result == Plugin_Handled || result == Plugin_Stop)
		return Plugin_Handled;
	
	Menu_AdvertsMode(iClient);
	return Plugin_Handled;
}

void Menu_RadioMain(int iClient)
{
	if(!IsClientValid(iClient))
		return;
	
	char sBuffer[64];
	
	Menu menu = new Menu(MenuHandler_Radio);
	
	char sChannelName[32];
	if(g_iRadioChannel[iClient] != -1)
		GetArrayString(g_aChannelNames, g_iRadioChannel[iClient], sChannelName, sizeof(sChannelName));
	else Format(sChannelName, sizeof(sChannelName), "%T", "RadioMenu_NoChannel", iClient);
	
	menu.SetTitle("%T", "RadioMenu_Title", iClient, sChannelName);
	
	if(g_bAdvert[iClient])
	{
		Format(sBuffer, sizeof(sBuffer), "%T", "RadioMenu_StopAdvert", iClient);
		menu.AddItem("stop_ad", sBuffer);
	}
	else if(!g_bRadio[iClient])
	{
		Format(sBuffer, sizeof(sBuffer), "%T", "RadioMenu_Start", iClient);
		menu.AddItem("start_radio", sBuffer);
	}
	else
	{
		Format(sBuffer, sizeof(sBuffer), "%T", "RadioMenu_StopRadio", iClient);
		menu.AddItem("stop_radio", sBuffer);
	}
	
	Format(sBuffer, sizeof(sBuffer), "%T", "RadioMenu_Channel", iClient);
	menu.AddItem("channel", sBuffer);
	
	Format(sBuffer, sizeof(sBuffer), "%T", "RadioMenu_Volume", iClient, g_iRadioVolume[iClient]);
	menu.AddItem("volume", sBuffer);
	
	Format(sBuffer, sizeof(sBuffer), "%T", "RadioMenu_AdvertsMode", iClient);
	menu.AddItem("adverts_mode", sBuffer);
	
	SetMenuExitButton(menu, true);
	
	menu.Display(iClient, MENU_TIME_FOREVER);
}

public int MenuHandler_Radio(Menu menu, MenuAction action, int iClient, int params)
{
	if (action == MenuAction_Select)
	{
		char sInfo[64];
		menu.GetItem(params, sInfo, sizeof(sInfo));
		
		if(StrEqual(sInfo, "start_radio"))
		{
			if(g_iRadioChannel[iClient] == -1)
				g_iRadioChannel[iClient] = GetRandomInt(0, GetArraySize(g_aChannelUrls)-1);
			
			Radio_Start(iClient);
			
			Menu_RadioMain(iClient);
		}
		else if(StrEqual(sInfo, "stop_ad"))
		{
			EndAdvert(iClient, true);
			
			Menu_RadioMain(iClient);
		}
		else if(StrEqual(sInfo, "stop_radio"))
		{
			Radio_Stop(iClient);
			
			Menu_RadioMain(iClient);
		}
		else if(StrEqual(sInfo, "volume"))
		{
			Menu_RadioVolume(iClient);
		}
		else if(StrEqual(sInfo, "channel"))
		{
			Menu_RadioChannel(iClient);
		}
		else if(StrEqual(sInfo, "adverts_mode"))
		{
			Action result;
			Call_StartForward(g_OnClientAccessAdvertSettings);
			Call_PushCell(iClient);
			Call_Finish(result);
			
			if(result == Plugin_Handled || result == Plugin_Stop)
				return;
				
			Menu_AdvertsMode(iClient);
		}
	}
	else if (action == MenuAction_End)
		delete menu;
}

void Menu_RadioVolume(int iClient)
{
	if(!IsClientValid(iClient))
		return;
	
	Menu menu = new Menu(MenuHandler_RadioVolume);
	
	menu.SetTitle("%T", "RadioMenu_VolumeTitle", iClient, g_iRadioVolume[iClient]);
	
	if(g_iRadioVolume[iClient] > 1)
		menu.AddItem("-1", "-1%");
	else menu.AddItem("x", "-1%", ITEMDRAW_DISABLED);
		
	if (g_iRadioVolume[iClient] < 100)
		menu.AddItem("+1", "+1%");
	else menu.AddItem("x", "+1%", ITEMDRAW_DISABLED);
	
	menu.AddItem("10", "10%");
	menu.AddItem("20", "20%");
	menu.AddItem("40", "40%");
	menu.AddItem("60", "60%");
	menu.AddItem("80", "80%");
	menu.AddItem("100", "100%");
	
	char sBuffer[64];
	Format(sBuffer, sizeof(sBuffer), "%T", "RadioMenu_Back", iClient);
	menu.AddItem("back", sBuffer);
	
	SetMenuExitButton(menu, true);
	
	menu.Display(iClient, MENU_TIME_FOREVER);
}

public int MenuHandler_RadioVolume(Menu menu, MenuAction action, int iClient, int params)
{
	if (action == MenuAction_Select)
	{
		char sInfo[64];
		menu.GetItem(params, sInfo, sizeof(sInfo));
		
		if(StrEqual(sInfo, "back"))
		{
			Menu_RadioMain(iClient);
			return;
		}
		
		int iVolume;
		
		if(ReplaceString(sInfo, sizeof(sInfo), "-", ""))
			iVolume = g_iRadioVolume[iClient]-StringToInt(sInfo);
		else if(ReplaceString(sInfo, sizeof(sInfo), "+", ""))
			iVolume = g_iRadioVolume[iClient]+StringToInt(sInfo);
		else iVolume = StringToInt(sInfo);
		
		Call_StartForward(g_OnClientRadioVolumeChanged);
		Call_PushCell(iClient);
		Call_PushCell(iVolume);
		Call_PushCell(g_iRadioVolume[iClient]);
		Call_Finish(_);
		
		g_iRadioVolume[iClient] = iVolume;
		
		if(!Radio_ChannelExist(g_iRadioChannel[iClient]))
			g_iRadioChannel[iClient] = GetRandomInt(0, GetArraySize(g_aChannelUrls)-1);
		
		Radio_Start(iClient);
		
		Menu_RadioVolume(iClient);
	}
	else if (action == MenuAction_End)
		delete menu;
}

bool Radio_ChannelExist(int channel)
{
	return 0 <= channel < GetArraySize(g_aChannelUrls);
}

void Menu_RadioChannel(int iClient)
{
	if(!IsClientValid(iClient))
		return;
	
	char sBuffer[64];
	
	Menu menu = new Menu(MenuHandler_RadioChannel);
		
	char sBack[64];
	Format(sBack, sizeof(sBack), "%T", "RadioMenu_Back", iClient);
	
	char sChannelName[32];
	if(g_iRadioChannel[iClient] != -1)
		GetArrayString(g_aChannelNames, g_iRadioChannel[iClient], sChannelName, sizeof(sChannelName));
	else Format(sChannelName, sizeof(sChannelName), "%T", "RadioMenu_NoChannel", iClient);
	
	menu.SetTitle("%T", "RadioMenu_ChannelTitle", iClient, sChannelName);
	
	int iChannels = GetArraySize(g_aChannelNames);
	for (int i = 0; i < iChannels; i++)
	{
		char sInfo[32];
		IntToString(i, sInfo, sizeof(sInfo));
		GetArrayString(g_aChannelNames, i, sBuffer, sizeof(sBuffer));
		menu.AddItem(sInfo, sBuffer);
	}
	
	Format(sBuffer, sizeof(sBuffer), "%T", "RadioMenu_Back", iClient);
	menu.AddItem("back", sBuffer);
	
	SetMenuExitButton(menu, true);
	
	menu.Display(iClient, MENU_TIME_FOREVER);
}

public int MenuHandler_RadioChannel(Menu menu, MenuAction action, int iClient, int params)
{
	if (action == MenuAction_Select)
	{
		char sInfo[64];
		menu.GetItem(params, sInfo, sizeof(sInfo));
		
		if(StrEqual(sInfo, "back"))
		{
			Menu_RadioMain(iClient);
			return;
		}
		
		int iInfo = StringToInt(sInfo);
		
		if(Radio_ChannelExist(iInfo))
		{
			Call_StartForward(g_OnClientRadioChannelChanged);
			Call_PushCell(iClient);
			Call_PushCell(iInfo);
			Call_PushCell(g_iRadioChannel[iClient]);
			Call_Finish(_);
			
			g_iRadioChannel[iClient] = iInfo;
			
			Radio_Start(iClient);
		}
		
		Menu_RadioChannel(iClient);
	}
	else if (action == MenuAction_End)
		delete menu;
}

void Menu_AdvertsMode(int iClient)
{
	if(!IsClientValid(iClient))
		return;
	
	char sBuffer[64];
	char sInfo[32];
	
	Menu menu = new Menu(MenuHandler_RadioAdvertsMode);
	
	menu.SetTitle("%T: %T", "RadioMenu_AdvertsModeTitle", iClient, g_sAdvertsModePhrases[g_iAdvertsMode[iClient]], iClient);
	
	for (int i = 0; i < sizeof(g_sAdvertsModePhrases); i++)
	{
		Format(sBuffer, sizeof(sBuffer), "%T", g_sAdvertsModePhrases[i], iClient);
		IntToString(i, sInfo, sizeof(sInfo));
		menu.AddItem(sInfo, sBuffer);
	}
	
	Format(sBuffer, sizeof(sBuffer), "%T", "RadioMenu_Back", iClient);
	menu.AddItem("back", sBuffer);
	
	SetMenuExitButton(menu, true);
	
	menu.Display(iClient, MENU_TIME_FOREVER);
}

public int MenuHandler_RadioAdvertsMode(Menu menu, MenuAction action, int iClient, int params)
{
	if (action == MenuAction_Select)
	{
		char sInfo[64];
		menu.GetItem(params, sInfo, sizeof(sInfo));
		
		if(StrEqual(sInfo, "back"))
		{
			Menu_RadioMain(iClient);
			return;
		}
		
		int iInfo = StringToInt(sInfo);
		int iModeOld = g_iAdvertsMode[iClient];
		g_iAdvertsMode[iClient] = iInfo;
		
		Call_StartForward(g_OnClientAdvertsModeChanged);
		Call_PushCell(iClient);
		Call_PushCell(iInfo);
		Call_PushCell(iModeOld);
		Call_Finish(_);
		
		Menu_AdvertsMode(iClient);
	}
	else if (action == MenuAction_End)
		delete menu;
}