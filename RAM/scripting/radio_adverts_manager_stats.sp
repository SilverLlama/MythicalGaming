#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "Radio Adverts Manager - Stats"
#define PLUGIN_VERSION "1.0"
#define PLUGIN_DESCRIPTION ""
#define PLUGIN_URL "https://gitlab.com/Zipcore/RadioAdvertsManager"

#include <sourcemod>
#include <multicolors>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>
#include <geoip>
#include <radio_adverts_manager>

#include <zcore/zcore_mysql> // Handles listen overrides
bool g_pZcoreMysql = false;

char sqlCreateTable[] = "CREATE TABLE IF NOT EXISTS `radio_adverts_manager` ( `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT, `ip` varchar(64) NOT NULL, `port` int(11), `playerID` bigint NOT NULL, `name` varchar(64) NOT NULL, `country` varchar(4) NOT NULL, `provider` int(11), `time` int(11) NOT NULL DEFAULT '0', `aborted` int(11) NOT NULL DEFAULT '0', `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";

bool g_bConnected;

char g_sIP[255];
int g_iPort;

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	RegPluginLibrary("radio_adverts_manager-stats");
	
	return APLRes_Success;
}

public void OnPluginStart()
{
	g_pZcoreMysql = LibraryExists("zcore-mysql");
	
	/* Mysql */
	ConnectDB();
	
	// Get Server IP
	Handle serverIP = FindConVar("hostip");
	int IP = GetConVarInt(serverIP);
	Format(g_sIP, sizeof(g_sIP), "%d.%d.%d.%d", IP >>> 24 & 255, IP >>> 16 & 255, IP >>> 8 & 255, IP & 255);
	
	// Get Server Port
	Handle serverPort = FindConVar("hostport");
	g_iPort = GetConVarInt(serverPort);
}

public void OnLibraryAdded(const char[] name)
{
	if (StrEqual(name, "zcore-mysql"))
		g_pZcoreMysql = true;
}

public void OnLibraryRemoved(const char[] name)
{
	if (StrEqual(name, "zcore-mysql"))
		g_pZcoreMysql = false;
}

/* Database Connection */

public void ZCore_Mysql_OnDatabaseError (int index, char[] config, char[] error )
{
	if(!StrEqual(config, "radio_adverts_manager"))
		return;
	
	DatabaseError("ZCore_Mysql_OnDatabaseError", error);
}

void DatabaseError(const char[] sFunction, const char[] error)
{
	LogError("[radio_adverts_manager_stats.smx] SQL Error on %s: %s", sFunction, error);
	
	g_iSQL = -1;
	g_hSQL = null;
	g_bConnected = false;
}

public void ZCore_Mysql_OnDatabaseConnected(int index, char[] config, Database connection_handle)
{
	if(!StrEqual(config, "radio_adverts_manager"))
		return;
	
	g_iSQL = index;
	g_hSQL = connection_handle;
	
	g_bConnected = true;
	CreateTables();
}

void ConnectDB()
{
	if(!g_pZcoreMysql)
		return;
		
	if(g_hSQL != null)
		return;
	
	ZCore_Mysql_Connect("radio_adverts_manager");
	
	if(g_hSQL != null)
	{
		g_bConnected = true;
		CreateTables();
	}
}

void CreateTables()
{
	SQL_SetCharset(g_hSQL, "utf8mb4");
	SQL_TQuery(g_hSQL, CallBack_Empty, "SET NAMES 'utf8mb4';");
	
	SQL_TQuery(g_hSQL, CallBack_Empty, sqlCreateTable);
}

public void CallBack_Empty(Handle owner, Handle hndl, const char[] error, any data)
{
	if (owner == null || hndl == null)
	{
		DatabaseError("CallBack_Empty", error);
		return;
	}
}

/* RAM Forwards */

public int RAM_OnClientAdvertFinished(int iClient, int iProvider, int iTime, bool bAbort)
{
	if(!g_bConnected)
		return;
	
	if(!IsClientInGame(iClient))
		return;
	
	char sAuth[32];
	if(!GetClientAuthId(iClient, AuthId_SteamID64, sAuth, sizeof(sAuth)))
		return;
	
	char sIP[32];
	if(!GetClientIP(iClient, sIP, sizeof(sIP)))
		Format(sIP, sizeof(sIP), "0.0.0.0");
		
	char sCountry[4];
	if(!GeoipCode3(sIP, sCountry))
		Format(sCountry, sizeof(sCountry), "Unknown");
	
	char sName[MAX_NAME_LENGTH];
	if(!GetClientName(iClient, sName, sizeof(sName)))
		Format(sName, sizeof(sName), "Unknown");
	
	char sSafeName[((MAX_NAME_LENGTH * 2) + 1)];
	if(!SQL_EscapeString(g_hSQL, sName, sSafeName, sizeof(sSafeName)))
		return;
	
	char sQuery[512];
	FormatEx(sQuery, sizeof(sQuery), "INSERT INTO `radio_adverts_manager` (ip, port, country, playerID, name, provider, time, aborted) VALUES ('%s', %d, '%s', '%s', '%s', %d, %d, %d)", g_sIP, g_iPort, sCountry, sAuth, sSafeName, iProvider, iTime, bAbort ? 1 : 0);
	SQL_TQuery(g_hSQL, CallBack_Empty, sQuery);
}