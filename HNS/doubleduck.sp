#include <sourcemod>
#include <sdktools>

#define DOUBLEDUCK_HEIGHT 40.0 // Has to be more or equal to 20 units.

public Action OnPlayerRunCmd( int client, int &buttons )
{
    if ( !IsPlayerAlive( client ) ) return Plugin_Continue;
    
    static int fFlags;
    fFlags = GetEntityFlags( client );
    
    if ( fFlags & FL_ONGROUND )
    {
        static bool bAllowDoubleDuck[MAXPLAYERS];
        
        if ( fFlags & FL_DUCKING )
        {
            // We're fully crouched, no doubleducking.
            bAllowDoubleDuck[client] = false;
            return Plugin_Continue;
        }
        
        if ( buttons & IN_DUCK )
        {
            // Next time when we're not holding duck we can do a doubleduck.
            bAllowDoubleDuck[client] = true;
            return Plugin_Continue;
        }
        
        
        // Is transitioning?
        // m_bDucking doesn't actually tell us whether we're uncrouching or crouching. That is why we're using bAllowDoubleDuck.
        if ( GetEntProp( client, Prop_Data, "m_bDucking" ) && bAllowDoubleDuck[client] )
        {
            float vecPos[3];
            GetClientAbsOrigin( client, vecPos );
            vecPos[2] += DOUBLEDUCK_HEIGHT; 
            
            if ( IsValidPlayerPos( client, vecPos ) )
            {
                TeleportEntity( client, vecPos, NULL_VECTOR, NULL_VECTOR );
            }
        }

    }
    
    return Plugin_Continue;
}

public bool IsValidPlayerPos( int client, float vecPos[3] )
{
    static const float vecMins[] = { -16.0, -16.0, 0.0 };
    static const float vecMaxs[] = { 16.0, 16.0, 72.0 };
    
    TR_TraceHullFilter( vecPos, vecPos, vecMins, vecMaxs, MASK_SOLID, TraceFilter_IgnorePlayer, client );
    
    return ( !TR_DidHit( null ) );
}

public bool TraceFilter_IgnorePlayer( int ent, int mask, any ignore_me )
{
    return ( ent != ignore_me );
}