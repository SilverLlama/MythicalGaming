#pragma semicolon 1

#define DEBUG

#define PLUGIN_AUTHOR "SilverLlama"
#define PLUGIN_VERSION "1.00"

#include <sourcemod>
#include <sdktools>
#include <cstrike>
//#include <sdkhooks>

EngineVersion g_Game;

public Plugin myinfo = 
{
	name = "PreSpeed",
	author = PLUGIN_AUTHOR,
	description = "PreSpeed",
	version = PLUGIN_VERSION,
	url = "http://mythical.pro"
};

public void OnPluginStart()
{
	g_Game = GetEngineVersion();
	if(g_Game != Engine_CSGO && g_Game != Engine_CSS)
	{
		SetFailState("This plugin is for CSGO/CSS only.");	
	}
}

bool g_bOnGround[MAXPLAYERS + 1];
int g_js_GroundFrames[MAXPLAYERS + 1] = 0;

public void OnClientPutInServer(client)
{
	g_js_GroundFrames[client] = 0;
}

public Action OnPlayerRunCmd(int client, int &buttons)
{
	if(!IsPlayerAlive(client) || IsFakeClient(client))
	{
		return Plugin_Continue;
	}
	
	if (g_bOnGround[client] && ((buttons & IN_MOVERIGHT) || (buttons & IN_MOVELEFT) || (buttons & IN_BACK) || (buttons & IN_FORWARD)))
		g_js_GroundFrames[client]++;
	
	if (GetEntityFlags(client) & FL_ONGROUND)
			g_bOnGround[client]=true;
		else
		{
			g_bOnGround[client]=false;
			g_js_GroundFrames[client] = 0;
		}
	if (g_js_GroundFrames[client] > 20)		
		PrintToChatAll("This is the groundframes: %i for %N", g_js_GroundFrames[client], client);
	
	if (g_js_GroundFrames[client] > 5)
		SpeedCap(client);
	
	
	return Plugin_Continue;
}



public SpeedCap(client)
{
	if (!IsPlayerAlive(client))
		return;

	static bool:IsOnGround[MAXPLAYERS + 1]; 

	
	decl Float:CurVelVec[3];
	GetEntPropVector(client, Prop_Data, "m_vecVelocity", CurVelVec);	
	
		
	if (g_bOnGround[client])
	{
		if (!IsOnGround[client])
		{
			IsOnGround[client] = true;    
			if(GetSpeed(client) > 255.0)
			{
				SetClientSpeed(client, 0.8);
				
			}
				
		}
	}
	else
		IsOnGround[client] = false;	
}

stock void SetClientSpeed(int iClient, float speed)
{
    SetEntPropFloat(iClient, Prop_Send, "m_flLaggedMovementValue", speed);
}

public Float:GetSpeed(clientid)
{
    decl Float:fVelocity[3];
    GetEntPropVector(clientid, Prop_Data, "m_vecVelocity", fVelocity);
    new Float:speed = SquareRoot(Pow(fVelocity[0],2.0)+Pow(fVelocity[1],2.0));
    return speed;
}
