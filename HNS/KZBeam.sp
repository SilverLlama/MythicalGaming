#pragma semicolon 1

#define DEBUG

#define PLUGIN_AUTHOR "SilverLlama"
#define PLUGIN_VERSION "1.00"

#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <sdkhooks>


EngineVersion g_Game;

new bool:g_bBeam[MAXPLAYERS+1];
new bool:g_bJumpBeam[MAXPLAYERS+1];
new bool:g_borg_JumpBeam[MAXPLAYERS+1];
new bool:g_bOnGround[MAXPLAYERS+1];
new g_Beam[3];
new Float:g_js_fJump_JumpOff_Pos[MAXPLAYERS+1][3];
new Float:g_fLastPosition[MAXPLAYERS + 1][3];

public Plugin myinfo = 
{
	name = "KZBeam",
	author = PLUGIN_AUTHOR,
	description = "...",
	version = PLUGIN_VERSION,
	url = "http://mythical.pro"
};

public void OnPluginStart()
{
	RegConsoleCmd("sm_beam", Client_PlayerJumpBeam);
}

public OnMapStart(){
	InitPrecache();
}


public InitPrecache()
{
	g_Beam[1] = PrecacheModel("materials/sprites/bluelaser1.vmt", true);
}

public Action:Event_OnJump(Handle:Event2, const String:Name[], bool:Broadcast)
{	
	decl client;
	client = GetClientOfUserId(GetEventInt(Event2, "userid"));
	g_bBeam[client]=true;
	Prethink(client, false);
}

public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon, &subtype, &cmdnum, &tickcount, &seed, mouse[2])
{
	
	decl Float:origin[3];
	GetClientAbsOrigin(client, origin);
	
	SetPlayerBeam(client, origin);
	
	if (GetEntityFlags(client) & FL_ONGROUND)	
			g_bOnGround[client]=true;
		else
			g_bOnGround[client]=false;
	
	
	if (g_bOnGround[client])
	{
			g_bBeam[client] = false;
			g_js_fJump_JumpOff_Pos[client] = origin;
	}	
		else
			g_bBeam[client] = true;
			
	g_fLastPosition[client] = origin;
}

public SetPlayerBeam(client, Float:origin[3])
{
	if(!g_bBeam[client] || g_bOnGround[client])
		return;
	
	
	new Float:v1[3], Float:v2[3];
	v1[0] = origin[0];
	v1[1] = origin[1];
	v1[2] = g_js_fJump_JumpOff_Pos[client][2];
	v2[0] = g_fLastPosition[client][0];
	v2[1] = g_fLastPosition[client][1];
	v2[2] = g_js_fJump_JumpOff_Pos[client][2];
	new color[4] = {255, 255, 255, 100};
	TE_SetupBeamPoints(v1, v2, g_Beam[1], 0, 0, 0, 2.5, 3.0, 3.0, 10, 0.0, color, 0);
	if (g_bJumpBeam[client])
		TE_SendToClient(client);
	
		
}

public Action:Client_PlayerJumpBeam(client, args)
{
	PlayerJumpBeam(client);
	if (g_bJumpBeam[client])
		PrintToChat(client, "[Mythical Beam] Beam has been enabled!");
	else
		PrintToChat(client, "[Mythical Beam] Beam has been disabled!");
	return Plugin_Handled;
}

public PlayerJumpBeam(client)
{
	if (g_bJumpBeam[client])
		g_bJumpBeam[client] = false;
	else
		g_bJumpBeam[client] = true;
}

public Prethink (client, bool:ladderjump)
{
	GetGroundOrigin(client, g_js_fJump_JumpOff_Pos[client]);	
}

stock GetGroundOrigin(client, Float:pos[3])
{
	decl Float:fOrigin[3], Float:result[3];
	GetClientAbsOrigin(client, fOrigin);
	TraceClientGroundOrigin(client, result, 100.0);
	pos = fOrigin;
	pos[2] = result[2];
}

stock TraceClientGroundOrigin(client, Float:result[3], Float:offset)
{
	decl Float:temp[2][3];
	GetClientEyePosition(client, temp[0]);
	temp[1] = temp[0];
	temp[1][2] -= offset;
	new Float:mins[] ={-16.0, -16.0, 0.0};
	new Float:maxs[] =	{16.0, 16.0, 60.0};
	new Handle:trace = TR_TraceHullFilterEx(temp[0], temp[1], mins, maxs, MASK_SHOT, TraceEntityFilterPlayer);
	if(TR_DidHit(trace))
	{
		TR_GetEndPosition(result, trace);
		CloseHandle(trace);
		return 1;
	}
	CloseHandle(trace);
	return 0;
}

public bool:TraceEntityFilterPlayer(entity, contentsMask)
{
    return entity > MaxClients;
}