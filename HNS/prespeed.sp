#pragma semicolon 1

#define DEBUG

#define PLUGIN_AUTHOR "SilverLlama"
#define PLUGIN_VERSION "1.00"

#include <sourcemod>
#include <sdktools>
#include <cstrike>
//#include <sdkhooks>

EngineVersion g_Game;

public Plugin myinfo = 
{
	name = "PreSpeed",
	author = PLUGIN_AUTHOR,
	description = "PreSpeed",
	version = PLUGIN_VERSION,
	url = "http://mythical.pro"
};

public void OnPluginStart()
{
	g_Game = GetEngineVersion();
	if(g_Game != Engine_CSGO && g_Game != Engine_CSS)
	{
		SetFailState("This plugin is for CSGO/CSS only.");	
	}
}

float g_fLastAngles[MAXPLAYERS + 1][3];
new Float:g_fVelocityModifierLastChange[MAXPLAYERS + 1];
float g_PrestrafeVelocity[MAXPLAYERS + 1];
int g_PrestrafeFrameCounter[MAXPLAYERS + 1];
bool g_bOnGround[MAXPLAYERS + 1];
float g_fBhopSpeedCap = 277.0;
float g_fTeleportValidationTime[MAXPLAYERS + 1];



public Action OnPlayerRunCmd(int client, int &buttons)
{
	if(!IsPlayerAlive(client) || IsFakeClient(client))
	{
		return Plugin_Continue;
	}
	new Float:ang[3];
	GetClientEyeAngles(client, ang);
	
	if (GetEntityFlags(client) & FL_ONGROUND)
			g_bOnGround[client]=true;
		else
			g_bOnGround[client]=false;
	
	
	SpeedCap(client);
	Prestrafe(client,ang[1], buttons);
	
	g_fLastAngles[client] = ang;
	
	return Plugin_Continue;
}


public OnClientPutInServer(client)
{
	g_PrestrafeVelocity[client] = 1.0;
	g_PrestrafeFrameCounter[client] = 0;
	g_fTeleportValidationTime[client] = 999999.9;
}


public SpeedCap(client)
{
	if (!IsPlayerAlive(client))
		return;

	static bool:IsOnGround[MAXPLAYERS + 1]; 

	
	decl Float:CurVelVec[3];
	GetEntPropVector(client, Prop_Data, "m_vecVelocity", CurVelVec);	
	
		
	if (g_bOnGround[client])
	{
		if (!IsOnGround[client])
		{
			IsOnGround[client] = true;    
			if (GetVectorLength(CurVelVec) > g_fBhopSpeedCap)
			{
				
				NormalizeVector(CurVelVec, CurVelVec);
				ScaleVector(CurVelVec, g_fBhopSpeedCap);
				DoValidTeleport(client, NULL_VECTOR, NULL_VECTOR, CurVelVec);
			}
		}
	}
	else
		IsOnGround[client] = false;	
}

public DoValidTeleport(client, Float:origin[3],Float:angles[3],Float:vel[3])
{
	if (!IsFakeClient(client))
		return;
	g_fTeleportValidationTime[client] = GetEngineTime() + 0.25;
	TeleportEntity(client, origin, angles, vel);
}

public Prestrafe(client, Float: ang, &buttons)
{		
	if (!IsPlayerAlive(client) || !(GetEntityFlags(client) & FL_ONGROUND))
		return;

	//decl.
	new Float:flDefaultKnifeSpeed = 1.0;
	new Float:flMaxKnifeSpeed = 1.104;	
	new Float:flDefaultUspSpeed= 1.041667;
	new Float:flMaxUspSpeed = 1.15;			
	new bool: turning_right;
	new bool: turning_left;	
	decl MaxFrameCount;	
	decl Float: IncSpeed, Float: DecSpeed;
	new Float: speed = GetSpeed(client);
	new bool: bForward;
	
	//get weapon
	decl String:classname[64];
	GetClientWeapon(client, classname, 64);

	if ((!StrEqual(classname, "weapon_hkp2000") && !StrEqual(classname, "weapon_knife")))
	{				
		if (StrEqual(classname, "weapon_hkp2000"))
			SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", flDefaultUspSpeed);
		else
			SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", flDefaultKnifeSpeed);	
		return;
	}
	// get turning direction
	if( ang < g_fLastAngles[client][1])
		turning_right = true;
	else 
		if( ang > g_fLastAngles[client][1])
			turning_left = true;	
	
	//get moving direction
	if (GetClientMovingDirection(client,false) > 0.0)
		bForward=true;
			

	new Float: flVelMd =	GetEntPropFloat(client, Prop_Send, "m_flVelocityModifier");
	if (StrEqual(classname, "weapon_knife") && flVelMd > flMaxKnifeSpeed+0.007)
		SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", flMaxKnifeSpeed-0.001);		
			
	//no mouse movement?
	if (!turning_right && !turning_left)
	{
		decl Float: diff;
		diff = GetEngineTime() - g_fVelocityModifierLastChange[client];
		if (diff > 0.2)
		{
			if(StrEqual(classname, "weapon_hkp2000"))
				g_PrestrafeVelocity[client] = flDefaultUspSpeed;
			else
				g_PrestrafeVelocity[client] = flDefaultKnifeSpeed;
			g_fVelocityModifierLastChange[client] = GetEngineTime();
			SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", g_PrestrafeVelocity[client]);
		}
		return;
	}
	if (((buttons & IN_MOVERIGHT) || (buttons & IN_MOVELEFT)) && speed > 248.9)
	{  
		//tickrate depending values
		
		MaxFrameCount = 75;	
		IncSpeed = 0.0009;
		if ((g_PrestrafeVelocity[client] > 1.08 && StrEqual(classname, "weapon_hkp2000")) || (g_PrestrafeVelocity[client] > 1.04 && !StrEqual(classname, "weapon_hkp2000")))
			IncSpeed = 0.001;			
		DecSpeed = 0.0045;
		
		if (((buttons & IN_MOVERIGHT && turning_right || turning_left && !bForward)) || ((buttons & IN_MOVELEFT && turning_left || turning_right && !bForward)))
		{		
			g_PrestrafeFrameCounter[client]++;						
			//Add speed if Prestrafe frames are less than max frame count	
			if (g_PrestrafeFrameCounter[client] < MaxFrameCount)
			{	
				//increase speed
				g_PrestrafeVelocity[client]+= IncSpeed;
				//usp
				if(StrEqual(classname, "weapon_hkp2000"))
				{		
					if (g_PrestrafeVelocity[client] > flMaxUspSpeed)
						g_PrestrafeVelocity[client]-=0.007;					
				}
				else
				{
					if (g_PrestrafeVelocity[client] > flMaxKnifeSpeed)
					{
						if (g_PrestrafeVelocity[client] > flMaxKnifeSpeed+0.007)
							g_PrestrafeVelocity[client] = flMaxKnifeSpeed-0.001;
						else
							g_PrestrafeVelocity[client]-=0.007;
					}
				}
				g_PrestrafeVelocity[client]+= IncSpeed;
			}
			else
			{
				//decrease speed
				g_PrestrafeVelocity[client]-= DecSpeed;
				g_PrestrafeFrameCounter[client] = g_PrestrafeFrameCounter[client] - 2;
				
				//usp reset 250.0 speed
				if(StrEqual(classname, "weapon_hkp2000"))
				{
					if (g_PrestrafeVelocity[client]< flDefaultUspSpeed)
					{
						g_PrestrafeFrameCounter[client] = 0;
						g_PrestrafeVelocity[client]= flDefaultUspSpeed;
					}
				}
				else	
					//knife reset 250.0 speed
					if (g_PrestrafeVelocity[client]< flDefaultKnifeSpeed)
					{	
						g_PrestrafeFrameCounter[client] = 0;
						g_PrestrafeVelocity[client]= flDefaultKnifeSpeed;	
					}			
			}
		}
		else
		{
			g_PrestrafeVelocity[client] -= 0.04;
			if(StrEqual(classname, "weapon_hkp2000"))
			{
				if (g_PrestrafeVelocity[client]< flDefaultUspSpeed)
					g_PrestrafeVelocity[client]= flDefaultUspSpeed;
			}
			else						
			if (g_PrestrafeVelocity[client]< flDefaultKnifeSpeed)
				g_PrestrafeVelocity[client]= flDefaultKnifeSpeed;		
		}
		
		//Set VelocityModifier	
		SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", g_PrestrafeVelocity[client]);
		g_fVelocityModifierLastChange[client] = GetEngineTime();		
	}
	else
	{
		if(StrEqual(classname, "weapon_hkp2000"))
			SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", flDefaultUspSpeed);
		else
			SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", flDefaultKnifeSpeed);
		g_PrestrafeFrameCounter[client] = 0;
	}
}


stock Float:GetClientMovingDirection(client, bool:ladder)
{
	new Float:fVelocity[3];
	GetEntPropVector(client, Prop_Data, "m_vecAbsVelocity", fVelocity);
	      
	new Float:fEyeAngles[3];
	GetClientEyeAngles(client, fEyeAngles);

	if(fEyeAngles[0] > 70.0) fEyeAngles[0] = 70.0;
	if(fEyeAngles[0] < -70.0) fEyeAngles[0] = -70.0;

	new Float:fViewDirection[3];
	
	if (ladder)
		GetEntPropVector(client, Prop_Send, "m_vecLadderNormal", fViewDirection);	
	else
		GetAngleVectors(fEyeAngles, fViewDirection, NULL_VECTOR, NULL_VECTOR);
	   
	NormalizeVector(fVelocity, fVelocity);
	NormalizeVector(fViewDirection, fViewDirection);

	new Float:direction = GetVectorDotProduct(fVelocity, fViewDirection);
	if (ladder)
		direction = direction * -1;
	return direction;
}

public Float:GetSpeed(client)
{
	decl Float:fVelocity[3];
	GetEntPropVector(client, Prop_Data, "m_vecVelocity", fVelocity);
	new Float:speed = SquareRoot(Pow(fVelocity[0],2.0)+Pow(fVelocity[1],2.0));
	return speed;
}