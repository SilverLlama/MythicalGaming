#pragma semicolon 1

#define DEBUG

#define PLUGIN_AUTHOR "SilverLlama"
#define PLUGIN_VERSION "1.00"

#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <sdkhooks>
 #include <smlib/weapons>
#include <smlib>
#include <ttt>

EngineVersion g_Game;

bool bGiven[MAXPLAYERS + 1];

public Plugin myinfo = 
{
	name = "USP",
	author = PLUGIN_AUTHOR,
	description = "Gives player an usp",
	version = PLUGIN_VERSION,
	url = "http://mythical.pro"
};

public void OnPluginStart()
{
	g_Game = GetEngineVersion();
	if(g_Game != Engine_CSGO && g_Game != Engine_CSS)
	{
		SetFailState("This plugin is for CSGO/CSS only.");	
	}
	
	RegConsoleCmd("sm_usp", Client_GiveUSP);
	RegConsoleCmd("sm_glock", Client_GiveGlock);
	//RegAdminCmd("sm_usp", Client_GiveUSP, ADMFLAG_ROOT);
	
	HookEvent("round_start", OnRoundStart, EventHookMode_PostNoCopy);
	
}

public void OnClientDisconnect(client)
{
		SDKUnhook(client, SDKHook_WeaponEquipPost, OnPostWeaponEquip);
}

public void OnClientPutInServer(client)
{
		SDKHook(client, SDKHook_WeaponEquipPost, OnPostWeaponEquip);
}

public Action OnRoundStart(Event hEvent, const char[] sName, bool bDontBroadcast)
{
	LoopValidClients(i)
		bGiven[i] = false;
}

public Action:Client_GiveUSP(client, args)
{
	if(!IsPlayerAlive(client))
	{
		PrintToChat(client, "[USP] You're not alive!");
		return Plugin_Handled;
	}
	
	if(GetClientTeam(client) != CS_TEAM_T)
	{
		PrintToChat(client, "[USP] Only terrorist can claim this gun!");
		return Plugin_Handled;
	}
	
	if(bGiven[client])
	{
		PrintToChat(client, "[USP] You can only claim one gun each round!");
		return Plugin_Handled;
	}
	
	bGiven[client] = true;

	GivePlayerItem(client, "weapon_usp_silencer");
	
	return Plugin_Handled;
}

public Action:Client_GiveGlock(client, args)
{
	if(!IsPlayerAlive(client))
	{
		PrintToChat(client, "[USP] You're not alive!");
		return Plugin_Handled;
	}
	
	if(GetClientTeam(client) != CS_TEAM_T)
	{
		PrintToChat(client, "[USP] Only terrorist can claim this gun!");
		return Plugin_Handled;
	}
	
	if(bGiven[client])
	{
		PrintToChat(client, "[USP] You can only claim one gun each round!");
		return Plugin_Handled;
	}
	
	bGiven[client] = true;

	GivePlayerItem(client, "weapon_glock");
	
	return Plugin_Handled;
}

public Action OnPostWeaponEquip(int client, int weapon)
{
	if(weapon == -1) return;
	
	Handle dataPackHandle;
	CreateDataTimer(0.0, Timer_SetCorrectAmmo, dataPackHandle);
	
	WritePackCell(dataPackHandle, EntIndexToEntRef(weapon));
}

public Action Timer_SetCorrectAmmo(Handle timer, Handle dataPackHandle)
{
	ResetPack(dataPackHandle);
	
	int weapon = EntRefToEntIndex(ReadPackCell(dataPackHandle));

	if(weapon == -1) 
		return;
	
	SetEntProp(weapon, Prop_Send, "m_iPrimaryReserveAmmoCount", 0);
	SetEntProp(weapon, Prop_Send, "m_iSecondaryReserveAmmoCount", 0);
	SetEntProp(weapon, Prop_Send, "m_iClip1", 0);
	SetEntProp(weapon, Prop_Send, "m_iClip2", 0);
}

