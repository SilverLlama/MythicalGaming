#include <sourcemod>
#include <calladmin>
#include <discord>

#define DISCORD_MSG "{\"attachments\": [{\"color\": \"#ff2222\",\"title\": \"{HOSTNAME} (steam://connect/{SERVER_IP}:{SERVER_PORT}){REFER_ID}\",\"fields\": [{\"title\": \"Reason\",\"value\": \"{REASON}\",\"short\": true},{\"title\": \"Reporter\",\"value\": \"{REPORTER_NAME} ({REPORTER_ID})\",\"short\": true},{\"title\": \"Staff Team\",\"value\": \"@Forum Staff\",\"short\": true},{\"title\": \"Target\",\"value\": \"{TARGET_NAME} ({TARGET_ID})\",\"short\": true}]}]}"

char sSymbols[25][1] = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

char g_sHostPort[6];
char g_sServerName[256];
char g_sHostIP[16];

ConVar g_cColor = null;
ConVar g_cRefer = null;
ConVar g_cRemove = null;
ConVar g_cRemove2 = null;
ConVar g_cWebhook = null;

public Plugin myinfo = 
{
	name = "Discord: Calladmin",
	author = ".#Zipcore",
	description = "",
	version = CALLADMIN_VERSION,
	url = "www.zipcore.net"
}

public void OnPluginStart()
{
	g_cColor = CreateConVar("discord_calladmin_color", "#ff2222", "Discord/Slack attachment color.");
	g_cRefer = CreateConVar("discord_calladmin_refer", "1", "If enabled add a random refer ID to the report title.");
	g_cRemove = CreateConVar("discord_calladmin_remove", " | By PulseServers.com", "Remove this part from servername before sending the report.");
	g_cRemove2 = CreateConVar("discord_calladmin_remove2", "3kliksphilip.com | ", "Remove this part from servername before sending the report.");
	g_cWebhook = CreateConVar("discord_calladmin_webhook", "calladmin", "Config key from configs/discord.cfg.");
	
	AutoExecConfig(true, "discord_calladmin");
}

public void OnAllPluginsLoaded()
{
	if (!LibraryExists("calladmin"))
	{
		SetFailState("CallAdmin not found");
		return;
	}
	IntToString(CallAdmin_GetHostPort(), g_sHostPort, sizeof(g_sHostPort));
	CallAdmin_GetHostIP(g_sHostIP, sizeof(g_sHostIP));
	CallAdmin_GetHostName(g_sServerName, sizeof(g_sServerName));
}

public void CallAdmin_OnServerDataChanged(ConVar convar, ServerData type, const char[] oldVal, const char[] newVal)
{
	if (type == ServerData_HostIP)
		CallAdmin_GetHostIP(g_sHostIP, sizeof(g_sHostIP));
	else if (type == ServerData_HostName)
		CallAdmin_GetHostName(g_sServerName, sizeof(g_sServerName));
	else if (type == ServerData_HostPort)
		IntToString(CallAdmin_GetHostPort(), g_sHostPort, sizeof(g_sHostPort));
}

public void CallAdmin_OnReportPost(int client, int target, const char[] reason)
{
	char sColor[8];
	g_cColor.GetString(sColor, sizeof(sColor));
	
	char sReason[(REASON_MAX_LENGTH + 1) * 2];
	strcopy(sReason, sizeof(sReason), reason);
	EscapeString(sReason, sizeof(sReason));
	
	char clientAuth[21];
	char clientName[(MAX_NAME_LENGTH + 1) * 2];
	
	if (client == REPORTER_CONSOLE)
	{
		strcopy(clientName, sizeof(clientName), "Server");
		strcopy(clientAuth, sizeof(clientAuth), "CONSOLE");
	}
	else
	{
		GetClientAuthId(client, AuthId_Steam2, clientAuth, sizeof(clientAuth));
		GetClientName(client, clientName, sizeof(clientName));
		EscapeString(clientName, sizeof(clientName));
	}
	
	char targetAuth[21];
	char targetName[(MAX_NAME_LENGTH + 1) * 2];
	
	GetClientAuthId(target, AuthId_Steam2, targetAuth, sizeof(targetAuth));
	GetClientName(target, targetName, sizeof(targetName));
	EscapeString(targetName, sizeof(targetName));
	
	EscapeString(g_sServerName, sizeof(g_sServerName));
	
	char sRemove[32];
	g_cRemove.GetString(sRemove, sizeof(sRemove));
	ReplaceString(g_sServerName, sizeof(g_sServerName), sRemove, "");
	
	g_cRemove2.GetString(sRemove, sizeof(sRemove));
	ReplaceString(g_sServerName, sizeof(g_sServerName), sRemove, "");
	
	char sMessage[4096] = DISCORD_MSG;
	
	ReplaceString(sMessage, sizeof(sMessage), "{COLOR}", sColor);
	
	ReplaceString(sMessage, sizeof(sMessage), "{HOSTNAME}", g_sServerName);
	ReplaceString(sMessage, sizeof(sMessage), "{SERVER_IP}", g_sHostIP);
	ReplaceString(sMessage, sizeof(sMessage), "{SERVER_PORT}", g_sHostPort);
	
	ReplaceString(sMessage, sizeof(sMessage), "{REASON}", sReason);
	
	ReplaceString(sMessage, sizeof(sMessage), "{REPORTER_NAME}", clientName);
	ReplaceString(sMessage, sizeof(sMessage), "{REPORTER_ID}", clientAuth);
	
	ReplaceString(sMessage, sizeof(sMessage), "{TARGET_NAME}", targetName);
	ReplaceString(sMessage, sizeof(sMessage), "{TARGET_ID}", targetAuth);
	
	char sRefer[16];
	if(g_cRefer.BoolValue)
		Format(sRefer, sizeof(sRefer), " # %s%s-%d%d", sSymbols[GetRandomInt(0, 25-1)], sSymbols[GetRandomInt(0, 25-1)], GetRandomInt(0, 9), GetRandomInt(0, 9));
	ReplaceString(sMessage, sizeof(sMessage), "{REFER_ID}", sRefer);
	
	SendMessage(sMessage);
}

SendMessage(char[] sMessage)
{
	char sWebhook[32];
	g_cWebhook.GetString(sWebhook, sizeof(sWebhook));
	Discord_SendMessage(sWebhook, sMessage);
}

stock void EscapeString(char[] string, int maxlen)
{
	ReplaceString(string, maxlen, "@", "＠");
	ReplaceString(string, maxlen, "'", "＇");
	ReplaceString(string, maxlen, "\"", "＂");
}