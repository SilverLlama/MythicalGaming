#include <sourcemod>
#include <discord>

#define MSG_BAN "{\"attachments\": [{\"color\": \"#ff2222\",\"title\": \"View on Sourcebans\",\"title_link\": \"https://sb.eu.3kliksphilip.com/index.php?p=banlist&searchText={STEAMID}\",\"fields\": [{\"title\": \"Player\",\"value\": \"{NICKNAME} ( {STEAMID} )\",\"short\": false},{\"title\": \"Ban Length\",\"value\": \"{BANLENGTH}\",\"short\": true},{\"title\": \"Reason\",\"value\": \"{REASON}\",\"short\": true}]}]}"

ConVar g_cWebhook = null;

public Plugin myinfo = 
{
	name = "Discord: Sourcebans",
	author = ".#Zipcore",
	description = "",
	version = "1.0",
	url = "www.zipcore.net"
}

public void OnPluginStart()
{
	g_cWebhook = CreateConVar("discord_sourcebans_webhook", "sourcebans", "Config key from configs/discord.cfg.");
	
	AutoExecConfig(true, "discord_sourcebans");
}

public int OnSBBanPlayer(int client, int target, int time, char[] reason)
{
	char sAuth[32];
	GetClientAuthId(target, AuthId_Steam2, sAuth, sizeof(sAuth));
	
	char sName[32];
	GetClientName(target, sName, sizeof(sName));
	
	char sLength[32];
	if(time == 0)
	{
		sLength = "Permanent";
	}
	else if (time >= 525600)
	{
		int years = RoundToFloor(time / 525600.0);
		Format(sLength, sizeof(sLength), "%d mins (%d year%s)", time, years, years == 1 ? "" : "s");
    }
	else if (time >= 10080)
	{
		int weeks = RoundToFloor(time / 10080.0);
		Format(sLength, sizeof(sLength), "%d mins (%d week%s)", time, weeks, weeks == 1 ? "" : "s");
    }
	else if (time >= 1440)
	{
		int days = RoundToFloor(time / 1440.0);
		Format(sLength, sizeof(sLength), "%d mins (%d day%s)", time, days, days == 1 ? "" : "s");
    }
	else if (time >= 60)
	{
		int hours = RoundToFloor(time / 60.0);
		Format(sLength, sizeof(sLength), "%d mins (%d hour%s)", time, hours, hours == 1 ? "" : "s");
    }
	else if (time > 0) Format(sLength, sizeof(sLength), "%d min%s", time, time == 1 ? "" : "s");
	else return;
    
	EscapeString(sName, strlen(sName));
	
	char sMSG[2048] = MSG_BAN;
	
	ReplaceString(sMSG, sizeof(sMSG), "{STEAMID}", sAuth);
	ReplaceString(sMSG, sizeof(sMSG), "{REASON}", reason);
	ReplaceString(sMSG, sizeof(sMSG), "{BANLENGTH}", sLength);
	ReplaceString(sMSG, sizeof(sMSG), "{NICKNAME}", sName);
	
	SendMessage(sMSG);
}

SendMessage(char[] sMessage)
{
	char sWebhook[32];
	g_cWebhook.GetString(sWebhook, sizeof(sWebhook));
	Discord_SendMessage(sWebhook, sMessage);
}

stock void EscapeString(char[] string, int maxlen)
{
	ReplaceString(string, maxlen, "@", "＠");
	ReplaceString(string, maxlen, "'", "＇");
	ReplaceString(string, maxlen, "\"", "＂");
}