#include <sourcemod>
#include <SteamWorks>

public Plugin myinfo = 
{
	name = "Discord API",
	author = ".#Zipcore, Credits: shavit",
	description = "",
	version = "1.0",
	url = "www.zipcore.net"
};

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	CreateNative("Discord_SendMessage", Native_SendMessage);
	
	RegPluginLibrary("discord");
	
	return APLRes_Success;
}

public int Native_SendMessage(Handle plugin, int numParams)
{
	char sWebhook[64]
	GetNativeString(1, sWebhook, sizeof(sWebhook));
	
	char sMessage[4096];
	GetNativeString(2, sMessage, sizeof(sMessage));
	
	char sUrl[512];
	if(!GetWebHook(sWebhook, sUrl, sizeof(sUrl)))
		return;
	
	Handle hRequest = SteamWorks_CreateHTTPRequest(k_EHTTPMethodPOST, sUrl);
	SteamWorks_SetHTTPRequestRawPostBody(hRequest, "application/json", sMessage, strlen(sMessage));
	SteamWorks_SetHTTPCallbacks(hRequest, view_as<SteamWorksHTTPRequestCompleted>(OnRequestComplete));
	SteamWorks_SendHTTPRequest(hRequest);
}

public void OnRequestComplete(Handle hRequest, bool bFailed, bool bRequestSuccessful, EHTTPStatusCode eStatusCode)
{
	if(!bFailed && bRequestSuccessful && eStatusCode != k_EHTTPStatusCode200OK && eStatusCode != k_EHTTPStatusCode204NoContent)
	{
		LogError("[OnRequestComplete] Error Code: [%d]", eStatusCode);
		SteamWorks_GetHTTPResponseBodyCallback(hRequest, GetBadResponse);
	}
	
	delete hRequest;
}

public GetBadResponse(const char[] sData)
{
	PrintToServer("[GetBadResponse] %s", sData);
}

bool GetWebHook(const char[] sWebhook, char[] sUrl, int iLength)
{
	KeyValues kv = new KeyValues("Discord");
	
	char sFile[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, sFile, sizeof(sFile), "configs/discord.cfg");

	if (!FileExists(sFile))
	{
		SetFailState("[GetWebHook] \"%s\" not found!", sFile);
		return false;
	}

	kv.ImportFromFile(sFile);

	if (!kv.GotoFirstSubKey())
	{
		SetFailState("[GetWebHook] Can't find webhook for \"%s\"!", sFile);
		return false;
	}
	
	char sBuffer[64];
	
	do
	{
		kv.GetSectionName(sBuffer, sizeof(sBuffer));
		
		if(StrEqual(sBuffer, sWebhook, false))
		{
			kv.GetString("url", sUrl, iLength);
			delete kv;
			return true;
		}
	}
	while (kv.GotoNextKey());
	
	delete kv;
	
	return false;
}
