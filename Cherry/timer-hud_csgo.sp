#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <clientprefs>
#include <smlib>
#include <timer>
#include <timer-logging>
#include <timer-stocks>
#include <timer-config_loader>
#include <mapchooser_extended>

#undef REQUIRE_PLUGIN
#include <timer-mapzones>
#include <timer-teams>
#include <timer-maptier>
#include <timer-rankings>
#include <timer-worldrecord>
#include <timer-physics>
#include <js_ljstats>

#define THINK_INTERVAL							1.0




//module check
new String:g_currentMap[64];
new Handle:g_cvarTimeLimit;
new bool:g_timerPhysics;
new bool:g_timerMapzones;
new bool:g_timerLjStats;
new bool:g_timerRankings;
new bool:g_timerWorldRecord;
new bool:spec[66];
new bool:hidemyass[66];
new bool:B_IsJump[66];
new bool:B_show_hud = true;
new g_iButtonsPressed[66];
new g_iJumps[66];
new Handle:g_hDelayJump[66];
new Handle:g_hThink_Map;
new g_iMap_TimeLeft = 1200;
new Float:gF_AngleCache[66];
new goodGains[66];
new totalMeasures[66];
new Float:prevSpeed[66];
new Float:prevSSpeed[66];
new Float:fspeed[66];
new Float:sspeed[66];
new Float:speedChange[66];
new Float:speedSixth[66];
new Float:countSixth[66];
new Handle:H_TimerHUD;
new Handle:cookieHudPref;
new Handle:cookieHudMainPref;
new Handle:cookieHudMainTimePref;
new Handle:cookieHudMainJumpsPref;
new Handle:cookieHudMainSpeedPref;
new Handle:cookieHudMainSpecPref;
new Handle:cookieHudMainJumpsAccPref;
new Handle:cookieHudMainSpeedMaxPref;
new Handle:cookieHudSidePref;
new Handle:cookieHudSideMapPref;
new Handle:cookieHudSideModePref;
new Handle:cookieHudSideWRPref;
new Handle:cookieHudSideRankPref;
new Handle:cookieHudSidePBPref;
new Handle:cookieHudSideTTWRPref;
new Handle:cookieHudSideKeysPref;
new Handle:cookieHudSideSpecPref;
new Handle:cookieHudSideSteamPref;
new Handle:cookieHudSideLevelPref;
new Handle:cookieHudSideTimeleftPref;
new Handle:cookieHudSidePointsPref;
new hudSettings[22][66];
new Panel:panel[66];
public Plugin:myinfo =
{
	name		= "[TIMER] HUD",
	author		= "Zipcore, Alongub, DR. API Improvements",
	description = "[Timer] Player HUD with optional details to show and cookie support",
	version		= PL_VERSION,
	url			= "forums.alliedmods.net/showthread.php?p=2074699"
};

public OnPluginStart()
{
	if (GetEngineVersion() != Engine_CSGO)
	{
		Timer_LogError("Don't use this plugin for other games than CS:GO.");
		SetFailState("Check timer error logs.");
		return;
	}
	g_timerPhysics = LibraryExists("timer-physics");
	g_timerMapzones = LibraryExists("timer-mapzones");
	g_timerLjStats = LibraryExists("timer-ljstats");
	g_timerRankings = LibraryExists("timer-rankings");
	g_timerWorldRecord = LibraryExists("timer-worldrecord");
	LoadPhysics();
	LoadTimerSettings();
	LoadTranslations("drapi/drapi_timer-hud_csgo.phrases");
	if (g_Settings[154])
	{
		HookEvent("player_jump", Event_PlayerJump, EventHookMode:1);
		HookEvent("player_death", Event_Reset, EventHookMode:1);
		HookEvent("player_team", Event_Reset, EventHookMode:1);
		HookEvent("player_spawn", Event_Reset, EventHookMode:1);
		HookEvent("player_disconnect", Event_Reset, EventHookMode:1);
		RegConsoleCmd("sm_hidemyass", Cmd_HideMyAss, "", 0);
		RegConsoleCmd("sm_hud", MenuHud, "", 0);
		g_cvarTimeLimit = FindConVar("mp_timelimit");
		AutoExecConfig(true, "timer/timer-hud", "sourcemod");
		cookieHudPref = RegClientCookie("timer_hud_master", "Turn on or off all hud components", CookieAccess:2);
		cookieHudMainPref = RegClientCookie("timer_hud_main", "Turn on or off main hud components", CookieAccess:2);
		cookieHudMainTimePref = RegClientCookie("timer_hud_main_time", "Turn on or off time component", CookieAccess:2);
		cookieHudMainJumpsPref = RegClientCookie("timer_hud_jumps", "Turn on or off jumps component", CookieAccess:2);
		cookieHudMainJumpsAccPref = RegClientCookie("timer_hud_jump_acc", "Turn on or off jumps accuracy component", CookieAccess:2);
		cookieHudMainSpeedPref = RegClientCookie("timer_hud_speed", "Turn on or off speed component", CookieAccess:2);
		cookieHudMainSpeedMaxPref = RegClientCookie("timer_hud_speed_max", "Turn on or off max speed component", CookieAccess:2);
		cookieHudMainSpecPref = RegClientCookie("timer_hud_spec", "Turn on or off spec", CookieAccess:2);
		cookieHudSidePref = RegClientCookie("timer_hud_side", "Turn on or off side hud component", CookieAccess:2);
		cookieHudSideMapPref = RegClientCookie("timer_hud_side_map", "Turn on or off map component", CookieAccess:2);
		cookieHudSideModePref = RegClientCookie("timer_hud_side_mode", "Turn on or off mode component", CookieAccess:2);
		cookieHudSideWRPref = RegClientCookie("timer_hud_side_wr", "Turn on or off wr component", CookieAccess:2);
		cookieHudSideRankPref = RegClientCookie("timer_hud_side_rank", "Turn on or off rank component", CookieAccess:2);
		cookieHudSidePBPref = RegClientCookie("timer_hud_side_pb", "Turn on or off pb component", CookieAccess:2);
		cookieHudSideTTWRPref = RegClientCookie("timer_hud_side_ttwr", "Turn on or off ttwr component", CookieAccess:2);
		cookieHudSideKeysPref = RegClientCookie("timer_hud_side_keys", "Turn on or off keys component", CookieAccess:2);
		cookieHudSideSpecPref = RegClientCookie("timer_hud_side_spec", "Turn on or off speclist component", CookieAccess:2);
		cookieHudSideSteamPref = RegClientCookie("timer_hud_side_steam", "Turn on or off steam component", CookieAccess:2);
		cookieHudSideLevelPref = RegClientCookie("timer_hud_side_level", "Turn on or off level component", CookieAccess:2);
		cookieHudSideTimeleftPref = RegClientCookie("timer_hud_side_timeleft", "Turn on or off timeleft component", CookieAccess:2);
		cookieHudSidePointsPref = RegClientCookie("timer_hud_side_points", "Turn on or off points component", CookieAccess:2);
	}
	new client = 1;
	while (client <= MaxClients)
	{
		OnClientCookiesCached(client);
		client++;
	}
	return;
}

public OnLibraryAdded(const String:name[])
{
	if (StrEqual(name, "timer-physics", true))
	{
		g_timerPhysics = true;
	}
	else
	{
		if (StrEqual(name, "timer-mapzones", true))
		{
			g_timerMapzones = true;
		}
		if (StrEqual(name, "timer-ljstats", true))
		{
			g_timerLjStats = true;
		}
		if (StrEqual(name, "timer-rankings", true))
		{
			g_timerRankings = true;
		}
		if (StrEqual(name, "timer-worldrecord", true))
		{
			g_timerWorldRecord = true;
		}
	}
	return;
}

public OnLibraryRemoved(const String:name[])
{
	if (StrEqual(name, "timer-physics", true))
	{
		g_timerPhysics = false;
	}
	else
	{
		if (StrEqual(name, "timer-mapzones", true))
		{
			g_timerMapzones = false;
		}
		if (StrEqual(name, "timer-ljstats", true))
		{
			g_timerLjStats = false;
		}
		if (StrEqual(name, "timer-rankings", true))
		{
			g_timerRankings = false;
		}
		if (StrEqual(name, "timer-worldrecord", true))
		{
			g_timerWorldRecord = false;
		}
	}
	return;
}

public OnMapStart()
{
	LoadPhysics();
	LoadTimerSettings();
	for (new client = 1; client <= MaxClients; client++)
	{
		g_hDelayJump[client] = INVALID_HANDLE;
	}
	
	GetCurrentMap(g_currentMap, sizeof(g_currentMap));
	
	H_TimerHUD 		= CreateTimer(0.1, HUDTimer_CSGO, _, TIMER_FLAG_NO_MAPCHANGE|TIMER_REPEAT);
	
	
	RestartMapTimer();
	B_show_hud = true;
}

public OnMapEnd()
{
	if(g_hThink_Map != INVALID_HANDLE)
	{
		CloseHandle(g_hThink_Map);
		g_hThink_Map = INVALID_HANDLE;
	}
}

public OnClientDisconnect(client)
{
	g_iButtonsPressed[client] = 0;
	if (g_hDelayJump[client] != INVALID_HANDLE)
	{
		CloseHandle(g_hDelayJump[client]);
		g_hDelayJump[client] = INVALID_HANDLE;
	}
}

public void:OnClientCookiesCached(client)
{
	new var1;
	if (IsClientInGame(client) && !IsFakeClient(client))
	{
		loadClientCookiesFor(client);
	}
	return void:0;
}

public OnMapVoteStart()
{
	B_show_hud = false;
	return 0;
}

public OnMapVoteEnd(String:map[])
{
	B_show_hud = true;
	return 0;
}

loadClientCookiesFor(client)
{
	if (cookieHudPref)
	{
		decl String:buffer[8];
		GetClientCookie(client, cookieHudPref, buffer, 5);
		if (!StrEqual(buffer, "", true))
		{
			new var1 = hudSettings;
			var1[0][var1][client] = StringToInt(buffer, 10);
		}
		GetClientCookie(client, cookieHudMainPref, buffer, 5);
		if (!StrEqual(buffer, "", true))
		{
			hudSettings[1][client] = StringToInt(buffer, 10);
		}
		GetClientCookie(client, cookieHudMainTimePref, buffer, 5);
		if (!StrEqual(buffer, "", true))
		{
			hudSettings[2][client] = StringToInt(buffer, 10);
		}
		GetClientCookie(client, cookieHudMainJumpsPref, buffer, 5);
		if (!StrEqual(buffer, "", true))
		{
			hudSettings[3][client] = StringToInt(buffer, 10);
		}
		GetClientCookie(client, cookieHudMainSpeedPref, buffer, 5);
		if (!StrEqual(buffer, "", true))
		{
			hudSettings[4][client] = StringToInt(buffer, 10);
		}
		GetClientCookie(client, cookieHudMainSpecPref, buffer, 5);
		if (!StrEqual(buffer, "", true))
		{
			hudSettings[6][client] = StringToInt(buffer, 10);
		}
		GetClientCookie(client, cookieHudMainSpeedMaxPref, buffer, 5);
		if (!StrEqual(buffer, "", true))
		{
			hudSettings[5][client] = StringToInt(buffer, 10);
		}
		GetClientCookie(client, cookieHudMainJumpsAccPref, buffer, 5);
		if (!StrEqual(buffer, "", true))
		{
			hudSettings[7][client] = StringToInt(buffer, 10);
		}
		GetClientCookie(client, cookieHudSidePref, buffer, 5);
		if (!StrEqual(buffer, "", true))
		{
			hudSettings[8][client] = StringToInt(buffer, 10);
		}
		GetClientCookie(client, cookieHudSideMapPref, buffer, 5);
		if (!StrEqual(buffer, "", true))
		{
			hudSettings[9][client] = StringToInt(buffer, 10);
		}
		GetClientCookie(client, cookieHudSideModePref, buffer, 5);
		if (!StrEqual(buffer, "", true))
		{
			hudSettings[10][client] = StringToInt(buffer, 10);
		}
		GetClientCookie(client, cookieHudSideWRPref, buffer, 5);
		if (!StrEqual(buffer, "", true))
		{
			hudSettings[11][client] = StringToInt(buffer, 10);
		}
		GetClientCookie(client, cookieHudSideRankPref, buffer, 5);
		if (!StrEqual(buffer, "", true))
		{
			hudSettings[12][client] = StringToInt(buffer, 10);
		}
		GetClientCookie(client, cookieHudSidePBPref, buffer, 5);
		if (!StrEqual(buffer, "", true))
		{
			hudSettings[13][client] = StringToInt(buffer, 10);
		}
		GetClientCookie(client, cookieHudSideTTWRPref, buffer, 5);
		if (!StrEqual(buffer, "", true))
		{
			hudSettings[14][client] = StringToInt(buffer, 10);
		}
		GetClientCookie(client, cookieHudSideKeysPref, buffer, 5);
		if (!StrEqual(buffer, "", true))
		{
			hudSettings[15][client] = StringToInt(buffer, 10);
		}
		GetClientCookie(client, cookieHudSideSpecPref, buffer, 5);
		if (!StrEqual(buffer, "", true))
		{
			hudSettings[16][client] = StringToInt(buffer, 10);
		}
		GetClientCookie(client, cookieHudSideSteamPref, buffer, 5);
		if (!StrEqual(buffer, "", true))
		{
			hudSettings[17][client] = StringToInt(buffer, 10);
		}
		GetClientCookie(client, cookieHudSideLevelPref, buffer, 5);
		if (!StrEqual(buffer, "", true))
		{
			hudSettings[18][client] = StringToInt(buffer, 10);
		}
		GetClientCookie(client, cookieHudSideTimeleftPref, buffer, 5);
		if (!StrEqual(buffer, "", true))
		{
			hudSettings[19][client] = StringToInt(buffer, 10);
		}
		GetClientCookie(client, cookieHudSidePointsPref, buffer, 5);
		if (!StrEqual(buffer, "", true))
		{
			hudSettings[20][client] = StringToInt(buffer, 10);
		}
		return 0;
	}
	return 0;
}

public MenuHandlerHud(Handle:menu, MenuAction:action, client, itemNum)
{
	if (action == MenuAction:4)
	{
		decl String:info[100];
		decl String:info2[100];
		new bool:found = GetMenuItem(menu, itemNum, info, 100, 0, info2, 100);
		if (found)
		{
			if (StrEqual(info, "master", true))
			{
				new var1 = hudSettings;
				if (var1[0][var1][client])
				{
					new var3 = hudSettings;
					if (var3[0][var3][client] == 1)
					{
						new var4 = hudSettings;
						var4[0][var4][client] = 0;
					}
				}
				else
				{
					new var2 = hudSettings;
					var2[0][var2][client] = 1;
				}
				decl String:buffer[8];
				new var5 = hudSettings;
				IntToString(var5[0][var5][client], buffer, 5);
				SetClientCookie(client, cookieHudPref, buffer);
			}
			if (StrEqual(info, "main", true))
			{
				if (hudSettings[1][client])
				{
					if (hudSettings[1][client] == 1)
					{
						hudSettings[1][client] = 0;
					}
				}
				else
				{
					hudSettings[1][client] = 1;
				}
				decl String:buffer[8];
				IntToString(hudSettings[1][client], buffer, 5);
				SetClientCookie(client, cookieHudMainPref, buffer);
			}
			if (StrEqual(info, "time", true))
			{
				if (hudSettings[2][client])
				{
					if (hudSettings[2][client] == 1)
					{
						hudSettings[2][client] = 0;
					}
				}
				else
				{
					hudSettings[2][client] = 1;
				}
				decl String:buffer[8];
				IntToString(hudSettings[2][client], buffer, 5);
				SetClientCookie(client, cookieHudMainTimePref, buffer);
			}
			if (StrEqual(info, "jumps", true))
			{
				if (hudSettings[3][client])
				{
					if (hudSettings[3][client] == 1)
					{
						hudSettings[3][client] = 0;
					}
				}
				else
				{
					hudSettings[3][client] = 1;
				}
				decl String:buffer[8];
				IntToString(hudSettings[3][client], buffer, 5);
				SetClientCookie(client, cookieHudMainJumpsPref, buffer);
			}
			if (StrEqual(info, "speed", true))
			{
				if (hudSettings[4][client])
				{
					if (hudSettings[4][client] == 1)
					{
						hudSettings[4][client] = 0;
					}
				}
				else
				{
					hudSettings[4][client] = 1;
				}
				decl String:buffer[8];
				IntToString(hudSettings[4][client], buffer, 5);
				SetClientCookie(client, cookieHudMainSpeedPref, buffer);
			}
			if (StrEqual(info, "speedmax", true))
			{
				if (hudSettings[5][client])
				{
					if (hudSettings[5][client] == 1)
					{
						hudSettings[5][client] = 0;
					}
				}
				else
				{
					hudSettings[5][client] = 1;
				}
				decl String:buffer[8];
				IntToString(hudSettings[5][client], buffer, 5);
				SetClientCookie(client, cookieHudMainSpeedMaxPref, buffer);
			}
			if (StrEqual(info, "spectators", true))
			{
				if (hudSettings[6][client])
				{
					if (hudSettings[6][client] == 1)
					{
						hudSettings[6][client] = 0;
					}
				}
				else
				{
					hudSettings[6][client] = 1;
				}
				decl String:buffer[8];
				IntToString(hudSettings[6][client], buffer, 5);
				SetClientCookie(client, cookieHudMainSpecPref, buffer);
			}
			if (StrEqual(info, "jumpacc", true))
			{
				if (hudSettings[7][client])
				{
					if (hudSettings[7][client] == 1)
					{
						hudSettings[7][client] = 0;
					}
				}
				else
				{
					hudSettings[7][client] = 1;
				}
				decl String:buffer[8];
				IntToString(hudSettings[7][client], buffer, 5);
				SetClientCookie(client, cookieHudMainJumpsAccPref, buffer);
			}
			if (StrEqual(info, "side", true))
			{
				if (hudSettings[8][client])
				{
					if (hudSettings[8][client] == 1)
					{
						hudSettings[8][client] = 0;
					}
				}
				else
				{
					hudSettings[8][client] = 1;
				}
				decl String:buffer[8];
				IntToString(hudSettings[8][client], buffer, 5);
				SetClientCookie(client, cookieHudSidePref, buffer);
			}
			if (StrEqual(info, "map", true))
			{
				if (hudSettings[9][client])
				{
					if (hudSettings[9][client] == 1)
					{
						hudSettings[9][client] = 0;
					}
				}
				else
				{
					hudSettings[9][client] = 1;
				}
				decl String:buffer[8];
				IntToString(hudSettings[9][client], buffer, 5);
				SetClientCookie(client, cookieHudSideMapPref, buffer);
			}
			if (StrEqual(info, "mode", true))
			{
				if (hudSettings[10][client])
				{
					if (hudSettings[10][client] == 1)
					{
						hudSettings[10][client] = 0;
					}
				}
				else
				{
					hudSettings[10][client] = 1;
				}
				decl String:buffer[8];
				IntToString(hudSettings[10][client], buffer, 5);
				SetClientCookie(client, cookieHudSideModePref, buffer);
			}
			if (StrEqual(info, "wr", true))
			{
				if (hudSettings[11][client])
				{
					if (hudSettings[11][client] == 1)
					{
						hudSettings[11][client] = 0;
					}
				}
				else
				{
					hudSettings[11][client] = 1;
				}
				decl String:buffer[8];
				IntToString(hudSettings[11][client], buffer, 5);
				SetClientCookie(client, cookieHudSideWRPref, buffer);
			}
			if (StrEqual(info, "level", true))
			{
				if (hudSettings[18][client])
				{
					if (hudSettings[18][client] == 1)
					{
						hudSettings[18][client] = 0;
					}
				}
				else
				{
					hudSettings[18][client] = 1;
				}
				decl String:buffer[8];
				IntToString(hudSettings[18][client], buffer, 5);
				SetClientCookie(client, cookieHudSideLevelPref, buffer);
			}
			if (StrEqual(info, "timeleft", true))
			{
				if (hudSettings[19][client])
				{
					if (hudSettings[19][client] == 1)
					{
						hudSettings[19][client] = 0;
					}
				}
				else
				{
					hudSettings[19][client] = 1;
				}
				decl String:buffer[8];
				IntToString(hudSettings[19][client], buffer, 5);
				SetClientCookie(client, cookieHudSideTimeleftPref, buffer);
			}
			if (StrEqual(info, "rank", true))
			{
				if (hudSettings[12][client])
				{
					if (hudSettings[12][client] == 1)
					{
						hudSettings[12][client] = 0;
					}
				}
				else
				{
					hudSettings[12][client] = 1;
				}
				decl String:buffer[8];
				IntToString(hudSettings[12][client], buffer, 5);
				SetClientCookie(client, cookieHudSideRankPref, buffer);
			}
			if (StrEqual(info, "pb", true))
			{
				if (hudSettings[13][client])
				{
					if (hudSettings[13][client] == 1)
					{
						hudSettings[13][client] = 0;
					}
				}
				else
				{
					hudSettings[13][client] = 1;
				}
				decl String:buffer[8];
				IntToString(hudSettings[13][client], buffer, 5);
				SetClientCookie(client, cookieHudSidePBPref, buffer);
			}
			if (StrEqual(info, "ttwr", true))
			{
				if (hudSettings[14][client])
				{
					if (hudSettings[14][client] == 1)
					{
						hudSettings[14][client] = 0;
					}
				}
				else
				{
					hudSettings[14][client] = 1;
				}
				decl String:buffer[8];
				IntToString(hudSettings[14][client], buffer, 5);
				SetClientCookie(client, cookieHudSideTTWRPref, buffer);
			}
			if (StrEqual(info, "keys", true))
			{
				if (hudSettings[15][client])
				{
					if (hudSettings[15][client] == 1)
					{
						hudSettings[15][client] = 0;
					}
				}
				else
				{
					hudSettings[15][client] = 1;
				}
				decl String:buffer[8];
				IntToString(hudSettings[15][client], buffer, 5);
				SetClientCookie(client, cookieHudSideKeysPref, buffer);
			}
			if (StrEqual(info, "spec", true))
			{
				if (hudSettings[16][client])
				{
					if (hudSettings[16][client] == 1)
					{
						hudSettings[16][client] = 0;
					}
				}
				else
				{
					hudSettings[16][client] = 1;
				}
				decl String:buffer[8];
				IntToString(hudSettings[16][client], buffer, 5);
				SetClientCookie(client, cookieHudSideSpecPref, buffer);
			}
			if (StrEqual(info, "steam", true))
			{
				if (hudSettings[17][client])
				{
					if (hudSettings[17][client] == 1)
					{
						hudSettings[17][client] = 0;
					}
				}
				else
				{
					hudSettings[17][client] = 1;
				}
				decl String:buffer[8];
				IntToString(hudSettings[17][client], buffer, 5);
				SetClientCookie(client, cookieHudSideSteamPref, buffer);
			}
			if (StrEqual(info, "points", true))
			{
				if (hudSettings[20][client])
				{
					if (hudSettings[20][client] == 1)
					{
						hudSettings[20][client] = 0;
					}
				}
				else
				{
					hudSettings[20][client] = 1;
				}
				decl String:buffer[8];
				IntToString(hudSettings[20][client], buffer, 5);
				SetClientCookie(client, cookieHudSidePointsPref, buffer);
			}
		}
		if (IsClientInGame(client))
		{
			ShowHudMenu(client, GetMenuSelectionPosition());
		}
	}
	else
	{
		if (action == MenuAction:16)
		{
			CloseHandle(menu);
		}
	}
	return 0;
}

public Action:MenuHud(client, args)
{
	ShowHudMenu(client, 1);
	return Action:3;
}



ShowHudMenu(client, start_item)
{
	new var1;
	if (g_Settings[153] && g_Settings[154])
	{
		new var2 = hudSettings;
		if (var2[0][var2][client] == 1)
		{
			new var3 = hudSettings;
			var3[0][var3][client] = 0;
			CPrintToChat(client, "%t", "HUD disabled");
		}
		else
		{
			new var4 = hudSettings;
			var4[0][var4][client] = 1;
			CPrintToChat(client, "%t", "HUD enabled");
		}
	}
	else
	{
		if (g_Settings[154])
		{
			new Handle:menu = CreateMenu(MenuHandlerHud, MenuAction:28);
			decl String:buffer[100];
			FormatEx(buffer, 100, "%T", "Custom Hud Menu", client);
			SetMenuTitle(menu, buffer);
			new var5 = hudSettings;
			if (var5[0][var5][client])
			{
				new String:master[256];
				Format(master, 256, "%T", "Disable HUD Master Switch", client);
				AddMenuItem(menu, "master", master, 0);
			}
			else
			{
				new String:master[256];
				Format(master, 256, "%T", "Enable HUD Master Switch", client);
				AddMenuItem(menu, "master", master, 0);
			}
			if (g_Settings[155])
			{
				if (hudSettings[1][client])
				{
					new String:main[256];
					Format(main, 256, "%T", "Disable Center HUD", client);
					AddMenuItem(menu, "main", main, 0);
				}
				new String:main[256];
				Format(main, 256, "%T", "Enable Center HUD", client);
				AddMenuItem(menu, "main", main, 0);
			}
			if (g_Settings[156])
			{
				if (hudSettings[8][client])
				{
					new String:side[256];
					Format(side, 256, "%T", "Disable Side HUD", client);
					AddMenuItem(menu, "side", side, 0);
				}
				new String:side[256];
				Format(side, 256, "%T", "Enable Side HUD", client);
				AddMenuItem(menu, "side", side, 0);
			}
			if (hudSettings[2][client])
			{
				new String:time[256];
				Format(time, 256, "%T", "Disable Time", client);
				AddMenuItem(menu, "time", time, 0);
			}
			else
			{
				new String:time[256];
				Format(time, 256, "%T", "Enable Time", client);
				AddMenuItem(menu, "time", time, 0);
			}
			if (g_Settings[158])
			{
				if (hudSettings[3][client])
				{
					new String:jumps[256];
					Format(jumps, 256, "%T", "Disable Jumps", client);
					AddMenuItem(menu, "jumps", jumps, 0);
				}
				new String:jumps[256];
				Format(jumps, 256, "%T", "Enable Jumps", client);
				AddMenuItem(menu, "jumps", jumps, 0);
			}
			if (g_Settings[160])
			{
				if (hudSettings[4][client])
				{
					new String:speed[256];
					Format(speed, 256, "%T", "Disable Speed", client);
					AddMenuItem(menu, "speed", speed, 0);
				}
				new String:speed[256];
				Format(speed, 256, "%T", "Enable Speed", client);
				AddMenuItem(menu, "speed", speed, 0);
			}
			if (g_Settings[161])
			{
				if (hudSettings[5][client])
				{
					new String:speedmax[256];
					Format(speedmax, 256, "%T", "Disable Max Speed", client);
					AddMenuItem(menu, "speedmax", speedmax, 0);
				}
				new String:speedmax[256];
				Format(speedmax, 256, "%T", "Enable Max Speed", client);
				AddMenuItem(menu, "speedmax", speedmax, 0);
			}
			if (g_Settings[175])
			{
				if (hudSettings[6][client])
				{
					new String:spectators[256];
					Format(spectators, 256, "%T", "Disable Spectators", client);
					AddMenuItem(menu, "spectators", spectators, 0);
				}
				new String:spectators[256];
				Format(spectators, 256, "%T", "Enable Spectators", client);
				AddMenuItem(menu, "spectators", spectators, 0);
			}
			if (g_Settings[159])
			{
				if (hudSettings[7][client])
				{
					new String:jumpacc[256];
					Format(jumpacc, 256, "%T", "Disable Jump Accuracy", client);
					AddMenuItem(menu, "jumpacc", jumpacc, 0);
				}
				new String:jumpacc[256];
				Format(jumpacc, 256, "%T", "Enable Jump Accuracy", client);
				AddMenuItem(menu, "jumpacc", jumpacc, 0);
			}
			if (g_Settings[170])
			{
				if (hudSettings[16][client])
				{
					new String:specs[256];
					Format(specs, 256, "%T", "Disable Spec List[SideHUD]", client);
					AddMenuItem(menu, "spec", specs, 0);
				}
				new String:specs[256];
				Format(specs, 256, "%T", "Enable Spec List[SideHUD]", client);
				AddMenuItem(menu, "spec", specs, 0);
			}
			if (g_Settings[174])
			{
				if (hudSettings[20][client])
				{
					new String:points[256];
					Format(points, 256, "%T", "Disable Points[SideHUD]", client);
					AddMenuItem(menu, "points", points, 0);
				}
				new String:points[256];
				Format(points, 256, "%T", "Enable Points[SideHUD]", client);
				AddMenuItem(menu, "points", points, 0);
			}
			if (g_Settings[163])
			{
				if (hudSettings[9][client])
				{
					new String:map[256];
					Format(map, 256, "%T", "Disable Map Display [SideHUD]", client);
					AddMenuItem(menu, "map", map, 0);
				}
				new String:map[256];
				Format(map, 256, "%T", "Enable Map Display [SideHUD]", client);
				AddMenuItem(menu, "map", map, 0);
			}
			if (g_Settings[164])
			{
				if (hudSettings[10][client])
				{
					new String:mode[256];
					Format(mode, 256, "%T", "Disable Style Display [SideHUD]", client);
					AddMenuItem(menu, "mode", mode, 0);
				}
				new String:mode[256];
				Format(mode, 256, "%T", "Enable Style Display [SideHUD]", client);
				AddMenuItem(menu, "mode", mode, 0);
			}
			if (g_Settings[165])
			{
				if (hudSettings[11][client])
				{
					new String:wr[256];
					Format(wr, 256, "%T", "Disable WR Display [SideHUD]", client);
					AddMenuItem(menu, "wr", wr, 0);
				}
				new String:wr[256];
				Format(wr, 256, "%T", "Enable WR Display [SideHUD]", client);
				AddMenuItem(menu, "wr", wr, 0);
			}
			if (g_Settings[173])
			{
				if (hudSettings[12][client])
				{
					new String:rank[256];
					Format(rank, 256, "%T", "Disable Rank Display [SideHUD]", client);
					AddMenuItem(menu, "rank", rank, 0);
				}
				new String:rank[256];
				Format(rank, 256, "%T", "Enable Rank Display [SideHUD]", client);
				AddMenuItem(menu, "rank", rank, 0);
			}
			if (g_Settings[172])
			{
				if (hudSettings[18][client])
				{
					new String:level[256];
					Format(level, 256, "%T", "Disable Level Display [SideHUD]", client);
					AddMenuItem(menu, "level", level, 0);
				}
				new String:level[256];
				Format(level, 256, "%T", "Enable Level Display [SideHUD]", client);
				AddMenuItem(menu, "level", level, 0);
			}
			if (g_Settings[167])
			{
				if (hudSettings[13][client])
				{
					new String:pb[256];
					Format(pb, 256, "%T", "Disable Personal Best [SideHUD]", client);
					AddMenuItem(menu, "pb", pb, 0);
				}
				new String:pb[256];
				Format(pb, 256, "%T", "Enable Personal Best [SideHUD]", client);
				AddMenuItem(menu, "pb", pb, 0);
			}
			if (g_Settings[168])
			{
				if (hudSettings[14][client])
				{
					new String:ttwr[256];
					Format(ttwr, 256, "%T", "Disable TTWR Display [SideHUD]", client);
					AddMenuItem(menu, "ttwr", ttwr, 0);
				}
				new String:ttwr[256];
				Format(ttwr, 256, "%T", "Enable TTWR Display [SideHUD]", client);
				AddMenuItem(menu, "ttwr", ttwr, 0);
			}
			if (g_Settings[157])
			{
				if (hudSettings[19][client])
				{
					new String:timeleft[256];
					Format(timeleft, 256, "%T", "Disable Timeleft Display [SideHUD]", client);
					AddMenuItem(menu, "timeleft", timeleft, 0);
				}
				new String:timeleft[256];
				Format(timeleft, 256, "%T", "Enable Timeleft Display [SideHUD]", client);
				AddMenuItem(menu, "timeleft", timeleft, 0);
			}
			if (g_Settings[169])
			{
				if (hudSettings[15][client])
				{
					new String:keys[256];
					Format(keys, 256, "%T", "Disable Keys Display [SideHUD/Spec only]", client);
					AddMenuItem(menu, "keys", keys, 0);
				}
				new String:keys[256];
				Format(keys, 256, "%T", "Enable Keys Display [SideHUD/Spec only]", client);
				AddMenuItem(menu, "keys", keys, 0);
			}
			if (g_Settings[171])
			{
				if (hudSettings[17][client])
				{
					new String:steam[256];
					Format(steam, 256, "%T", "Disable Steam [SideHUD/Spec only]", client);
					AddMenuItem(menu, "steam", steam, 0);
				}
				new String:steam[256];
				Format(steam, 256, "%T", "Enable Steam [SideHUD/Spec only]", client);
				AddMenuItem(menu, "steam", steam, 0);
			}
			SetMenuExitButton(menu, true);
			DisplayMenuAtItem(menu, client, start_item, 0);
		}
	}
	return 0;
}

public Action:Cmd_HideMyAss(client, args)
{
	new var1;
	if (IsClientConnected(client) && IsClientInGame(client) && Client_IsAdmin(client))
	{
		if (hidemyass[client])
		{
			hidemyass[client] = 0;
			CPrintToChat(client, "%t", "Hide My Ass: Disabled");
		}
		hidemyass[client] = 1;
		CPrintToChat(client, "%t", "Hide My Ass: Enabled");
	}
	return Action:3;
}

public void:OnConfigsExecuted()
{
	if (g_cvarTimeLimit)
	{
		HookConVarChange(g_cvarTimeLimit, ConVarChange_TimeLimit);
	}
	if (!H_TimerHUD)
	{
		CreateTimer(0.1, HUDTimer_CSGO, any:0, 3);
	}
	return void:0;
}

public ConVarChange_TimeLimit(Handle:cvar, String:oldVal[], String:newVal[])
{
	RestartMapTimer();
	return 0;
}

RestartMapTimer()
{
	if (g_hThink_Map)
	{
		CloseHandle(g_hThink_Map);
		g_hThink_Map = MissingTAG:0;
	}
	new bool:gotTimeLeft = GetMapTimeLeft(g_iMap_TimeLeft);
	new var1;
	if (gotTimeLeft && g_iMap_TimeLeft > 0)
	{
		g_hThink_Map = CreateTimer(1.0, Timer_Think_Map, any:0, 1);
	}
	return 0;
}

public Action:Timer_Think_Map(Handle:timer)
{
	g_iMap_TimeLeft -= 1;
	return Action:0;
}

public void:OnClientPutInServer(client)
{
	if (!IsFakeClient(client))
	{
		new var2 = hudSettings;
		var2[0][var2][client] = 1;
		hudSettings[1][client] = 1;
		hudSettings[2][client] = 1;
		hudSettings[3][client] = 1;
		hudSettings[4][client] = 1;
		hudSettings[5][client] = 1;
		hudSettings[6][client] = 1;
		hudSettings[7][client] = 1;
		hudSettings[8][client] = 1;
		hudSettings[9][client] = 1;
		hudSettings[10][client] = 1;
		hudSettings[11][client] = 1;
		hudSettings[18][client] = 1;
		hudSettings[12][client] = 1;
		hudSettings[13][client] = 1;
		hudSettings[14][client] = 1;
		hudSettings[15][client] = 1;
		hudSettings[16][client] = 1;
		hudSettings[17][client] = 1;
		hudSettings[20][client] = 1;
		hudSettings[19][client] = 1;
		hudSettings[21][client] = 0;
		if (AreClientCookiesCached(client))
		{
			loadClientCookiesFor(client);
		}
	}
	new var1;
	if (g_hThink_Map && IsServerProcessing())
	{
		RestartMapTimer();
	}
	if (!H_TimerHUD)
	{
		CreateTimer(0.1, HUDTimer_CSGO, any:0, 3);
	}
	return void:0;
}

public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon)
{
	g_iButtonsPressed[client] = buttons;
	if (GetClientButtons(client) & 2)
	{
		B_IsJump[client] = 1;
	}
	if (!GetEntityFlags(client) & 1)
	{
		if (!Timer_GetPauseStatus(client))
		{
			new var1;
			if (IsPlayerAlive(client) && IsValidClient(client))
			{
				fspeed[client] = GetSpeed(client);
				new iGroundEntity = GetEntPropEnt(client, PropType:0, "m_hGroundEntity", 0);
				new Float:fAngle = angles[1] - gF_AngleCache[client];
				while (fAngle > 180.0)
				{
					fAngle -= 360.0;
				}
				while (fAngle < -180.0)
				{
					fAngle += 360.0;
				}
				new var2;
				if (iGroundEntity == -1 && !GetEntityFlags(client) & 512 && 0.0 != fAngle)
				{
					new Float:fAbsVelocity[3] = 0.0;
					GetEntPropVector(client, PropType:1, "m_vecAbsVelocity", fAbsVelocity, 0);
					if (SquareRoot(Pow(fAbsVelocity[0], 2.0) + Pow(fAbsVelocity[1], 2.0)) > 0.0)
					{
						new Float:fTempAngle = angles[1];
						new Float:fAngles[3] = 0.0;
						GetVectorAngles(fAbsVelocity, fAngles);
						if (fTempAngle < 0.0)
						{
							fTempAngle += 360.0;
						}
						new Float:fDirectionAngle = fTempAngle - fAngles[1];
						if (fDirectionAngle < 0.0)
						{
							fDirectionAngle = -fDirectionAngle;
						}
						totalMeasures[client]++;
						new var3;
						if (fDirectionAngle < 22.5 || fDirectionAngle > 337.5)
						{
							new var4;
							if ((fAngle > 0.0 && vel[1] < 0.0) || (fAngle < 0.0 && vel[1] > 0.0))
							{
								goodGains[client]++;
							}
						}
					}
				}
				gF_AngleCache[client] = angles[1];
				prevSpeed[client] = fspeed[client];
			}
		}
	}
	return Action:0;
}

public Float:getSync(client)
{
	return goodGains[client] / float(totalMeasures[client]) * 100;
}

public UpdateSpeed(client, Float:tSpeed)
{
	if (prevSSpeed[client] < tSpeed)
	{
		speedChange[client] = tSpeed - prevSSpeed[client] / prevSSpeed[client] * 100.0;
	}
	else
	{
		speedChange[client] = tSpeed - prevSSpeed[client] / prevSSpeed[client] * 100.0;
	}
	return 0;
}

public ResetSync(client)
{
	totalMeasures[client] = 0;
	goodGains[client] = 0;
	prevSSpeed[client] = 0;
	countSixth[client] = 0;
	speedSixth[client] = 0;
	speedChange[client] = 0;
	return 0;
}

public Float:GetSpeed(client)
{
	decl Float:fVelocity[3];
	GetEntPropVector(client, PropType:1, "m_vecVelocity", fVelocity, 0);
	new Float:speed = SquareRoot(Pow(fVelocity[0], 2.0) + Pow(fVelocity[1], 2.0));
	return speed;
}

public Action:Event_PlayerJump(Handle:event, String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid", 0));
	g_iJumps[client]++;
	g_hDelayJump[client] = CreateTimer(0.3, Timer_DelayJumpHud, client, 2);
	new var1;
	if (IsPlayerAlive(client) && IsValidClient(client))
	{
		sspeed[client] = GetSpeed(client);
		UpdateSpeed(client, sspeed[client]);
		prevSSpeed[client] = fspeed[client];
		if (hudSettings[21][client] == 1)
		{
			new var2 = countSixth[client];
			var2 = var2[1.0];
			if (6.0 == countSixth[client])
			{
				CPrintToChat(client, "[{PURPLE}TIMER{NORMAL}] 6th jump: You gained %.2f speed.", sspeed[client]);
			}
		}
	}
	return Action:0;
}

public Action:Timer_DelayJumpHud(Handle:timer, any:client)
{
	g_hDelayJump[client] = 0;
	return Action:4;
}

public Action:Event_Reset(Handle:event, String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid", 0));
	g_iJumps[client] = 0;
	if (g_hDelayJump[client])
	{
		CloseHandle(g_hDelayJump[client]);
		g_hDelayJump[client] = 0;
	}
	return Action:0;
}

public Action:HUDTimer_CSGO(Handle:timer)
{
	new client = 1;
	while (client <= MaxClients)
	{
		spec[client] = 0;
		client++;
	}
	new client = 1;
	while (client <= MaxClients)
	{
		if (IsClientInGame(client))
		{
			if (!hidemyass[client])
			{
				new var1;
				if (!IsPlayerAlive(client) || IsClientObserver(client))
				{
					new iObserverMode = GetEntProp(client, PropType:0, "m_iObserverMode", 4, 0);
					new var2;
					if (iObserverMode == 4 || iObserverMode == 5)
					{
						new clienttoshow = GetEntPropEnt(client, PropType:0, "m_hObserverTarget", 0);
						new var3;
						if (clienttoshow > 0 && clienttoshow < MaxClients + 1)
						{
							spec[clienttoshow] = 1;
						}
					}
				}
			}
		}
		client++;
	}
	new client = 1;
	while (client <= MaxClients)
	{
		if (IsClientInGame(client))
		{
			UpdateHUD_CSGO(client);
		}
		client++;
	}
	return Action:0;
}

UpdateHUD_CSGO(client)
{
	if (!IsClientInGame(client))
	{
		return 0;
	}
	new var39 = hudSettings;
	if (!var39[0][var39][client])
	{
		return 0;
	}
	if (!g_Settings[154])
	{
		return 0;
	}
	new iClientToShow;
	new iObserverMode;
	iClientToShow = client;
	new var1;
	if (!IsPlayerAlive(client) || IsClientObserver(client))
	{
		iObserverMode = GetEntProp(client, PropType:0, "m_iObserverMode", 4, 0);
		new var2;
		if (iObserverMode == 4 || iObserverMode == 5)
		{
			iClientToShow = GetEntPropEnt(client, PropType:0, "m_hObserverTarget", 0);
			new var3;
			if (iClientToShow <= 0 || iClientToShow > MaxClients || IsFakeClient(iClientToShow))
			{
				return 0;
			}
		}
		return 0;
	}
	new var4;
	if (g_timerLjStats && IsClientInLJMode(iClientToShow))
	{
		return 0;
	}
	new String:centerText[512];
	new String:centerStartText[512];
	if (IsFakeClient(iClientToShow))
	{
		return 0;
	}
	decl String:auth[64];
	GetClientAuthId(iClientToShow, AuthIdType:1, auth, 64, true);
	decl String:buffer[32];
	decl String:bestbuffer[32];
	new bool:enabled;
	new Float:bestTime = 0.0;
	new bestJumps;
	new jumps;
	new fpsmax;
	new bool:bonus;
	new Float:time = 0.0;
	new RecordId;
	new Float:RecordTime = 0.0;
	new RankTotal;
	if (g_timerWorldRecord)
	{
		Timer_GetClientTimer(iClientToShow, enabled, time, jumps, fpsmax);
	}
	new style;
	if (g_timerPhysics)
	{
		style = Timer_GetStyle(iClientToShow);
	}
	new ranked;
	if (g_timerPhysics)
	{
		ranked = Timer_IsStyleRanked(style);
	}
	new currentLevel;
	if (g_timerMapzones)
	{
		currentLevel = Timer_GetClientLevelID(iClientToShow);
	}
	if (currentLevel < 1)
	{
		currentLevel = 1;
	}
	if (currentLevel > 1000)
	{
		bonus = true;
	}
	if (g_timerPhysics)
	{
		Timer_GetStyleRecordWRStats(style, bonus, RecordId, RecordTime, RankTotal);
		Timer_SecondsToTime(time, buffer, 32, 0);
	}
	new Float:maxspeed = 0.0;
	new Float:currentspeed = 0.0;
	new Float:avgspeed = 0.0;
	if (g_timerPhysics)
	{
		Timer_GetMaxSpeed(iClientToShow, maxspeed);
		Timer_GetCurrentSpeed(iClientToShow, currentspeed);
		Timer_GetAvgSpeed(iClientToShow, avgspeed);
	}
	new Float:accuracy = 0.0;
	if (g_timerPhysics)
	{
		Timer_GetJumpAccuracy(iClientToShow, accuracy);
	}
	if (accuracy > 100.0)
	{
		accuracy = 100.0;
	}
	else
	{
		if (accuracy < 0.0)
		{
			accuracy = 0.0;
		}
	}
	if (ranked)
	{
		if (g_timerWorldRecord)
		{
			Timer_GetBestRound(iClientToShow, style, bonus, bestTime, bestJumps);
		}
		Timer_SecondsToTime(bestTime, bestbuffer, 32, 2);
	}
	new points;
	if (g_timerRankings)
	{
		points = Timer_GetPoints(iClientToShow);
	}
	new points100 = points;
	if (0 < g_Settings[146])
	{
		points100 = RoundToFloor(1065353216 * points / g_Settings[146]);
	}
	if (iClientToShow == client)
	{
		new var5;
		if (points > 0 && g_Settings[146] > 0)
		{
			CS_SetMVPCount(client, points100);
		}
	}
	new rank;
	new var6;
	if (ranked && g_timerWorldRecord)
	{
		rank = Timer_GetStyleRank(iClientToShow, bonus, style);
	}
	new prank;
	if (g_timerRankings)
	{
		prank = Timer_GetPointRank(iClientToShow);
	}
	new var7;
	if (prank > 2000 || prank < 1)
	{
		prank = 2000;
	}
	new nprank = prank * -1;
	new String:sRankTotal[32];
	Format(sRankTotal, 32, "%d", RankTotal);
	if (iClientToShow == client)
	{
		new var8;
		if (g_Settings[146] > 0 && points100 > 0)
		{
			CS_SetMVPCount(iClientToShow, points100);
		}
		if (g_Settings[147])
		{
			SetEntProp(client, PropType:1, "m_iFrags", nprank, 4, 0);
		}
		if (g_Settings[148])
		{
			Client_SetDeaths(client, rank);
		}
		new var9;
		if (g_Settings[149] && !IsFakeClient(client))
		{
			decl String:tagbuffer[32];
			if (g_Settings[151])
			{
				if (enabled)
				{
					FormatEx(tagbuffer, 32, "%s", buffer);
				}
				if (ranked)
				{
					FormatEx(tagbuffer, 32, "%s", bestbuffer);
				}
			}
			new var10;
			if (g_Settings[151] && g_Settings[39] && g_Settings[150])
			{
				Format(tagbuffer, 32, " %s", tagbuffer);
			}
			new var11;
			if (g_Settings[39] && g_Settings[150])
			{
				new var12;
				if (!enabled && !ranked)
				{
					Format(tagbuffer, 32, "%s%s", g_Physics[style][36], tagbuffer);
				}
				Format(tagbuffer, 32, "%s%s", g_Physics[style][68], tagbuffer);
			}
			if (Client_HasAdminFlags(client, 8))
			{
				Format(tagbuffer, 32, "%s", "Admin");
			}
			else
			{
				if (Client_HasAdminFlags(client, 4))
				{
					Format(tagbuffer, 32, "%s", "Moderator");
				}
				if (Client_HasAdminFlags(client, 1048576))
				{
					Format(tagbuffer, 32, "%s", "Trusted");
				}
			}
			CS_SetClientClanTag(client, tagbuffer);
		}
	}
	new stagecount;
	if (g_timerMapzones)
	{
		if (bonus)
		{
			stagecount = Timer_GetMapzoneCount(MapZoneType:42) + Timer_GetMapzoneCount(MapZoneType:9) + 1;
		}
		stagecount = Timer_GetMapzoneCount(MapZoneType:41) + Timer_GetMapzoneCount(MapZoneType:6) + 1;
	}
	if (currentLevel > 1000)
	{
		currentLevel += -1000;
	}
	if (currentLevel == 999)
	{
		currentLevel = stagecount;
	}
	if (!enabled)
	{
		jumps = 0;
	}
	decl String:timeString[64];
	Timer_SecondsToTime(time, timeString, 64, 1);
	if (StrEqual(timeString, "00:-0.0", true))
	{
		Format(timeString, 64, "00:00.0");
	}
	new var13;
	if (hudSettings[18][client] && g_Settings[39] && g_Settings[176])
	{
		if (stagecount <= 1)
		{
			Format(centerText, 512, "%T", "Stage: Linear", client);
		}
		else
		{
			Format(centerText, 512, "%T", "Stage", client, currentLevel, stagecount);
		}
		if (hudSettings[2][client])
		{
			Format(centerText, 512, "%s 	", centerText);
		}
	}
	if (hudSettings[2][client])
	{
		if (Timer_GetPauseStatus(iClientToShow))
		{
			Format(centerText, 512, "%T", "Time: Pause", client, centerText, timeString);
		}
		else
		{
			if (enabled)
			{
				new var14;
				if (0.0 == RecordTime || RecordTime > time)
				{
					Format(centerText, 512, "%T", "Time: RecordTime", client, centerText, timeString);
				}
				else
				{
					Format(centerText, 512, "%T", "Time: RecordTime2", client, centerText, timeString);
				}
			}
			Format(centerText, 512, "%T", "Time: Stop", client, centerText);
		}
		new var16;
		if (hudSettings[3][client] && g_Settings[158] && (!g_Settings[175] || !g_Settings[176]))
		{
			Format(centerText, 512, "%s 		", centerText);
		}
	}
	new var17;
	if ((hudSettings[3][client] && g_Settings[158]) && (hudSettings[7][client] && g_Settings[159]))
	{
		Format(centerText, 512, "%s%T: %d [%.2f %%]", centerText, "Jumps", client, jumps, accuracy);
	}
	else
	{
		new var21;
		if (hudSettings[3][client] && g_Settings[158] && (!g_Settings[175] || !g_Settings[176]))
		{
			Format(centerText, 512, "%s%T: %d", centerText, "Jumps", client, jumps);
		}
	}
	new var22;
	if (hudSettings[2][client] || hudSettings[18][client] || hudSettings[3][client])
	{
		Format(centerText, 512, "%s\n", centerText);
	}
	new var23;
	if (ranked && g_timerWorldRecord)
	{
		if (hudSettings[12][client])
		{
			Format(centerText, 512, "%T", "Style", client, centerText, g_Physics[style][4]);
			if (hudSettings[13][client])
			{
				Format(centerText, 512, "%s    	", centerText);
			}
		}
		if (hudSettings[13][client])
		{
			Timer_GetBestRound(iClientToShow, style, bonus, bestTime, bestJumps);
			Timer_SecondsToTime(bestTime, bestbuffer, 32, 2);
			Format(centerText, 512, "%T", "Record", client, centerText, bestbuffer);
		}
		new var24;
		if (hudSettings[13][client] || hudSettings[12][client])
		{
			Format(centerText, 512, "%s\n", centerText);
		}
	}
	else
	{
		Format(centerText, 512, "%T", "Unranked", client, centerText);
	}
	new var25;
	if (hudSettings[10][client] && g_Settings[39])
	{
		Format(centerText, 512, "%s%s %.1f%s", centerText, "Gain:", speedChange[iClientToShow], "PCT");
		if (hudSettings[4][client])
		{
			Format(centerText, 512, "%s      	", centerText);
		}
	}
	else
	{
		new var26;
		if (hudSettings[18][client] && !g_Settings[39])
		{
			if (stagecount <= 1)
			{
				Format(centerText, 512, "%T", "Stage: Linear2", client, centerText);
			}
			else
			{
				Format(centerText, 512, "%T", "Stage2", client, centerText, currentLevel, stagecount);
			}
			if (hudSettings[4][client])
			{
				Format(centerText, 512, "%s 	", centerText);
			}
		}
	}
	if (hudSettings[4][client])
	{
		Format(centerText, 512, "%T", "Speed", client, centerText, currentspeed);
	}
	new var27;
	if (hudSettings[3][client] && g_Settings[158] && g_Settings[175] && g_Settings[176])
	{
		Format(centerText, 512, "%s\n%T: %d", centerText, "Jumps", client, jumps);
		if (hudSettings[6][client])
		{
			Format(centerText, 512, "%s 	", centerText);
		}
	}
	new String:keys[256];
	new var28;
	if (g_Settings[169] && hudSettings[15][client])
	{
		new String:S_IN_FORWARD[16];
		new String:S_IN_BACK[16];
		new String:S_IN_MOVERIGHT[16];
		new String:S_IN_MOVELEFT[16];
		new String:S_IN_JUMP[16];
		new String:S_IN_DUCK[16];
		if (GetClientButtons(iClientToShow) & 8)
		{
			strcopy(S_IN_FORWARD, 16, "↥");
		}
		else
		{
			strcopy(S_IN_FORWARD, 16, "_");
		}
		if (GetClientButtons(iClientToShow) & 16)
		{
			strcopy(S_IN_BACK, 16, "↧");
		}
		else
		{
			strcopy(S_IN_BACK, 16, "_");
		}
		if (GetClientButtons(iClientToShow) & 1024)
		{
			strcopy(S_IN_MOVERIGHT, 16, "↦");
		}
		else
		{
			strcopy(S_IN_MOVERIGHT, 16, "_");
		}
		if (GetClientButtons(iClientToShow) & 512)
		{
			strcopy(S_IN_MOVELEFT, 16, "↤");
		}
		else
		{
			strcopy(S_IN_MOVELEFT, 16, "_");
		}
		new var29;
		if (GetClientButtons(iClientToShow) & 2 || B_IsJump[iClientToShow])
		{
			strcopy(S_IN_JUMP, 16, "⇗");
		}
		else
		{
			strcopy(S_IN_JUMP, 16, "_");
		}
		B_IsJump[iClientToShow] = 0;
		if (GetClientButtons(iClientToShow) & 4)
		{
			strcopy(S_IN_DUCK, 16, "⇘");
		}
		else
		{
			strcopy(S_IN_DUCK, 16, "_");
		}
		Format(keys, 256, "%s%s%s%s | %s%s", S_IN_MOVELEFT, S_IN_FORWARD, S_IN_MOVERIGHT, S_IN_BACK, S_IN_JUMP, S_IN_DUCK);
	}
	new String:RecordTimeString[32];
	new String:DiffTimeString[32];
	new bool:negate;
	if (Timer_IsStyleRanked(style))
	{
		Timer_SecondsToTime(RecordTime, RecordTimeString, 32, 2);
		if (time - RecordTime >= 0)
		{
			Timer_SecondsToTime(time - RecordTime, DiffTimeString, 32, 2);
		}
		else
		{
			negate = true;
			Timer_SecondsToTime(RecordTime - time, DiffTimeString, 32, 2);
		}
		if (StrEqual(RecordTimeString, "00:-0.00", true))
		{
			FormatEx(RecordTimeString, 32, "00:00.00");
		}
		if (StrEqual(RecordTimeString, "00:00.-0", true))
		{
			FormatEx(RecordTimeString, 32, "00:00.00");
		}
	}
	new var30;
	if (g_Settings[155] && hudSettings[1][client])
	{
		if (Timer_GetPauseStatus(iClientToShow))
		{
			Format(centerStartText, 512, "%s", "	      <font color='#F553F5'>Mythical Gaming</font>");
			Format(centerStartText, 512, "%s\n	   %s", centerStartText, "<font color='#FF8A00'>Your time is paused</font>");
			Format(centerStartText, 512, "%s\n%s %s", centerStartText, "PB:", bestbuffer);
			Format(centerStartText, 512, "%s		%s %s", centerStartText, "WR:", RecordTimeString);
			Format(centerStartText, 512, "%s\n%s %5.2f", centerStartText, "Speed: ", currentspeed);
			Format(centerStartText, 512, "<font size='16'>%s</font>", centerStartText);
			PrintHintText(client, centerStartText);
		}
		if (enabled)
		{
			Format(centerText, 512, "%s\n", centerText);
			if (getSync(iClientToShow) > 85)
			{
				Format(centerText, 512, "%s%s %.1f%s", centerText, "Sync:", getSync(iClientToShow), "PCT");
			}
			else
			{
				if (getSync(iClientToShow) > 65)
				{
					Format(centerText, 512, "%s%s %.1f%s", centerText, "Sync:", getSync(iClientToShow), "PCT");
				}
				Format(centerText, 512, "%s%s %.1f%s", centerText, "Sync:", getSync(iClientToShow), "PCT");
			}
			Format(centerText, 512, "%s    	%s", centerText, keys);
			Format(centerText, 512, "<font size='16'>%s</font>", centerText);
			ReplaceString(centerText, 512, "PCT", "%%", true);
			PrintHintText(client, centerText);
		}
		ResetSync(client);
		Format(centerStartText, 512, "%s", "	      <font color='#F553F5'>Mythical Gaming</font>");
		Format(centerStartText, 512, "%s\n%s %s", centerStartText, "PB:", bestbuffer);
		Format(centerStartText, 512, "%s		%s %s\n", centerStartText, "WR:", RecordTimeString);
		if (rank < 1)
		{
			Format(centerStartText, 512, "%T", "Rank", client, centerStartText, sRankTotal);
		}
		else
		{
			Format(centerStartText, 512, "%T", "Rank2", client, centerStartText, rank, sRankTotal);
		}
		Format(centerStartText, 512, "%s			%s %5.2f", centerStartText, "Speed:", currentspeed);
		Format(centerStartText, 512, "<font size='16'>%s</font>", centerStartText);
		PrintHintText(client, centerStartText);
	}
	new speccount;
	new String:speclist[1024];
	new var31;
	if (g_Settings[170] && hudSettings[16][client])
	{
		new j = 1;
		while (j <= MaxClients)
		{
			new var32;
			if (!IsClientInGame(j) || !IsClientObserver(j))
			{
			}
			else
			{
				if (!(IsClientSourceTV(j)))
				{
					new iSpecMode = GetEntProp(j, PropType:0, "m_iObserverMode", 4, 0);
					new var33;
					if (!(iSpecMode != 4 && iSpecMode != 5))
					{
						new iTarget = GetEntPropEnt(j, PropType:0, "m_hObserverTarget", 0);
						new var34;
						if (iClientToShow == iTarget && !hidemyass[j])
						{
							speccount++;
							Format(speclist, 1024, "%s %N\n", speclist, j);
						}
					}
				}
			}
			j++;
		}
	}
	new var35;
	if (g_Settings[156] && hudSettings[8][client] && B_show_hud)
	{
		new var36;
		if (iObserverMode == 4 || iObserverMode == 5)
		{
			new var37;
			if (iClientToShow > 0 && IsClientInGame(iClientToShow) && !IsFakeClient(iClientToShow))
			{
				BuildSideHud(client, iClientToShow, points, g_currentMap, style, g_Physics[style][4], time, RecordTime, rank, sRankTotal, currentLevel, stagecount, bestbuffer, g_iMap_TimeLeft, auth, speccount, speclist, keys);
			}
		}
		new var38;
		if (iClientToShow == client && speccount > 0)
		{
			new MenuSource:menu = GetClientMenu(client, Handle:0);
			if (menu)
			{
			}
			else
			{
				BuildSideHud(client, iClientToShow, points, g_currentMap, style, g_Physics[style][4], time, RecordTime, rank, sRankTotal, currentLevel, stagecount, bestbuffer, g_iMap_TimeLeft, auth, speccount, speclist, keys);
			}
		}
	}
	return 0;
}

void:BuildSideHud(client, target, points, String:map[], style, String:stylename[], Float:time, Float:wrtime, rank, String:ranktotal[], currentlevel, stagecount, String:pb[], timeleft, String:auth[], speccount, String:speclist[], String:keys[])
{
	panel[client] = CreatePanel(Handle:0);
	new String:title[256];
	if (target != client)
	{
		new var1;
		if (g_Settings[171] && hudSettings[17][client])
		{
			Format(title, 256, "%N [%s]", target, auth);
		}
		else
		{
			Format(title, 256, "%N", target);
		}
	}
	else
	{
		new var2;
		if (target == client && speccount > 0)
		{
			Format(title, 256, "");
		}
	}
	SetPanelTitle(panel[client], title, false);
	if (target != client)
	{
		new String:RecordTimeString[32];
		new String:DiffTimeString[32];
		new bool:negate;
		if (Timer_IsStyleRanked(style))
		{
			Timer_SecondsToTime(wrtime, RecordTimeString, 32, 2);
			if (time - wrtime >= 0)
			{
				Timer_SecondsToTime(time - wrtime, DiffTimeString, 32, 2);
			}
			else
			{
				negate = true;
				Timer_SecondsToTime(wrtime - time, DiffTimeString, 32, 2);
			}
			if (StrEqual(RecordTimeString, "00:-0.00", true))
			{
				FormatEx(RecordTimeString, 32, "00:00.00");
			}
			if (StrEqual(RecordTimeString, "00:00.-0", true))
			{
				FormatEx(RecordTimeString, 32, "00:00.00");
			}
		}
		new String:header[256];
		Format(header, 256, "%T", "HeaderPlayer", client);
		DrawPanelItem(panel[client], header, 0);
		new var3;
		if (g_Settings[173] && hudSettings[12][client])
		{
			new String:item[256];
			Format(item, 256, "%T", "Rankside", client, rank, ranktotal);
			DrawPanelItem(panel[client], item, 2);
		}
		new var4;
		if (g_Settings[174] && hudSettings[20][client])
		{
			new String:item[256];
			Format(item, 256, "%T", "Pointsside", client, points);
			DrawPanelItem(panel[client], item, 2);
		}
		new var5;
		if (g_Settings[164] && hudSettings[10][client])
		{
			new String:item[256];
			Format(item, 256, "%T", "Styleside", client, stylename);
			DrawPanelItem(panel[client], item, 2);
		}
		new String:header2[256];
		Format(header2, 256, "%T", "HeaderTime", client);
		DrawPanelItem(panel[client], header2, 0);
		new var6;
		if (g_Settings[165] && hudSettings[11][client])
		{
			new String:item[256];
			Format(item, 256, "%T", "WRside", client, RecordTimeString);
			DrawPanelItem(panel[client], item, 2);
		}
		new var7;
		if (g_Settings[167] && hudSettings[13][client])
		{
			new String:item[256];
			Format(item, 256, "%T", "PBside", client, pb);
			DrawPanelItem(panel[client], item, 2);
		}
		new var8;
		if (g_Settings[168] && hudSettings[14][client])
		{
			new String:item[256];
			new var9;
			if (wrtime > 0.0 && time > 0.0)
			{
				if (!negate)
				{
					Format(item, 256, "%T", "Timetowrside", client, DiffTimeString);
				}
				Format(item, 256, "%T", "Timetowrside2", client, DiffTimeString);
			}
			DrawPanelItem(panel[client], item, 2);
		}
		new String:header3[256];
		Format(header3, 256, "%T", "HeaderMap", client);
		DrawPanelItem(panel[client], header3, 0);
		new var10;
		if (g_Settings[163] && hudSettings[9][client])
		{
			new String:item[256];
			Format(item, 256, "%T", "Mapside", client, map);
			DrawPanelItem(panel[client], item, 2);
		}
		new var11;
		if (g_Settings[172] && hudSettings[18][client])
		{
			new String:item[256];
			if (stagecount <= 1)
			{
				Format(item, 256, "%T", "Stageside", client);
			}
			else
			{
				Format(item, 256, "%T", "Stageside2", client, currentlevel, stagecount);
			}
			DrawPanelItem(panel[client], item, 2);
		}
		new var12;
		if (g_Settings[157] && hudSettings[19][client])
		{
			new String:item[256];
			if (0 < timeleft)
			{
				Format(item, 256, "%T", "Timeleftside", client, timeleft / 60, timeleft % 60);
			}
			else
			{
				Format(item, 256, "%T", "Timeleftside", client, RoundToFloor(-1082130432 * timeleft) / 60, RoundToFloor(-1082130432 * timeleft) % 60);
			}
			DrawPanelItem(panel[client], item, 2);
		}
		new var13;
		if (g_Settings[169] && hudSettings[15][client])
		{
			new String:header4[256];
			Format(header4, 256, "%T", "HeaderKeys", client);
			DrawPanelItem(panel[client], header4, 0);
			DrawPanelItem(panel[client], keys, 2);
		}
	}
	new var14;
	if (g_Settings[170] && hudSettings[16][client])
	{
		if (0 < speccount)
		{
			new String:header5[256];
			Format(header5, 256, "%T", "HeaderSpec", client);
			DrawPanelItem(panel[client], header5, 0);
			DrawPanelItem(panel[client], speclist, 2);
		}
	}
	DrawPanelItem(panel[client], "", 8);
	SendPanelToClient(panel[client], client, MenuSideHudAction, 1);
	CloseHandle(panel[client]);
	return void:0;
}

public MenuSideHudAction(Menu:menu, MenuAction:action, param1, param2)
{
	switch (action)
	{
		case 8:
		{
			CloseHandle(menu);
		}
		case 16:
		{
			CloseHandle(menu);
		}
		default:
		{
		}
	}
	return 0;
}