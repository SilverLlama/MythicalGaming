#include <sourcemod>
#define CHECKLIMIT 2
#pragma semicolon 1
#pragma tabsize 0


new g_PlayerTime[MAXPLAYERS+1];
new g_PlayerLimit[MAXPLAYERS+1];
 
public Plugin:myinfo =
{
        name = "TTT rules",
        author = "SilverLlama",
        description = "Shows the CSOGBig.",
        version = "1.0",
        url = "http://mythical-gaming.com"
}
 
public OnPluginStart()
{
    RegConsoleCmd("sm_big", command_big);
    RegConsoleCmd("sm_gobig", command_big);
    RegConsoleCmd("sm_csgobig", command_big);
    RegConsoleCmd("sm_skins", command_skins);
    RegConsoleCmd("sm_forum", command_forum);
	RegConsoleCmd("sm_forums", command_forum);
	RegConsoleCmd("sm_group", command_group);
	RegConsoleCmd("sm_groups", command_group);
	RegConsoleCmd("sm_sgroup", command_group);
	RegConsoleCmd("sm_steamgroup", command_group);
	RegConsoleCmd("sm_donate", command_donate);
}



public Action:command_big(client, args)
{
	// Gets the GameTime
	new acttime = GetTime();
    // Only if some time has passed

    if(g_PlayerTime[client] < acttime-1)
    {
                    // Increase the amount..
    	g_PlayerLimit[client]++;

                    // If used too often
    	if(g_PlayerLimit[client] <= CHECKLIMIT)
		{
			QueryClientConVar(client, "cl_disablehtmlmotd", ConVarQueryFinished:ClientConVar, client);
			ShowMOTDPanel(client, "CSGO Big, Go big or go home!", "http://mythical-gaming.com/test6.html", MOTDPANEL_TYPE_URL);
			new String:name[MAX_NAME_LENGTH];
			GetClientName(client, name, sizeof(name));
			PrintToChatAll("\x01 \x02[!big]\x04 %s is trying his luck on CSGOBig!", name);
			g_PlayerTime[client] = acttime;
		} else {
			QueryClientConVar(client, "cl_disablehtmlmotd", ConVarQueryFinished:ClientConVar, client);
			// Sets the clients time
    		g_PlayerTime[client] = acttime;
    		ShowMOTDPanel(client, "CSGO Big, Go big or go home!", "http://mythical-gaming.com/test6.html", MOTDPANEL_TYPE_URL);
    	}
	}
	return Plugin_Handled;
}


public Action:command_group(client, args)
{
	// Gets the GameTime
	new acttime = GetTime();
    // Only if some time has passed

    if(g_PlayerTime[client] < acttime-1)
    {
                    // Increase the amount..
    	g_PlayerLimit[client]++;

                    // If used too often
    	if(g_PlayerLimit[client] <= CHECKLIMIT)
		{
			QueryClientConVar(client, "cl_disablehtmlmotd", ConVarQueryFinished:ClientConVar, client);
			ShowMOTDPanel(client,"Mythical Gaming Steamgroup","http://mythical-gaming.com/test4.html" ,MOTDPANEL_TYPE_URL);
			new String:name[MAX_NAME_LENGTH];
			GetClientName(client, name, sizeof(name));
			PrintToChatAll("\x01 \x02[!Group]\x03 %s is checking out our steamgroup!",name);
			g_PlayerTime[client] = acttime;
		} else {
			QueryClientConVar(client, "cl_disablehtmlmotd", ConVarQueryFinished:ClientConVar, client);
			// Sets the clients time
    		g_PlayerTime[client] = acttime;
			ShowMOTDPanel(client,"Mythical Gaming Steamgroup","http://mythical-gaming.com/test4.html" ,MOTDPANEL_TYPE_URL);
    	}
	}
	return Plugin_Handled;
}


public Action:command_forum(client, args)
{
	// Gets the GameTime
	new acttime = GetTime();
    // Only if some time has passed

    if(g_PlayerTime[client] < acttime-1)
    {
                    // Increase the amount..
    	g_PlayerLimit[client]++;

                    // If used too often
    	if(g_PlayerLimit[client] <= CHECKLIMIT)
		{
			QueryClientConVar(client, "cl_disablehtmlmotd", ConVarQueryFinished:ClientConVar, client);
			ShowMOTDPanel(client,"Mythical Gaming Forums","http://mythical-gaming.com/test3.html" ,MOTDPANEL_TYPE_URL);
			new String:name[MAX_NAME_LENGTH];
			GetClientName(client, name, sizeof(name));
			PrintToChatAll("\x01 \x02[!Forums]\x03 %s is browsing the forums!",name);
			g_PlayerTime[client] = acttime;
		} else {
			QueryClientConVar(client, "cl_disablehtmlmotd", ConVarQueryFinished:ClientConVar, client);
			// Sets the clients time
    		g_PlayerTime[client] = acttime;
			ShowMOTDPanel(client,"Mythical Gaming Forums","http://mythical-gaming.com/test3.html" ,MOTDPANEL_TYPE_URL);
    	}
	}
	return Plugin_Handled;
}

public Action:command_skins(client, args)
{
	// Gets the GameTime
	new acttime = GetTime();
    // Only if some time has passed

    if(g_PlayerTime[client] < acttime-1)
    {
                    // Increase the amount..
    	g_PlayerLimit[client]++;

                    // If used too often
    	if(g_PlayerLimit[client] <= CHECKLIMIT)
		{
			QueryClientConVar(client, "cl_disablehtmlmotd", ConVarQueryFinished:ClientConVar, client);
			ShowMOTDPanel(client, "Skins", "http://www.mythical-gaming.com/test5.html", MOTDPANEL_TYPE_URL);
			new String:name[MAX_NAME_LENGTH];
			GetClientName(client, name, sizeof(name));
			PrintToChatAll("\x01 \x02[!skins]\x04 %s is looking at a preview of the playerskins!", name);
			g_PlayerTime[client] = acttime;
		} else {
			QueryClientConVar(client, "cl_disablehtmlmotd", ConVarQueryFinished:ClientConVar, client);
			// Sets the clients time
    		g_PlayerTime[client] = acttime;
    		ShowMOTDPanel(client, "Skins", "http://www.mythical-gaming.com/test5.html", MOTDPANEL_TYPE_URL);
    	}
	}
	return Plugin_Handled;
}



public Action:command_donate(client, args)
{
		new Handle:menu = CreateMenu(Handle_Menu);
		SetMenuTitle(menu, "Donation Info");		
			

		AddMenuItem(menu, "paypal", "Paypal");
		AddMenuItem(menu, "key", "Skin Donation Info");
		if(g_PlayerLimit[client] <= CHECKLIMIT)
		{
			new String:name[MAX_NAME_LENGTH];
			GetClientName(client, name, sizeof(name));
			PrintToChatAll("\x01 \x02[!Donate]\x04 %s is thinking about donating!", name);
		}
		DisplayMenu(menu, client, MENU_TIME_FOREVER);
		
}
	
public Handle_Menu(Handle:menu, MenuAction:action, client, itemNum)
{
	if ( action == MenuAction_Select )
	{
		decl String:info[100], String:info2[100];
		new bool:found = GetMenuItem(menu, itemNum, info, sizeof(info), _, info2, sizeof(info2));
		if(found)
		{
			if(StrEqual(info, "paypal"))
			{
				QueryClientConVar(client, "cl_disablehtmlmotd", ConVarQueryFinished:ClientConVar, client);
				ShowMOTDPanel(client, "Paypal", "http://www.mythical-gaming.com/test.html", MOTDPANEL_TYPE_URL);
			}
			else if(StrEqual(info, "key"))
			{
				QueryClientConVar(client, "cl_disablehtmlmotd", ConVarQueryFinished:ClientConVar, client);
				ShowMOTDPanel(client, "Skin Donation Info", "http://www.mythical-gaming.com/test2.html", MOTDPANEL_TYPE_URL);
			}
		}
	}
}


public void ClientConVar(QueryCookie:cookie, client, ConVarQueryResult:result, const String:cvarName[], const String:cvarValue[])
{
	if (StringToInt(cvarValue) > 0)
	{
		PrintToChat(client, "---------------------------------------------------------------");
		PrintToChat(client, "You have cl_disablehtmlmotd to 1 and for that reason the rules do not show up for you");
		PrintToChat(client, "Please, put this in your console: cl_disablehtmlmotd 0");
		PrintToChat(client, "---------------------------------------------------------------");
	}
}