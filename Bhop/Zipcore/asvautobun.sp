#pragma semicolon 1

#define DEBUG

#define PLUGIN_AUTHOR "SilverLlama"
#define PLUGIN_VERSION "1.00"

#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <sdkhooks>
#include <timer>


ConVar sv_autobunnyhopping = null;

public Plugin myinfo = 
{
	name = "sv_autobunnyhop",
	author = PLUGIN_AUTHOR,
	description = "Enables and disables sv_autobunnyhopping client side",
	version = PLUGIN_VERSION,
	url = "http://mythical.pro"
};

public void OnPluginStart()
{
	sv_autobunnyhopping = FindConVar("sv_autobunnyhopping");
	sv_autobunnyhopping.BoolValue = false;
}

public int OnTimerStarted(int client)
{
	UpdateAutoBhop(client);	
}

public int OnTimerStopped(int client)
{
	UpdateAutoBhop(client);	
}

void UpdateAutoBhop(int client)
{
	if(IsFakeClient(client))
		return;
	
	if(sv_autobunnyhopping != null)
	{
		if(Timer_GetStyle(client) == 3)
		{
			sv_autobunnyhopping.ReplicateToClient(client, "0");
		}
		else
		{
			sv_autobunnyhopping.ReplicateToClient(client, "1");
		}
			
	}
}
