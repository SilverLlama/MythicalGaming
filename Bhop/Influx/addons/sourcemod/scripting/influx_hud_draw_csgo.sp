#pragma tabsize 0
#include <sourcemod>

#include <influx/core>
#include <influx/hud>

#include <msharedutil/misc>


#undef REQUIRE_PLUGIN
#include <influx/help>
#include <influx/recording>
#include <influx/strafes>
#include <influx/jumps>
#include <influx/pause>
#include <influx/practise>
//#include <influx/strfsync>
#include <influx/truevel>
#include <influx/zones_stage>
#include <influx/zones_checkpoint>
#include <influx/maprankings>
#include <influx/style_tas>


//#define DEBUG


new bool:B_IsJump[MAXPLAYERS + 1];
new Float:gF_AngleCache[66];
new goodGains[66];
new totalMeasures[66];
new g_iButtonsPressed[MAXPLAYERS+1] = {0,...};


new Float:prevSpeed[MAXPLAYERS+1];
new Float:prevSSpeed[MAXPLAYERS+1];
new Float:fspeed[MAXPLAYERS+1];
new Float:speedChange[MAXPLAYERS+1];
new Float:speedSixth[MAXPLAYERS+1] = 0.0;
new Float:countSixth[MAXPLAYERS+1] = 0.0;


// LIBRARIES
bool g_bLib_Strafes;
bool g_bLib_Jumps;
bool g_bLib_Pause;
bool g_bLib_Practise;
bool g_bLib_Recording;
//bool g_bLib_StrfSync;
bool g_bLib_Truevel;
bool g_bLib_Stage;
bool g_bLib_CP;
bool g_bLib_MapRanks;
bool g_bLib_Style_Tas;


public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - HUD | Draw CS:GO",
    description = "Displays info on player's screen.",
    version = INF_VERSION
};

public APLRes AskPluginLoad2( Handle hPlugin, bool late, char[] szError, int error_len )
{
    if ( GetEngineVersion() != Engine_CSGO )
    {
        FormatEx( szError, error_len, "Bad engine version!" );
        
        return APLRes_Failure;
    }
    
    return APLRes_Success;
}

public void OnPluginStart()
{
    // LIBRARIES
    g_bLib_Strafes = LibraryExists( INFLUX_LIB_STRAFES );
    g_bLib_Jumps = LibraryExists( INFLUX_LIB_JUMPS );
    g_bLib_Pause = LibraryExists( INFLUX_LIB_PAUSE );
    g_bLib_Practise = LibraryExists( INFLUX_LIB_PRACTISE );
    g_bLib_Recording = LibraryExists( INFLUX_LIB_RECORDING );
    //g_bLib_StrfSync = LibraryExists( INFLUX_LIB_STRFSYNC );
    g_bLib_Truevel = LibraryExists( INFLUX_LIB_TRUEVEL );
    g_bLib_Stage = LibraryExists( INFLUX_LIB_ZONES_STAGE );
    g_bLib_CP = LibraryExists( INFLUX_LIB_ZONES_CP );
    g_bLib_MapRanks = LibraryExists( INFLUX_LIB_MAPRANKS );
    g_bLib_Style_Tas = LibraryExists( INFLUX_LIB_STYLE_TAS );
}

public void OnLibraryAdded( const char[] lib )
{
    if ( StrEqual( lib, INFLUX_LIB_STRAFES ) ) g_bLib_Strafes = true;
    if ( StrEqual( lib, INFLUX_LIB_JUMPS ) ) g_bLib_Jumps = true;
    if ( StrEqual( lib, INFLUX_LIB_PAUSE ) ) g_bLib_Pause = true;
    if ( StrEqual( lib, INFLUX_LIB_PRACTISE ) ) g_bLib_Practise = true;
    if ( StrEqual( lib, INFLUX_LIB_RECORDING ) ) g_bLib_Recording = true;
    if ( StrEqual( lib, INFLUX_LIB_TRUEVEL ) ) g_bLib_Truevel = true;
    if ( StrEqual( lib, INFLUX_LIB_ZONES_STAGE ) ) g_bLib_Stage = true;
    if ( StrEqual( lib, INFLUX_LIB_ZONES_CP ) ) g_bLib_CP = true;
    if ( StrEqual( lib, INFLUX_LIB_MAPRANKS ) ) g_bLib_MapRanks = true;
    if ( StrEqual( lib, INFLUX_LIB_STYLE_TAS ) ) g_bLib_Style_Tas = true;
}

public void OnLibraryRemoved( const char[] lib )
{
    if ( StrEqual( lib, INFLUX_LIB_STRAFES ) ) g_bLib_Strafes = false;
    if ( StrEqual( lib, INFLUX_LIB_JUMPS ) ) g_bLib_Jumps = false;
    if ( StrEqual( lib, INFLUX_LIB_PAUSE ) ) g_bLib_Pause = false;
    if ( StrEqual( lib, INFLUX_LIB_PRACTISE ) ) g_bLib_Practise = false;
    if ( StrEqual( lib, INFLUX_LIB_RECORDING ) ) g_bLib_Recording = false;
    //if ( StrEqual( lib, INFLUX_LIB_STRFSYNC ) ) g_bLib_StrfSync = false;
    if ( StrEqual( lib, INFLUX_LIB_TRUEVEL ) ) g_bLib_Truevel = false;
    if ( StrEqual( lib, INFLUX_LIB_ZONES_STAGE ) ) g_bLib_Stage = false;
    if ( StrEqual( lib, INFLUX_LIB_ZONES_CP ) ) g_bLib_CP = false;
    if ( StrEqual( lib, INFLUX_LIB_MAPRANKS ) ) g_bLib_MapRanks = false;
    if ( StrEqual( lib, INFLUX_LIB_STYLE_TAS ) ) g_bLib_Style_Tas = false;
}

public Action Influx_OnDrawHUD( int client, int target, HudType_t hudtype )
{
    static char szMsg[256];
    szMsg[0] = '\0';
    
    decl String:szTemp[32];
    decl String:szTemp2[32];
    decl String:szSecFormat[12];
    
    
    int hideflags = Influx_GetClientHideFlags( client );
    
    
    if ( hudtype == HUDTYPE_HINT )
    {
        Influx_GetSecondsFormat_Timer( szSecFormat, sizeof( szSecFormat ) );
        
        
        RunState_t state = Influx_GetClientState( target );
        
        if ( !(hideflags & HIDEFLAG_TIME) && state == STATE_RUNNING )
        {
            float cptime = INVALID_RUN_TIME;
            
            if (g_bLib_CP
            &&  (GetEngineTime() - Influx_GetClientLastCPTouch( target )) < 2.0)
            {
                cptime = Influx_GetClientLastCPSRTime( target );
                
                // Fallback to best time if no SR time is found.
                if ( cptime == INVALID_RUN_TIME ) cptime = Influx_GetClientLastCPBestTime( target );
            }
                        
            
            if ( g_bLib_Pause && Influx_IsClientPaused( target ) )
            {
                Inf_FormatSeconds( Influx_GetClientPausedTime( target ), szTemp, sizeof( szTemp ), "%05.2f" );
                FormatEx( szMsg, sizeof( szMsg ), "Time: %s", szTemp );
            }
            else if ( cptime != INVALID_RUN_TIME )
            {
                float time = Influx_GetClientLastCPTime( target );
                
                decl c;
                
                Inf_FormatSeconds( Inf_GetTimeDif( time, cptime, c ), szTemp2, sizeof( szTemp2 ), szSecFormat );
                
                FormatEx( szMsg, sizeof( szMsg ), "CP: <font color=\"#42f4a1\">%c%s</font>", c, szTemp2 );
            }
            else if ( g_bLib_Style_Tas && Influx_GetClientStyle( target ) == STYLE_TAS )
            {
                Inf_FormatSeconds( Influx_GetClientTASTime( target ), szTemp, sizeof( szTemp ), szSecFormat );
                FormatEx( szMsg, sizeof( szMsg ), "Time: %s", szTemp );
            }
            else
            {
                Inf_FormatSeconds( Influx_GetClientTime( target ), szTemp, sizeof( szTemp ), szSecFormat );
                if(Influx_GetClientTime( target ) < Influx_GetClientCurrentBestTime( target ))
                	Format( szMsg, sizeof( szMsg ), "Time: <font color=\"#47e22f\">%s</font>", szTemp );
            	else Format( szMsg, sizeof( szMsg ), "Time: <font color=\"#d82222\">%s</font>", szTemp );
           }
            
            float time = Influx_GetClientCurrentPB( target );
			Inf_FormatSeconds( time, szTemp2, sizeof( szTemp2 ), szSecFormat );
			
			
			
			Format(szMsg, sizeof(szMsg), "%s\t\t%s %s",szMsg, "PB:", szTemp2);
            
            Influx_GetModeName( Influx_GetClientMode(target), szTemp, sizeof( szTemp ), true );  
            Format(szMsg, sizeof(szMsg), "%s\nMode: %s",szMsg, szTemp);
            
            Influx_GetStyleShortName( Influx_GetClientStyle(target), szTemp, sizeof( szTemp ), true );
            if(Influx_GetClientStyle(target) == 0)
            	Format(szMsg, sizeof(szMsg), "%s\t\tStyle: <font color=\"#735421\">Normal</font>",szMsg );
            else Format(szMsg, sizeof(szMsg), "%s\t\tStyle: <font color=\"#3ds15d\">%s</font>",szMsg, szTemp );
            if (getSync(target))
            	Format(szMsg, sizeof(szMsg), "%s\nSync: <font color=\"#d5e5e1\">%5.2fPCT</font>",szMsg,  getSync( target ));
            else Format(szMsg, sizeof(szMsg), "%s\nSync: <font color=\"#ded214\">100.00PCT</font>",szMsg);
            
            Format(szMsg, sizeof(szMsg), "%s\t\t%s <font color=\"#47e22f\">%5.2f</font>",szMsg, "Speed:", GetSpeed( target ));
            
        }
        else if ( state == STATE_START || state == STATE_FINISHED )
        {
            ResetSync(target);
            Influx_GetRunName( Influx_GetClientRunId( target ), szTemp, sizeof( szTemp ) );
            //FormatEx( szMsg, sizeof( szMsg ), "<font color=\"#4286f4\">In %s Start</font>", szTemp );
                      
			Format(szMsg, sizeof(szMsg), "%s", "\t      <font color='#F553F5'>Mythical Gaming</font>");
            
            float time = Influx_GetClientCurrentBestTime( target );
			Inf_FormatSeconds( time, szTemp, sizeof( szTemp ), szSecFormat );
			Format(szMsg, sizeof(szMsg), "%s\n%s %s",szMsg, "WR:", szTemp);
            
            time = Influx_GetClientCurrentPB( target );
			Inf_FormatSeconds( time, szTemp, sizeof( szTemp ), szSecFormat );
			Format(szMsg, sizeof(szMsg), "%s\t\t%s %s",szMsg, "PB:", szTemp);
			
			
			Format(szMsg, sizeof(szMsg), "%s\nRank: <font color=\"#47e22f\">%i/%i</font>",szMsg, Influx_GetClientCurrentMapRank(target),Influx_GetClientCurrentMapRankCount(target) );
			Format(szMsg, sizeof(szMsg), "%s\t\t\t%s <font color=\"#47e22f\">%5.2f</font>",szMsg, "Speed:", GetSpeed( target ));
			
            
        }
        
        
        bool bprac = ( g_bLib_Practise && !(hideflags & HIDEFLAG_PRACMODE) && Influx_IsClientPractising( target ) );
        
        bool bpause = ( g_bLib_Pause && !(hideflags & HIDEFLAG_PAUSEMODE) && Influx_IsClientPaused( target ) );
        
        if ( bprac || bpause )
        {
            if ( bprac )
            {
                Format( szMsg, sizeof( szMsg ), "%s\nPractising", szMsg );
            }
            if ( bpause )
            {
                Format( szMsg, sizeof( szMsg ), "%s%s\nPaused", szMsg, bprac ? "/" : "" );
            }
        }
        

        
        
        if ( szMsg[0] != '\0' )
        {
        	Format(szMsg, sizeof(szMsg), "<font size='16'>%s</font>", szMsg);
        	ReplaceString(szMsg, sizeof(szMsg), "PCT", "%%", true);
            PrintHintText( client, szMsg );
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    else if ( hudtype == HUDTYPE_MENU_CSGO )
    {
        Influx_GetSecondsFormat_Sidebar( szSecFormat, sizeof( szSecFormat ) );
        
        
        // Disable for bots.
        if ( IsFakeClient( target ) )
        {
            // Draw recording bot info.
            if ( g_bLib_Recording && Influx_GetReplayBot() == target )
            {
                float time = Influx_GetReplayTime();
                if ( time == INVALID_RUN_TIME ) return Plugin_Stop;
                
                
                decl String:szTime[12];
                
                
                
                Influx_GetModeName( Influx_GetReplayMode(), szTemp, sizeof( szTemp ), true );
                Influx_GetStyleName( Influx_GetReplayStyle(), szTemp2, sizeof( szTemp2 ), true );
                
                
                Inf_FormatSeconds( time, szTime, sizeof( szTime ), szSecFormat );
                
                
                decl String:szName[16];
                Influx_GetReplayName( szName, sizeof( szName ) );
                
                FormatEx( szMsg, sizeof( szMsg ), "%s%s%s\n \nTime: %s\nName: %s",
                    szTemp2, // Style
                    ( szTemp2[0] != '\0' ) ? " " : "",
                    szTemp, // Mode
                    szTime,
                    szName );
                
                
                ShowPanel( client, szMsg );
            }
            
            return Plugin_Stop;
        }
        
        
        if ( g_bLib_Stage && Influx_ShouldDisplayStages( client ) )
        {
            int stages = Influx_GetClientStageCount( target );
            
            if ( stages < 2 )
            {
                strcopy( szTemp2, sizeof( szTemp2 ), "Linear" );
            }
            else
            {
                FormatEx( szTemp2, sizeof( szTemp2 ), "%i/%i", Influx_GetClientStage( target ), stages );
            }
            
            FormatEx( szMsg, sizeof( szMsg ), "Stage: %s", szTemp2 );
        }
        
        if ( g_bLib_MapRanks )
        {
            int rank = Influx_GetClientCurrentMapRank( target );
            int numrecs = Influx_GetClientCurrentMapRankCount( target );
            
            if ( numrecs > 0 )
            {
                if ( rank > 0 )
                {
                    Format( szMsg, sizeof( szMsg ), "%s%sRank: %i/%i", szMsg, NEWLINE_CHECK( szMsg ), rank, numrecs );
                }
                else
                {
                    Format( szMsg, sizeof( szMsg ), "%s%sRank: ?/%i", szMsg, NEWLINE_CHECK( szMsg ), numrecs );
                }
            }
        }
        
        
        if ( !(hideflags & HIDEFLAG_PB_TIME) && Influx_IsClientCached( target ) )
        {
            float time = Influx_GetClientCurrentPB( target );
            
            if ( time > INVALID_RUN_TIME )
            {
                Inf_FormatSeconds( time, szTemp2, sizeof( szTemp2 ), szSecFormat );
                FormatEx( szTemp, sizeof( szTemp ), "PB: %s", szTemp2 );
            }
            else
            {
                strcopy( szTemp, sizeof( szTemp ), "PB: N/A" );
            }
            
            Format( szMsg, sizeof( szMsg ), "%s%s%s", szMsg, NEWLINE_CHECK( szMsg ), szTemp );
        }
        
        if ( !(hideflags & HIDEFLAG_WR_TIME) )
        {
            float time = Influx_GetClientCurrentBestTime( target );
            
            if ( time > INVALID_RUN_TIME )
            {
                decl String:szTemp3[32];
                
                Inf_FormatSeconds( time, szTemp2, sizeof( szTemp2 ), szSecFormat );
                Influx_GetClientCurrentBestName( target, szTemp3, sizeof( szTemp3 ) );
                
                LimitString( szTemp3, sizeof( szTemp3 ), 8 );
                
                
                FormatEx( szTemp, sizeof( szTemp ), "SR: %s (%s)", szTemp2, szTemp3 );
            }
            else
            {
                strcopy( szTemp, sizeof( szTemp ), "SR: N/A" );
            }
            
            Format( szMsg, sizeof( szMsg ), "%s%s%s",
                szMsg,
                NEWLINE_CHECK( szMsg ),
                szTemp );
        }
        
        
        ADD_SEPARATOR( szMsg, "\n " );
        
        
        RunState_t state = Influx_GetClientState( target );
        
        if ( g_bLib_Strafes && state >= STATE_RUNNING )
        {
            Format( szMsg, sizeof( szMsg ), "%s%sStrafes: %i",
                szMsg,
                NEWLINE_CHECK( szMsg ),
                Influx_GetClientStrafeCount( target ) );
        }
        
        if ( g_bLib_Jumps && state >= STATE_RUNNING )
        {
            Format( szMsg, sizeof( szMsg ), "%s%sJumps: %i",
                szMsg,
                NEWLINE_CHECK( szMsg ),
                Influx_GetClientJumpCount( target ) );
        }
        
        //ShowPanel( client, szMsg );
    }
    
    return Plugin_Stop;
}

// Check if they want truevel.
stock float GetSpeed( int client )
{
    return ( g_bLib_Truevel && Influx_IsClientUsingTruevel( client ) ) ? GetEntityTrueSpeed( client ) : GetEntitySpeed( client );
}

stock void ShowPanel( int client, const char[] msg )
{
    Panel panel = new Panel();
    panel.SetTitle( msg );
    panel.Send( client, Hndlr_Panel_Empty, 3 );
    
    delete panel;
}

public int Hndlr_Panel_Empty( Menu menu, MenuAction action, int client, int param2 ) {}


public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon)
{
	RunState_t state = Influx_GetClientState( client );
	g_iButtonsPressed[client] = buttons;
	if(GetClientButtons(client) & IN_JUMP)
	{
		B_IsJump[client] = true;
	}
	if (!(GetEntityFlags(client) & 1))
	{
		if (state == STATE_RUNNING)
		{
			if (IsPlayerAlive(client))
			{
				fspeed[client] = GetSpeed(client);
				new iGroundEntity = GetEntPropEnt(client, PropType:0, "m_hGroundEntity", 0);
				new Float:fAngle = angles[1] - gF_AngleCache[client];
				while (fAngle > 180.0)
				{
					fAngle -= 360.0;
				}
				while (fAngle < -180.0)
				{
					fAngle += 360.0;
				}
				if (iGroundEntity == -1 && !(GetEntityFlags(client) & 512) && 0.0 != fAngle)
				{
					new Float:fAbsVelocity[3] = 0.0;
					GetEntPropVector(client, PropType:1, "m_vecAbsVelocity", fAbsVelocity, 0);
					if (SquareRoot(Pow(fAbsVelocity[0], 2.0) + Pow(fAbsVelocity[1], 2.0)) > 0.0)
					{
						new Float:fTempAngle = angles[1];
						new Float:fAngles[3] = 0.0;
						GetVectorAngles(fAbsVelocity, fAngles);
						if (fTempAngle < 0.0)
						{
							fTempAngle += 360.0;
						}
						new Float:fDirectionAngle = fTempAngle - fAngles[1];
						if (fDirectionAngle < 0.0)
						{
							fDirectionAngle = -fDirectionAngle;
						}
						totalMeasures[client]++;
						if (fDirectionAngle < 22.5 || fDirectionAngle > 337.5)
						{
							if ((fAngle > 0.0 && vel[1] < 0.0) || (fAngle < 0.0 && vel[1] > 0.0))
							{
								goodGains[client]++;
							}
						}
					}
				}
				gF_AngleCache[client] = angles[1];
				prevSpeed[client] = fspeed[client];
			}
		}
	}
}



public UpdateSpeed(clientid, Float:tSpeed)
{
    //I don't know if you got checked if the player jumped, but since it puts out how many times you jumped in a row, you could use that function
        if(prevSSpeed[clientid] < tSpeed)
        {
           
       speedChange[clientid] = (tSpeed - prevSSpeed[clientid]) / prevSSpeed[clientid] * 100.0;

      //!!!!!!!!!!!!! Format to %.1f in the hud to get a value like: 81.4
      //User experience could get improved by adding a plus in front of float when formatting (if the value is positive)
        }
    else  speedChange[clientid] = (tSpeed - prevSSpeed[clientid]) / prevSSpeed[clientid] * 100.0;
    
}

public float getSync(int client)
{
	return goodGains[client] / float(totalMeasures[client]) * 100;
}

public ResetSync(clientid)
{
  totalMeasures[clientid] = 0;
	goodGains[clientid] = 0;
	prevSSpeed[clientid] = 0.0;
	countSixth[clientid] = 0.0;
	speedSixth[clientid] = 0.0;
	speedChange[clientid] = 0.0;
}