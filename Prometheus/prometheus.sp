#pragma semicolon 1

#include <sourcemod>
#include <clientprefs>
#include <morecolors>
#undef REQUIRE_PLUGIN
#tryinclude <sourcebans>
#tryinclude <sb_admins>
#define REQUIRE_PLUGIN

#define PLUGIN_VERSION "1.1"

public Plugin myinfo = {
	name		= "Prometheus - Sourcemod Donation System",
	author		= "Nanochip",
	description = "Automatic Donation System",
	version		= PLUGIN_VERSION,
	url			= "https://scriptfodder.com/scripts/view/565"
};

static char KVPath[PLATFORM_MAX_PATH];

ConVar cvarMode;
ConVar cvarBroadcast;


bool csgo;

Handle db = null;

public void OnPluginStart()
{
	if (GetEngineVersion() == Engine_CSGO) csgo = true;
	else csgo = false;

	LoadTranslations("common.phrases");
	CreateConVar("sm_prometheus_version", PLUGIN_VERSION, "Prometheus Version", FCVAR_SPONLY|FCVAR_UNLOGGED|FCVAR_DONTRECORD|FCVAR_REPLICATED|FCVAR_NOTIFY);
	cvarMode = CreateConVar("sm_prometheus_mode", "3", "0 = Disable the plugin, 1 = Add the donator to admins.cfg (flatfile), 2 = Add the donator to the MySQL database (uses the 'default' connection info in your databases.cfg), 3 = Add the donator to Sourcebans admin database", 0, true, 0.0, true, 3.0);
	if (csgo)
	{
		cvarBroadcast = CreateConVar("sm_prometheus_message", "[Prometheus] PLAYER_NAME has donated $DONATION_AMOUNT and receives PACKAGE_NAME! Thank you!", "Edit the broadcast message when a player donates. CS:GO detected, no colors available.", 0);
	}
	else
	{
		cvarBroadcast = CreateConVar("sm_prometheus_message", "{fullred}[Prometheus] {haunted}PLAYER_NAME {honeydew}has donated {haunted}$DONATION_AMOUNT{honeydew} and receives {haunted}PACKAGE_NAME{honeydew}! Thank you!", "What do you want the broadcast message to be? List of colors can be found here: https://www.doctormckay.com/morecolors.php", 0);
	}
	//cvarCheckInterval = CreateConVar("sm_prometheus_checkinterval", "600.0", "Time in seconds to check if the user's rank has expired. Default is every 10 minutes (600 seconds)", 0, true, 60.0);
	CreateTimer(10.0, Timer_CheckInterval, _, TIMER_REPEAT);
	
	RegAdminCmd("sm_prometheus", Cmd_Prometheus, ADMFLAG_ROOT, "");
	
	if (FileExists("addons/sourcemod/configs/prometheus.cfg"))
	{
		BuildPath(Path_SM, KVPath, sizeof(KVPath), "configs/prometheus.cfg");
	} else {
		SetFailState("[Prometheus] Could not find prometheus.cfg in addons/sourcemod/configs/ folder!");
	}


	ConnectToDB();
	
	AutoExecConfig(true, "Prometheus");
}

void ConnectToDB()
{
	char errors[255];
	if (SQL_CheckConfig("sourcebans"))
	{
		db = SQL_Connect("sourcebans", true, errors, sizeof(errors));
	}
	if (db == null) LogError("[Prometheus] Unable to connect to MySQL database: %s", errors);
}

public Action Timer_CheckInterval(Handle timer)
{
	char strExpires[64], steamid[32];
	Handle kvdb = CreateKeyValues("Prometheus");
	FileToKeyValues(kvdb, KVPath);
	if (!KvGotoFirstSubKey(kvdb)) return Plugin_Continue;
	do
	{
		KvGetString(kvdb, "expire_time", strExpires, sizeof(strExpires));
		if (GetTime() >= StringToInt(strExpires))
		{
			KvGetString(kvdb, "steamid", steamid, sizeof(steamid));
			LogAction(0, -1, "%s is now expired, removing rank...", steamid);
			RemovePlayer(steamid);
			ServerCommand("sm_reloadadmins");
			KvDeleteThis(kvdb);
		}
	} while (KvGotoNextKey(kvdb));
	
	KvRewind(kvdb);
	KeyValuesToFile(kvdb, KVPath);
	CloseHandle(kvdb);
	return Plugin_Continue;
}

public void OnMapStart()
{

	ConnectToDB();
	
}

public Action Cmd_Prometheus(int client, int args)
{
	//sm_prometheus (name) (steamid) (donation_amount) (expires_unixtime) (flag/group) (package_name)
	if (GetConVarInt(cvarMode) == 0) return Plugin_Handled;
	char name[MAX_NAME_LENGTH], steamid[32], dAmount[32], strExpires[64], flGroup[64], strTime[64], packageName[256];
	int group;
	
	// Get the info from the command
	GetCmdArg(1, name, sizeof(name));
	GetCmdArg(2, steamid, sizeof(steamid));
	GetCmdArg(3, dAmount, sizeof(dAmount));
	GetCmdArg(4, strExpires, sizeof(strExpires));
	GetCmdArg(5, flGroup, sizeof(flGroup));
	GetCmdArg(6, packageName, sizeof(packageName));
	
	// Store the time of donation and expiration time in prometheus.cfg
	IntToString(GetTime(), strTime, sizeof(strTime));
	if (!StrEqual(strExpires, "0"))
	{
		Handle kvdb = CreateKeyValues("Prometheus");
		FileToKeyValues(kvdb, KVPath);
		if (KvJumpToKey(kvdb, steamid, true))
		{
			KvSetString(kvdb, "donation_time", strTime);
			KvSetString(kvdb, "expire_time", strExpires);
			KvSetString(kvdb, "steamid", steamid);
		}
		KvRewind(kvdb);
		KeyValuesToFile(kvdb, KVPath);
		CloseHandle(kvdb);
	}
	group = StringToInt(flGroup);
	// If Sourcebans is enabled, add them to the SB database
	AddAdmin(steamid, group, name);
	
	ServerCommand("sm_reloadadmins");
	
	char broadcast[1024];
	GetConVarString(cvarBroadcast, broadcast, sizeof(broadcast));
	ReplaceString(broadcast, sizeof(broadcast), "PLAYER_NAME", name, true);
	ReplaceString(broadcast, sizeof(broadcast), "DONATION_AMOUNT", dAmount, true);
	ReplaceString(broadcast, sizeof(broadcast), "PACKAGE_NAME", packageName, true);
	if (csgo) PrintToChatAll(broadcast);
	else CPrintToChatAll(broadcast);
	PrintToServer(broadcast);
	
	return Plugin_Handled;
}

char sql_selectId[] = "SELECT id FROM `sb2_admins` where identity = '%s'";
char sql_InsertId[] = "INSERT INTO `sb2_admins_server_groups` VALUES (%i, %i, 1)";
char sql_InsertPlayer[] = "INSERT INTO `sb2_admins` (name, auth, identity) VALUES ('%s', '%s', '%s')";
char sql_DeletePlayer[] = "DELETE FROM `sb2_admins_server_groups` where admin_id = %i AND (group_id = 43 OR group_id = 40)";

void AddAdmin(char steamid[32], int group, char name[MAX_NAME_LENGTH])
{
	char query[1000];
	Format(query, sizeof(query), sql_selectId, steamid);
 
	DBResultSet hQuery = SQL_Query(db, query);
	if (hQuery == null)
	{
		return;
	}
 	
 	if (!SQL_FetchRow(hQuery))
 		InsertPlayer(steamid, name, group);
 	else
	{
		int id = SQL_FetchInt(hQuery, 0);
		Format(query, sizeof(query), sql_InsertId, id, group);
		DBResultSet hiQuery = SQL_Query(db, query);	
		delete hiQuery;
	} 	
 	
 
	delete hQuery;
 	
}

void RemovePlayer(char steamid[32])
{

	char query[1000];
	Format(query, sizeof(query), sql_selectId, steamid);
 	
	DBResultSet hQuery = SQL_Query(db, query);	
 	if (SQL_FetchRow(hQuery))
	{
		int id = SQL_FetchInt(hQuery, 0);
		Format(query, sizeof(query), sql_DeletePlayer, id);
		DBResultSet hiQuery = SQL_Query(db, query);	
		delete hiQuery;
	} 	
 	
 
	delete hQuery;
 	
}


void InsertPlayer(char steamid[32], char name[MAX_NAME_LENGTH], int group)
{
	char query[1000];
	
	char escapedName[64];
	StripQuotes(name);
	SQL_EscapeString(db, name, escapedName, sizeof(escapedName));
	Format(query, sizeof(query), sql_InsertPlayer, escapedName, "steam", steamid);
	
	if (!SQL_FastQuery(db, query))
	{
		char error[255];
		SQL_GetError(db, error, sizeof(error));
		PrintToServer("Failed to query (error: %s)", error);
	}
	Format(query, sizeof(query), sql_selectId, steamid);
	DBResultSet hQuery = SQL_Query(db, query);
	if (SQL_FetchRow(hQuery))
	{
		int id = SQL_FetchInt(hQuery, 0);
		Format(query, sizeof(query), sql_InsertId, id, group);
		DBResultSet hiQuery = SQL_Query(db, query);
		delete hiQuery;
	}
	
}