//Hello
#pragma semicolon 1
#include <smlib>
#include <sourcemod>
#include <sdktools>
#include <clientprefs>

//AlphaKey is Gay

new stopSettings[MAXPLAYERS+1];

new Handle:cookieStopPref;
Handle H_StopMusic = INVALID_HANDLE;

public Plugin:myinfo =
{
	name = "stop map music",
	author = "uFF",
	description = "its obv",
	version = "1.3.3.7",
	url = "http://www.r4p1d.xyz"
};

public OnPluginStart()
{
	RegConsoleCmd("sm_stopmusic", Command_StopMusic, "Stop map music");
	RegConsoleCmd("sm_musicstop", Command_StopMusic, "Stop map music");
	RegConsoleCmd("sm_mapmusic", Command_StopMusic, "Stop map music");
	
	cookieStopPref = RegClientCookie("stopmusic_setting", "Turn on or off", CookieAccess_Private);
	
	for (new client = 1; client <= MaxClients; client++)
	{
		OnClientCookiesCached(client);
	}
	
	
}

public OnClientPutInServer(client)
{
	if(!IsFakeClient(client))
	{
		stopSettings[client] = 0;
		
		if (AreClientCookiesCached(client))
		{
			loadClientCookiesFor(client);
		}
	}
	if(H_StopMusic == INVALID_HANDLE) CreateTimer(1.0, StoppingMusic, _, TIMER_FLAG_NO_MAPCHANGE|TIMER_REPEAT);
}

public OnClientCookiesCached(client)
{
	// Initializations and preferences loading
	if(IsClientInGame(client) && !IsFakeClient(client))
	{
		loadClientCookiesFor(client);	
	}
}

public OnMapStart() 
{
	H_StopMusic = CreateTimer(1.0, StoppingMusic, _, TIMER_FLAG_NO_MAPCHANGE|TIMER_REPEAT);
}

public Action:StoppingMusic(Handle:timer)
{
	for (new client = 1; client <= MaxClients; client++)
	{
		if(IsClientInGame(client) && !IsFakeClient(client))
		{
		if(stopSettings[client] == 1) 
		{
			ClientCommand(client, "playgamesound Music.StopAllExceptMusic'");
		}
		}
	}	
}

public Action:Command_StopMusic(client, args)
{
	
	decl String:buffer[5];
	
	
	if (stopSettings[client] == 0) 
	{
		stopSettings[client] = 1;
		IntToString(stopSettings[client], buffer, 5);
		SetClientCookie(client, cookieStopPref, buffer);
		PrintToChat(client, "[MUSIC] Music is now permenantly stopped.");
	}	
	else if(stopSettings[client] == 1) 
	{
		stopSettings[client] = 0;
		IntToString(stopSettings[client], buffer, 5);
		SetClientCookie(client, cookieStopPref, buffer);
		PrintToChat(client, "[MUSIC] Music is now enabled again.");
	}
	else
	{
		stopSettings[client] = 1;
		IntToString(stopSettings[client], buffer, 5);
		SetClientCookie(client, cookieStopPref, buffer);
		PrintToChat(client, "[MUSIC] Music is now permenantly stopped.");
	}
		
		
	
	
	return Plugin_Handled;
}

// 



loadClientCookiesFor(client)
{
	if(cookieStopPref == INVALID_HANDLE)
		return;
	
	decl String:buffer[5];
	
	//Master HUD
	GetClientCookie(client, cookieStopPref, buffer, 5);
	if(!StrEqual(buffer, ""))
	{
		stopSettings[client] = StringToInt(buffer);
	}
}